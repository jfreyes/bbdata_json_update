﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BeisbolData.Data;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace BeisbolData_Web_XML_Database_Update
{
    public partial class _Default : System.Web.UI.Page
    {
        System.Diagnostics.Stopwatch crono = new System.Diagnostics.Stopwatch();
        string MLBurl_Base = "http://gd2.mlb.com/components/game/win/year_YYYY/month_MM/day_DD/";


        public static string GetLigaCode()
        {
            using (var db1 = new BeisbolDataEntities())
            {
                string FirstTeamCode = db1.Teams.OrderBy(o => o.ID).FirstOrDefault().IDMLB.ToString();
                string FirstTeamResult = GetJsonResponse("http://statsapi.mlb.com/api/v1/teams/" + FirstTeamCode);
                return JsonConvert.DeserializeObject<TeamRO>(FirstTeamResult).teams.FirstOrDefault().league.id.ToString();
            }
        }

        public static string GetJsonResponse(string url)
        {
            var request = WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();

            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                return sr.ReadToEnd();
            }

        }
        public string LigaCode;

        public class LDEntity
        {
            public int id { get; set; }
            public string name { get; set; }
            public string link { get; set; }
        }

        public class Team
        {
            public int id { get; set; }
            public string name { get; set; }
            public string link { get; set; }
            public string teamCode { get; set; }
            public string fileCode { get; set; }
            public string abbreviation { get; set; }
            public string teamName { get; set; }
            public string locationName { get; set; }
            public string firstYearOfPlay { get; set; }
            public LDEntity league { get; set; }
            public LDEntity sport { get; set; }
            public string shortName { get; set; }
            public string parentOrgName { get; set; }
            public int parentOrgId { get; set; }
            public bool active { get; set; }
        }

        public class TeamRO
        {
            public string copyright { get; set; }
            public List<Team> teams { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                DateTime Fecha = DateTime.Now.AddDays(-1);
                Fecha = Fecha.AddHours(4); //1
                txtFecha.Text = Fecha.ToShortDateString();
                int SeasonID = Fecha.Year;
                if (Fecha.Month == 1)
                {
                    SeasonID--;
                    if (Fecha.Day >= 18)
                        txtPhaseID.Text = "3";
                    else
                        txtPhaseID.Text = "2";
                }
                else
                {
                    if (Fecha.Month == 12)
                    {
                        if (Fecha.Day >= 26)
                            txtPhaseID.Text = "2";
                        else
                            txtPhaseID.Text = "1";
                    }
                    else
                        txtPhaseID.Text = "1";
                }
                txtSeasonID.Text = SeasonID.ToString();
            }
        }

        private void SetupSeasonInfo()
        {
            SeasonInfo.Phase = Convert.ToByte(txtPhaseID.Text);
            SeasonInfo.AñoInicio = Convert.ToInt32(txtSeasonID.Text);
        }

        string GetURL(DateTime Fecha)
        {
            string myURL;
            myURL = MLBurl_Base.Replace("YYYY", Fecha.Year.ToString());
            myURL = myURL.Replace("MM", Fecha.Month.ToString("00"));
            myURL = myURL.Replace("DD", Fecha.Day.ToString("00"));
            return myURL;
        }

        public class SchRow
        {
            public string game_status_ind { get; set; }
            public string game_status_text { get; set; }
            public int game_nbr { get; set; }
            public string home_team_abbrev { get; set;}
            public string away_team_abbrev { get; set;}
            public DateTime game_time_local { get; set; }
            public string home_league_id { get; set; }
            public string game_id { get; set; }
            public string game_pk { get; set; }
        }

        public class SchQueryResults
        {
            public DateTime created { get; set; }
            public int totalSize { get; set; }
            public object row { get; set; }
        }

        public class ScheduleVwComplete
        {
            public string copyRight { get; set; }
            public SchQueryResults queryResults { get; set; }
        }

        public class ScheduleRO
        {
            public ScheduleVwComplete schedule_vw_complete { get; set; }
        }

        public class GameTuple
        {
            public SchRow GID { get; set; }
            public string GPK { get; set; }
        }

        List<GameTuple> ParseHTML(DateTime fecha)
        {
            List<GameTuple> Respuesta = new List<GameTuple>();
            string url = "http://lookup-service-prod.mlb.com/lookup/json/named.schedule_vw_complete.bam?game_date=%27" + fecha.ToString("yyyy/MM/dd") + "%27&season=" + SeasonInfo.AñoInicio.ToString() + "&sort_column=game_time_et%27&league_id=" + LigaCode;
            ScheduleRO SRO = JsonConvert.DeserializeObject<ScheduleRO>(GetJsonResponse(url));
            List<SchRow> schRowList = new List<SchRow>();
            if (SRO.schedule_vw_complete.queryResults.totalSize == 1)
            {
                SchRow schrow = JsonConvert.DeserializeObject<SchRow>(SRO.schedule_vw_complete.queryResults.row.ToString());
                schRowList.Add(schrow);
            }
            else
            {
                schRowList = JsonConvert.DeserializeObject<List<SchRow>>(SRO.schedule_vw_complete.queryResults.row.ToString());
            }
            if (SRO.schedule_vw_complete.queryResults.totalSize != 0)
                foreach (SchRow sRow in schRowList)
                    Respuesta.Add(new GameTuple { GID = sRow, GPK = sRow.game_pk } );
            return Respuesta;
        }

        string OpenURL(string url)
        {
            string Respuesta;
            WebClient myClient = new WebClient();
            Stream myStream = myClient.OpenRead(url);
            StreamReader myReader = new StreamReader(myStream);
            Respuesta = myReader.ReadToEnd();
            myReader.Close();
            myStream.Close();
            return Respuesta;
        }

        protected void btOK_Click(object sender, EventArgs e)
        {
            LigaCode = GetLigaCode();
            string Directorio = Request.PhysicalApplicationPath + @"temp";
            if (!Directory.Exists(Directorio))
            {
                lblStatus.Text = "Directorio de Trabajo no disponible";
                return;
            }
            try
            {
                string testFile = Request.PhysicalApplicationPath + @"temp\check.txt";
                StreamWriter myWriter = new StreamWriter(testFile);
                myWriter.WriteLine("This is a test");
                myWriter.Flush();
                myWriter.Close();
            }
            catch (Exception)
            {
                lblStatus2.Text = "Can't write to Temp Directory";
                return;
            }
            lblStatus2.Text = Directorio;
            //return;
            crono.Start();
            string myURL;
            DateTime FechaInicio = Convert.ToDateTime(txtFecha.Text);
            DateTime Fecha = FechaInicio;
            DateTime FechaFin = FechaInicio;

            SetupSeasonInfo();

            //List<string> JuegosNoProcesar = txtNoGame.Text.Split(Convert.ToChar(",")).ToList();
            List<string> JuegosNoProcesar = new List<string>();

            while (Fecha <= FechaFin)
            {
                Boolean HayJuegos;
                lblStatus.Text = "Now Processing: " + Fecha.ToShortDateString();
                myURL = GetURL(Fecha);
                //MessageBox.Show(myURL);
                //MessageBox.Show(OpenURL(myURL));
                //MessageBox.Show(ParseHTML(OpenURL(myURL)));
                List<GameTuple> JuegosDia = new List<GameTuple>();
                try
                {
                    //JuegosDia = ParseHTML(OpenURL(myURL));
                    // 17-1-2018
                    JuegosDia = ParseHTML(Fecha);
                    HayJuegos = true;
                }
                catch (Exception exc)
                {
                    HayJuegos = false;
                    //throw;
                }
                if (HayJuegos)
                {
                    foreach (string Juego in JuegosNoProcesar)
                    {
                        JuegosDia.Remove(JuegosDia.Where(p => "gid_" + p.GID.game_id.Replace("/", "_").Replace("-", "_") == Juego).FirstOrDefault());
                    }
                    foreach (GameTuple Juego in JuegosDia)
                    {
                        //GameData gamedata = new GameData(Juego, myURL);
                        lblStatus2.Text = "Game: " + Juego;
                        GameData gamedata = new GameData(Juego, myURL);
                        if (gamedata.Valid)
                        {
                            if (gamedata.ProcessGame())
                            {
                                txtLog1.Text += Juego.GID.game_id + " OK" + Environment.NewLine;
                            }
                            else
                            {
                                txtLog1.Text += Juego.GID.game_id + " BAD" + Environment.NewLine;
                                txtLog1.Text += "Error Description: " + ErrorInfo.ErrorDescription + Environment.NewLine;
                                txtLog1.Text += "Where: " + ErrorInfo.WhereWereYou + Environment.NewLine;
                            }
                        }
                    }
                }
                Fecha = Fecha.AddDays(1);
            } //END WHILE
            Sumarizacion();
            crono.Stop();
            TimeSpan ts = crono.Elapsed;
            lblStatus2.Text = "Total time: " + ts.TotalSeconds.ToString() + " segundos";
            txtLog1.Text += lblStatus2.Text;
        }

        private void Sumarizacion()
        {
            // UPDATE SUMARIZACION GAMES, STANDING AND SEASONSTATS
            int RecordsAffected;
            txtLog1.Text += "Finalizing..." + Environment.NewLine;
            lblStatus.Text = "Finalizing...";

            lblStatus2.Text = "Updating Game Stats";

            RecordsAffected = GameData.UpdateGameStats(SeasonInfo.AñoInicio, SeasonInfo.Phase);
            lblStatus2.Text += ": " + RecordsAffected.ToString() + " records";
            txtLog1.Text += "Updating Game Stats: " + RecordsAffected.ToString() + " records" + Environment.NewLine;

            lblStatus2.Text = "Updating Season Stats";

            RecordsAffected = GameData.UpdateSeasonStats(SeasonInfo.AñoInicio, SeasonInfo.Phase);
            lblStatus2.Text += ": " + RecordsAffected.ToString() + " records";
            txtLog1.Text += "Updating Season Stats (Should Return ZERO): " + RecordsAffected.ToString() + " records" + Environment.NewLine;

            lblStatus2.Text = "Updating Standing";

            RecordsAffected = GameData.UpdateStandingStats(SeasonInfo.AñoInicio, SeasonInfo.Phase);
            lblStatus2.Text += ": " + RecordsAffected.ToString() + " records";
            txtLog1.Text += "Updating Standing: " + RecordsAffected.ToString() + " records" + Environment.NewLine;

            lblStatus2.Text = "Updating Streak...";

            RecordsAffected = GameData.UpdateStreak(SeasonInfo.AñoInicio, SeasonInfo.Phase);
            lblStatus2.Text += ": " + RecordsAffected.ToString() + " records";
            txtLog1.Text += "Updating Streak: " + RecordsAffected.ToString() + " records" + Environment.NewLine;

            lblStatus2.Text = "Updating Last 10 Games Record...";

            RecordsAffected = GameData.UpdateLast10GamesRecord(SeasonInfo.AñoInicio, SeasonInfo.Phase);
            lblStatus2.Text += ": " + RecordsAffected.ToString() + " records";
            txtLog1.Text += "Updating Last 10 Games Record: " + RecordsAffected.ToString() + " records" + Environment.NewLine;

            lblStatus.Text = "Done!";
            lblStatus2.Text = "Updates completed!";
            txtLog1.Text += "Process Completed." + Environment.NewLine;
        }

        protected void btSumarizar_Click(object sender, EventArgs e)
        {
            SetupSeasonInfo();
            Sumarizacion();
        }

        protected void btUpdateTeamID_Click(object sender, EventArgs e)
        {
            BeisbolDataEntities db = new BeisbolDataEntities();
            var datos = db.Pitcher_Game_Stats.Where(m => m.SeasonID == 2011 && m.PhaseID == 1)
                .OrderBy(m => m.GameID).ToList();
            int i = 0;
            int j = 0;
            foreach (var item in datos)
            {
                i++;
                BeisbolData.Data.PlayerRepository.UpdateCurrentTeam(item.PlayerID, item.TeamID);
            }

            var datos2 = db.Player_Game_Stats.Where(m => m.SeasonID == 2011 && m.PhaseID == 1)
                .OrderBy(m => m.GameID).ToList();
            foreach (var item in datos2)
            {
                j++;
                BeisbolData.Data.PlayerRepository.UpdateCurrentTeam(item.PlayerID, item.TeamID);
            }
            txtLog1.Text += "Updated " + i.ToString() + " Pitcher records" + Environment.NewLine;
            txtLog1.Text += "Updated " + j.ToString() + " Player records" + Environment.NewLine;
            lblStatus.Text = "Completed!";
            lblStatus2.Text = "Done!";
        }

    }
}
