﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.IO;
using System.Data;
using System.Xml;

using BeisbolData.Data;
using BeisbolData.Data.Model;

namespace BeisbolData_Web_XML_Database_Update
{
    public class GameData
    {
        private class PitcherError
        {
            public int IDMLB { get; set; }
            public int Errors { get; set; }
        }

        public DateTime Fecha { get; set; }
        public string Team1 { get; set; }
        public string Team2 { get; set; }
        public int Numero { get; set; }
        public Boolean Valid { get; private set; }
        public string URL { get; set; }

        private string Juego;
        private List<string> Teams = new List<string> { "esc", "tor", "agu", "lic", "est", "gig" };

        private string UrlJuego
        {
            get { return URL + Juego; }
        }

        private int TablaAtBats
        {
            get
            {
                if (dsGE == null) ReadGameEventsIntoDataset();
                return dsGE.Tables[2].Columns.Count >= 15 ? 2 : 3;
            }
        }

        private Boolean BoxScoreDisponible;
        private Boolean ExtraInning;

        private string WhereAmI = "Init";

        private List<PitcherError> ErroresPitcher;

        //Important Definitions for Game Processing.  Global to the class but private outside.
        BeisbolDataEntities db;
        int PitcherAway, PitcherHome;
        Team Home, Away;
        Game gamea; //Away Team Game Info
        Game gameh; //Home Team Game Info
        Calendar cal;
        DataSet ds, dsGE;

        //GameEvents Files
        string GameEventsDownloadFile = "C:\\temp\\gameevents.xml";
        string GameEventsNewFile = "C:\\temp\\gameevents2.xml";

        public GameData(string data, string url)
        {
            try
            {
                string[] datos = data.Split(Convert.ToChar("_"));
                Fecha = new DateTime(int.Parse(datos[1]), int.Parse(datos[2]), int.Parse(datos[3]));
                Team1 = datos[4].Substring(0, 3);
                Team2 = datos[5].Substring(0, 3);
                //Team2 = datos.ElementAt(5);
                Numero = int.Parse(datos[6]);
                Juego = data;
                URL = url;
                //GameEventsDownloadFile=System.Web.HttpContext.Current.Request.PhysicalApplicationPath + @"temp\gameevents.xml";
                //GameEventsDownloadFile = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + @"temp\gameevents2.xml";
                GameEventsDownloadFile = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + @"temp\gameevents.xml";
                GameEventsNewFile = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + @"temp\gameevents2.xml";
                Validate();
            }
            catch (Exception)
            {
                SetInvalid();
                //throw;
            }
        }

        private void SetInvalid()
        {
            Valid = false;
            Team1 = string.Empty; Team2 = null; Numero = 0;
        }

        private void Validate()
        {
            if (Teams.Contains(Team1) && Teams.Contains(Team2))
                Valid = true;
            else
                SetInvalid();
        }

        #region Helper Functions and Methods
        private int GetGameTime(string data)
        {
            const string SearchString = "<b>T</b>:";
            int pos = data.IndexOf(SearchString);
            if (pos >= 0)
            {
                pos += SearchString.Length + 1;
                return GetMinutes_From_HourMinuteFormat(data.Substring(pos));
            }
            else
            {
                return 0;
            }
        }

        private int GetMinutes_From_HourMinuteFormat(string data)
        {
            string[] hm = data.Trim().Split(Convert.ToChar(":"));
            int Horas = 0;
            int Minutos = 0;
            Boolean BHoras = false;
            Boolean BMinutos = false;
            BHoras = Int32.TryParse(hm[0], out Horas);
            BMinutos = Int32.TryParse(hm[1], out Minutos);
            if (!BMinutos)
            {
                hm[1] = hm[1].Substring(0, 2);
                BMinutos = Int32.TryParse(hm[1], out Minutos);
            }
            if (BMinutos && BHoras)
                return hm.Length == 2 ? (Horas * 60) + Minutos : 0;
            else
                return 0;
            //return hm.Length == 2 ? (Convert.ToInt32(hm[0]) * 60) + Convert.ToInt32(hm[1]) : 0;
        }

        private int GetMinutes_From_HourMinuteFormat(string data, string ampm)
        {
            string[] hm = data.Trim().Split(Convert.ToChar(":"));
            if (ampm.ToLower() == "pm")
            {
                return hm.Length == 2 ? ((Convert.ToInt32(hm[0]) + 12) * 60) + Convert.ToInt32(hm[1]) : 0;
            }
            else
            {
                return hm.Length == 2 ? (Convert.ToInt32(hm[0]) * 60) + Convert.ToInt32(hm[1]) : 0;
            }
        }

        private int GetInt32Data(DataSet ds, int Tabla, int Fila, string Columna)
        {
            return GetInt32Data(ds.Tables[Tabla].Rows[Fila], Columna);
        }
        private int GetInt32Data(DataRow DR, string Columna)
        {
            try
            {
                return Convert.ToInt32(DR[Columna].ToString());
            }
            catch (Exception)
            {
                return 0;
                //throw;
            }
        }

        private void GetDoublePlays(ref int dpa, ref int dph)
        {
            WhereAmI = "GetDoublePlays";
            if (dsGE == null)
                ReadGameEventsIntoDataset();
            foreach (DataRow Fila in dsGE.Tables[TablaAtBats].Rows)
            {
                if (Fila["des"].ToString().ToLower().IndexOf("double play") >= 0 ||
                    Fila["event"].ToString().ToUpper().IndexOf("DP") >= 0)
                {
                    if (Fila["teamjn"].ToString() == "A")
                        dph++;
                    else
                        dpa++;
                }
            }
        }

        private Boolean HasData(DataRow DR, string Columna)
        {
            return DR[Columna].ToString().Length > 0;
        }

        private Boolean AreRunnersInScoringPosition(DataRow DR)
        {
            if (DR == null)
                return false;
            if (HasData(DR, "b2") || HasData(DR, "b3"))
                return true;
            else
                return false;
        }

        private Boolean AreRunnersOn(DataRow DR)
        {
            if (DR == null)
                return false;
            if (HasData(DR, "b1") || AreRunnersInScoringPosition(DR))
                return true;
            else
                return false;
        }

        private Boolean IsEventHit(string evento)
        {
            if (evento.ToLower() == "single" || evento.ToLower() == "double" ||
                evento.ToLower() == "triple" || evento.ToLower() == "home run")
                return true;
            else
                return false;
        }

        private string GetSituationBase(DataRow DR, string Base)
        {
            return HasData(DR, Base) ? "1" : "0";
        }

        private string GetSituationID(DataRow DR)
        {
            if (DR == null)
                return "S000";
            else
                return "S" + GetSituationBase(DR, "b1") + GetSituationBase(DR, "b2") + GetSituationBase(DR, "b3");
        }

        private string GetFilePathForParsedGameEventsXML(string URL)
        {
            return "";
        }

        private void SetGameAgrValuesAtZero(Game game)
        {
            game.SB = 0;
            game.CS = 0;
            game.RISPAB = 0;
            game.RISPH = 0;
            game.RISPRBI = 0;
            game.ROBAB = 0;
            game.ROBH = 0;
            game.IBB = 0;
            game.HBP = 0;
            game.OUT2RBI = 0;
            game.GIDP = 0;
            game.S = 0;
            game.SF = 0;
            game.PUTOUT = 0;
            game.PASSEDBALL = 0;
            game.ASSIST = 0;
            game.PICKEDOFF = 0;
            game.P_IBB = 0;
            game.P_HBP = 0;
            game.P_BALK = 0;
            game.P_WP = 0;
            game.P_SV = 0;
            game.P_PICKOFF = 0;
            game.P_E = 0;
            game.P_GROUND = 0;
            game.P_FLY = 0;
            game.P_BATSFACED = 0;
            game.P_BATSCORED = 0;
            game.P_BATSINH = 0;
            game.P_HOLDS = 0;
            game.P_SB = 0;
            game.P_BLOWNSAVES = 0;
            game.P_Innings = 0;
            //Added 28-9-2011
            game.FLY = 0;
            game.GROUND = 0;
            game.P_S = 0;
            game.P_SF = 0;
            game.P_GIDP = 0;
            game.P_CG = 0;
            game.P_SHO = 0;
            //Added 8-10-2011
            game.C_CS = 0;
            //Added 28-10-2011
            game.C_SB = 0;
            game.P_CS = 0;
            game.P_QS = 0;
            //Added 10-11-2011
            game.P_TeamSHO = 0;
        }

        //private int GetStreakValue(int W, int streak)
        //{
        //    if (W == 1)
        //    {
        //        if (streak > 0)
        //            return ++streak;
        //        else
        //            return 1;
        //    }
        //    else
        //    {
        //        if (streak < 0)
        //            return --streak;
        //        else
        //            return -1;
        //    }
        //}

        #endregion

        private void ProcessGameEvents(Player_Game_Stat PlayerGS, int IDMLB)
        {
            WhereAmI = "ProcessGameEvents Batter";
            if (dsGE == null)
                ReadGameEventsIntoDataset();
            //Determine which table is AtBats and which is Stops
            DataRow FilaAnterior = null;
            PlayerGS.RISPAB = 0;
            PlayerGS.RISPH = 0;
            PlayerGS.RISPRBI = 0;
            PlayerGS.ROBAB = 0;
            PlayerGS.ROBH = 0;
            PlayerGS.IBB = 0;
            PlayerGS.OUT2RBI = 0;
            PlayerGS.GIDP = 0;
            PlayerGS.S = 0;
            PlayerGS.PASSEDBALL = 0;
            PlayerGS.CS = 0;
            PlayerGS.PICKEDOFF = 0;
            //Added 26-9-2011
            PlayerGS.GROUND = 0;
            PlayerGS.FLY = 0;
            //Added 8-10-2011
            PlayerGS.C_CS = 0;
            //Added 28-10-2011
            PlayerGS.C_SB = 0;
            //End Added
            // Table 2 ATBATS first
            foreach (DataRow Fila in dsGE.Tables[TablaAtBats].Rows)
            {
                //if (GetInt32Data(Fila, "batter") == PlayerGS.Player.IDMLB)
                // AT BATS Information is Checked here
                if (GetInt32Data(Fila, "batter") == IDMLB)
                {
                    string Evento = Fila["event"].ToString().ToLower();
                    if (AreRunnersInScoringPosition(FilaAnterior))
                    {
                        PlayerGS.RISPAB++;
                        if (IsEventHit(Evento))
                        {
                            PlayerGS.RISPH++;
                        }
                        else if (Evento.IndexOf("walk") >= 0 ||
                            Evento.IndexOf("hit by pitch") >= 0 ||
                            Evento.IndexOf("sac ") >= 0)
                        {
                            PlayerGS.RISPAB--;
                        }
                        if (GetInt32Data(Fila, "rbi") >= 0)
                            PlayerGS.RISPRBI += GetInt32Data(Fila, "rbi");
                    }
                    if (AreRunnersOn(FilaAnterior))
                    {
                        PlayerGS.ROBAB++;
                        if (IsEventHit(Evento))
                            PlayerGS.ROBH++;
                    }
                    if (Evento == "intent walk")
                        PlayerGS.IBB++;
                    if (GetInt32Data(FilaAnterior, "o") == 2)
                    {
                        PlayerGS.OUT2RBI += GetInt32Data(Fila, "rbi");
                    }
                    if (Fila["des"].ToString().ToLower().IndexOf("double play") >= 0 ||
                        Evento.ToUpper().IndexOf("DP") >= 0)
                    {
                        PlayerGS.GIDP++;
                    }
                    if (Evento.IndexOf("sac bunt") >= 0)
                        PlayerGS.S++;

                    //GAME EVENTS ANALYSIS FOR GROUND AND FLY BALLS
                    //Added 26-9-2011
                    //Same as Pitcher in its ProcessGameEvents

                    if (Evento.IndexOf("pop out") >= 0 || Evento.IndexOf("flyout") >= 0
                        || Evento.IndexOf("fly") >= 0 || Evento.IndexOf("lineout") >= 0) //Added 15-9-2011
                    {
                        PlayerGS.FLY++;
                    }
                    else if (Evento.IndexOf("groundout") >= 0 || Evento.IndexOf("grounded") >= 0
                        || Evento.IndexOf("sac bunt") >= 0) //Added Grounded 15-9-2011 - Sac Bunt 19-9-2011
                    {
                        PlayerGS.GROUND++;
                    }
                    else if (IsEventHit(Evento))
                    {
                        //if (Fila["des"].ToString().ToLower().IndexOf("groundball") >= 0)
                        if (Fila["des"].ToString().ToLower().IndexOf("ground") >= 0) //15-9-2011
                            PlayerGS.GROUND++;
                        else
                            PlayerGS.FLY++;
                    }
                    //Added 26-9-2011 - 15-9-2011
                    else
                    {
                        string EventoDes = Fila["des"].ToString().ToLower();
                        if (EventoDes.IndexOf("ground") >= 0 || EventoDes.IndexOf("sacrifice bunt") >= 0) //Bunt added 26-9-2011 - 19-9-2011
                            PlayerGS.GROUND++;
                        else if (EventoDes.IndexOf("lines out") >= 0 || EventoDes.IndexOf("flies out") >= 0 ||
                            EventoDes.IndexOf("pops out") >= 0)
                            PlayerGS.FLY++; //End Added
                        else
                        { //Added 26-9-2011 - 19-9-2011
                            if (EventoDes.IndexOf("reaches on fielding error") >= 0 ||
                                EventoDes.IndexOf("reaches on throwing error") >= 0)
                                PlayerGS.GROUND++;
                        }
                    }

                    //END ANALYSIS

                }
                // STOPS Information is checked here
                if (GetInt32Data(Fila, "player") == IDMLB)
                {
                    string Evento = Fila["event"].ToString().ToLower();
                    if (Evento.IndexOf("caught stealing") >= 0 ||
                        Evento.IndexOf("runner out") >= 0)
                    {
                        PlayerGS.CS++;
                    }
                    if ((Evento.IndexOf("picked off stealing") >= 0 ||
                        Evento.IndexOf("pickoff") >= 0) && Evento.IndexOf("pickoff error") <= 0)
                    {
                        PlayerGS.PICKEDOFF++;
                    }
                }
                //FilaAnterior debe ser Null cuando hicieron el 3er out o cambio de Inning
                if (GetInt32Data(Fila, "o") == 3)
                    FilaAnterior = null;
                else
                    FilaAnterior = Fila;
            }
        }

        private void ProcessGameEvents(Pitcher_Game_Stat PitcherGS, int IDMLB)
        {
            WhereAmI = "ProcessGameEvents Pitcher";
            if (dsGE == null)
                ReadGameEventsIntoDataset();
            //Setup Data
            PitcherGS.P_IBB = 0;
            PitcherGS.P_HBP = 0;
            PitcherGS.P_GROUND = 0;
            PitcherGS.P_FLY = 0;
            //Added 28-9-2011
            PitcherGS.P_S = 0;
            PitcherGS.P_SF = 0;
            PitcherGS.P_GIDP = 0;
            PitcherGS.P_CG = 0;
            PitcherGS.P_SHO = 0;
            foreach (DataRow Fila in dsGE.Tables[TablaAtBats].Rows)
            {
                if (GetInt32Data(Fila, "pitcher") == IDMLB)
                {
                    string Evento = Fila["event"].ToString().ToLower();
                    if (Evento == "intent walk")
                        PitcherGS.P_IBB++;
                    if (Evento == "hit by pitch")
                        PitcherGS.P_HBP++;
                    //Added 28-9-2011
                    if (Fila["des"].ToString().ToLower().IndexOf("double play") >= 0 ||
                        Evento.ToUpper().IndexOf("DP") >= 0)
                    {
                        PitcherGS.P_GIDP++;
                    }
                    if (Fila["des"].ToString().ToLower().IndexOf("sacrifice fly") >= 0 ||
                        Evento.ToLower().IndexOf("sac fly") >= 0)
                    {
                        PitcherGS.P_SF++;
                    }
                    if (Evento.IndexOf("sac bunt") >= 0)
                        PitcherGS.P_S++;
                    //End Added 28-9-2011
                    if (Evento.IndexOf("pop out") >= 0 || Evento.IndexOf("flyout") >= 0
                        || Evento.IndexOf("fly") >= 0 || Evento.IndexOf("lineout") >= 0) //Added 15-9-2011
                    {
                        PitcherGS.P_FLY++;
                    }
                    else if (Evento.IndexOf("groundout") >= 0 || Evento.IndexOf("grounded") >= 0
                        || Evento.IndexOf("sac bunt") >= 0) //Added Grounded 15-9-2011 - Sac Bunt 19-9-2011
                    {
                        PitcherGS.P_GROUND++;
                    }
                    else if (IsEventHit(Evento))
                    {
                        //if (Fila["des"].ToString().ToLower().IndexOf("groundball") >= 0)
                        if (Fila["des"].ToString().ToLower().IndexOf("ground") >= 0) //15-9-2011
                            PitcherGS.P_GROUND++;
                        else
                            PitcherGS.P_FLY++;
                    }
                    //Added 15-9-2011
                    else
                    {
                        string EventoDes = Fila["des"].ToString().ToLower();
                        if (EventoDes.IndexOf("ground") >= 0 || EventoDes.IndexOf("sacrifice bunt") >= 0) //Bunt added 19-9-2011
                            PitcherGS.P_GROUND++;
                        else if (EventoDes.IndexOf("lines out") >= 0 || EventoDes.IndexOf("flies out") >= 0 ||
                            EventoDes.IndexOf("pops out") >= 0)
                            PitcherGS.P_FLY++; //End Added
                        else
                        { //Added 19-9-2011
                            if (EventoDes.IndexOf("reaches on fielding error") >= 0 ||
                                EventoDes.IndexOf("reaches on throwing error") >= 0)
                                PitcherGS.P_GROUND++;
                        }
                    }
                }
            }
        }

        private void ReadBoxScoreIntoDataset()
        {
            ds = new DataSet();
            XmlTextReader myXMLReader = new XmlTextReader(UrlJuego + "/boxscore.xml");
            ds.ReadXml(myXMLReader);
        }

        private void ReadGameEventsIntoDataset()
        {
            WhereAmI = "ReadGameEventsIntoDataset";
            int Inning = 0, Numero = 0;
            string TeamHA = string.Empty, B1 = "", B2 = "", B3 = "", Description = string.Empty;
            Boolean IsAction = false;
            WebClient myClient = new WebClient();
            myClient.DownloadFile(UrlJuego + "/game_events.xml", GameEventsDownloadFile);
            myClient.Dispose();
            StreamReader myReader = new StreamReader(GameEventsDownloadFile);
            StreamWriter myWriter = new StreamWriter(GameEventsNewFile);
            string Linea;
            //while (myReader.Peek()>=0)
            while (myReader.EndOfStream == false)
            {
                Linea = myReader.ReadLine();
                if (Linea.ToLower().IndexOf("<inning") >= 0)
                    Inning++;
                if (Linea.ToLower().IndexOf("<top>") >= 0)
                    TeamHA = "A";
                else if (Linea.ToLower().IndexOf("<bottom>") >= 0)
                    TeamHA = "H";
                if (Linea.ToLower().IndexOf("<atbat") >= 0 || Linea.ToLower().IndexOf("<action") >= 0)
                {
                    Numero++;
                    if (Linea.ToLower().IndexOf("<action") >= 0)
                    {
                        IsAction = true;
                        Linea = Linea.Replace("<action", "<atbat");
                    }
                    else
                    {
                        IsAction = false;
                    }
                    Linea += " numjn=\"" + Numero.ToString() + "\" teamjn=\"" + TeamHA +
                        "\" inningjn=\"" + Inning.ToString() + "\"";
                }
                if (Linea.ToLower().IndexOf("b1=") >= 0)
                {
                    if (Linea.ToLower().IndexOf("b1=\"\"") >= 0)
                        B1 = "";
                    else
                        B1 = "100";
                }
                if (Linea.ToLower().IndexOf("b2=") >= 0)
                {
                    if (Linea.ToLower().IndexOf("b2=\"\"") >= 0)
                        B2 = "";
                    else
                        B2 = "010";
                }
                if (Linea.ToLower().IndexOf("b3=") >= 0)
                {
                    if (Linea.ToLower().IndexOf("b3=\"\"") >= 0)
                        B3 = "";
                    else
                        B3 = "001";
                }
                #region IsAction
                if (IsAction)
                {
                    if (Linea.ToLower().IndexOf("des=\"") >= 0)
                        Description = Linea.ToLower();
                    //Caught Stealing Event
                    if (Linea.ToLower().IndexOf("event=\"") >= 0)
                    {
                        if (Linea.ToLower().IndexOf("caught stealing 2b") >= 0)
                            B1 = "";
                        if (Linea.ToLower().IndexOf("caught stealing 3b") >= 0)
                            B2 = "";
                        if (Linea.ToLower().IndexOf("caught stealing h") >= 0)
                            B3 = "";
                    }
                    //Balk Event
                    if (Linea.ToLower().IndexOf("balk") >= 0 && Linea.ToLower().IndexOf("event=\"") >= 0)
                    {
                        B1 = ""; B2 = ""; B3 = "";
                        if (Description.IndexOf("advances to 2nd") >= 0)
                            B2 = "010";
                        if (Description.IndexOf("advances to 3rd") >= 0)
                            B3 = "001";
                    }
                    //Wild Pitch Event
                    if (Linea.ToLower().IndexOf("wild pitch") >= 0 && Linea.ToLower().IndexOf("event=\"") >= 0)
                    {
                        if (Description.IndexOf("to 2nd") >= 0)
                        {
                            B1 = "";
                            B2 += "010X";
                        }
                        if (Description.IndexOf("to 3rd") >= 0)
                        {
                            B3 += "001X";
                            if (B2.Length <= 3)
                                B2 = "";
                        }
                        if (Description.IndexOf("scores") >= 0)
                        {
                            if (B3.Length <= 3)
                                B3 = "";
                        }
                    }
                    //Passed Ball Event
                    if (Linea.ToLower().IndexOf("passed ball") >= 0 && Linea.ToLower().IndexOf("event=\"") >= 0)
                    {
                        if (Description.IndexOf("to 2nd") >= 0)
                        {
                            B1 = "";
                            B2 += "010X";
                        }
                        if (Description.IndexOf("to 3rd") >= 0)
                        {
                            B3 += "001X";
                            if (B2.Length <= 3)
                                B2 = "";
                        }
                        if (Description.IndexOf("scores") >= 0)
                        {
                            if (B3.Length <= 3)
                                B3 = "";
                        }
                    }
                    //Stolen Base Event
                    if (Linea.ToLower().IndexOf("steals") >= 0)
                    {
                        if (Linea.ToLower().IndexOf("2nd base") >= 0)
                        {
                            if (Linea.ToLower().IndexOf("advances to") >= 0)
                            {
                                B1 = "";
                                B2 = "";
                                if (Linea.ToLower().IndexOf("3rd") >= 0)
                                    B3 += "001X";
                                else
                                    B3 = "";
                            }
                            else
                            {
                                B1 = "";
                                B2 += "010X";
                            }
                        }
                        if (Linea.ToLower().IndexOf("3rd base") >= 0)
                        {
                            if (Linea.ToLower().IndexOf("advances") >= 0 || Linea.ToLower().IndexOf("scores") >= 0)
                            {
                                B3 = "";
                            }
                            else
                            {
                                B3 = "001X";
                                if (B2.Length <= 3)
                                    B2 = "";
                            }
                        }
                    }
                    // Pickoff Event
                    if ((Linea.ToLower().IndexOf("pickoff") >= 0 || Linea.ToLower().IndexOf("picked off") >= 0)
                        && Linea.ToLower().IndexOf("event=\"") >= 0)
                    {
                        if ((Linea.ToLower().IndexOf("stealing 2b") >= 0 || Linea.ToLower().IndexOf("off 1b") >= 0)
                            && Linea.ToLower().IndexOf("error") < 0)
                        {
                            B1 = "";
                        }
                        if ((Linea.ToLower().IndexOf("stealing 3b") >= 0 || Linea.ToLower().IndexOf("off 2b") >= 0)
                            && Linea.ToLower().IndexOf("error") < 0)
                        {
                            B2 = "";
                        }
                        if ((Linea.ToLower().IndexOf("stealing h") >= 0 || Linea.ToLower().IndexOf("off 3b") >= 0)
                            && Linea.ToLower().IndexOf("error") < 0)
                        {
                            B3 = "";
                        }
                    }
                    // Pickoff Error Event
                    if (Linea.ToLower().IndexOf("pickoff error") >= 0 && Linea.ToLower().IndexOf("event=\"") >= 0)
                    {
                        if (Description.IndexOf("to 2nd") >= 0)
                        {
                            B1 = "";
                            B2 += "010X";
                        }
                        if (Description.IndexOf("to 3rd") >= 0)
                        {
                            B3 += "001X";
                            if (B2.Length <= 3)
                                B2 = "";
                        }
                        if (Description.IndexOf("scores") >= 0)
                        {
                            if (B3.Length <= 3)
                                B3 = "";
                        }
                    }
                    //Set Runners on Base information on Action Event
                    if (Linea.ToLower().IndexOf("player=\"") >= 0)
                    {
                        Linea += " b1=\"" + B1 + "\" b2=\"" + B2 + "\" b3=\"" + B3 + "\"";
                    }
                }
                #endregion
                //Write the line to the output stream
                myWriter.WriteLine(Linea);
            }
            //Cleanup
            myWriter.Flush();
            myWriter.Close();
            myWriter.Dispose();
            myReader.Close();
            myReader.Dispose();
            //Read new XML File GameEvents
            dsGE = new DataSet();
            dsGE.ReadXml(GameEventsNewFile);
        }

        private void ProcessCalendar()
        {
            // Calendar
            WhereAmI = "ProcessCalendar";

            //if (ds == null)
            //    ReadBoxScoreIntoDataset();

            CalendarRepository cr = new CalendarRepository(db);
            TeamRepository tr = new TeamRepository(db);
            StandingRepository sr = new StandingRepository(db);

            //DataSet ds2 = new DataSet();
            //XmlTextReader myXMLReader = new XmlTextReader(UrlJuego + "/game.xml");
            //ds2.ReadXml(myXMLReader);

            DataSet ds2 = new DataSet();
            XmlTextReader myXMLReader = new XmlTextReader(UrlJuego + "/linescore.xml");
            ds2.ReadXml(myXMLReader);

            DateTime FechaJuego = Fecha.AddMinutes(GetMinutes_From_HourMinuteFormat(ds2.Tables[0].Rows[0]["time"].ToString(), ds2.Tables[0].Rows[0]["ampm"].ToString()));
            Home = tr.GetTeamBy_MLB_ShortName(ds2.Tables[0].Rows[0]["home_code"].ToString());
            Away = tr.GetTeamBy_MLB_ShortName(ds2.Tables[0].Rows[0]["away_code"].ToString());
            //Check if the game is in the calendar (with MLB Data)
            Boolean CalNotFound = false;
            cal = cr.GetCalendarByDateTimeAndTeam(SeasonInfo.AñoInicio, SeasonInfo.Phase, FechaJuego, Away.ID, Home.ID, this.Numero);
            //cal = cr.GetCalendarByDateTimeAndTeam(SeasonInfo.AñoInicio, SeasonInfo.Phase, FechaJuego, Away.ID, Home.ID);
            CalNotFound = cal == null;
            if (CalNotFound)
            {
                cal = new Calendar();
                cal.SeasonID = SeasonInfo.AñoInicio;
                cal.PhaseID = SeasonInfo.Phase;
                cal.GameID = SeasonRepository.GetNewGameID(SeasonInfo.AñoInicio);
                cal.Team1 = Away.ID;
                cal.Team2 = Home.ID;
            }
            //Properties Update
            string statusgame, statusDescription;
            cal.Descripcion = Away.Nombre + " vs " + Home.Nombre; //Added 14-9-2011
            //19-9-2011 -> Solicitado por Arturo para Dynamic Data
            cal.Descripcion = cal.GameID.ToString() + " - " + FechaJuego.Day.ToString() + "-" + FechaJuego.Month.ToString() + "-"
                + FechaJuego.Year.ToString() + " - " + Away.NombreCorto + " vs " + Home.NombreCorto;
            cal.Fecha = FechaJuego;
            cal.Noche = FechaJuego.Hour >= 19;
            statusgame = ds2.Tables[0].Rows[0]["ind"].ToString().ToUpper();
            statusDescription = ds2.Tables[0].Rows[0]["status"].ToString();
            if (BoxScoreDisponible)
                cal.GameTime = GetGameTime(ds.Tables[0].Rows[0]["game_info"].ToString());
            else
                cal.GameTime = 0;
            if (statusgame == "F" || statusgame == "FO" || statusDescription.Trim().ToLower() == "game over" ||
                (cal.GameTime > 0 && statusDescription.ToLower().Trim() == "preview"))
            {
                cal.Official = true;
                cal.Status = "P";
            }
            else
            {
                cal.Official = false;
                cal.Status = "N";
            }
            cal.EstadioID = Convert.ToInt32(Home.EstadioID);
            cal.PostPoned = statusDescription.ToLower().Trim() == "postponed";
            cal.StatusDescription_MLB = statusDescription;
            cal.GameNumber_MLB = Numero;
            if (CalNotFound)
            {
                //Add the new cal if it was not found
                cr.Add(cal);
            }
            //Make Sure the teams are in the standing
            sr.CreateDefaultStanding(SeasonInfo.AñoInicio, SeasonInfo.Phase, Home.ID);
            sr.CreateDefaultStanding(SeasonInfo.AñoInicio, SeasonInfo.Phase, Away.ID);
            //Clean up Dataset 2 ds2
            ds2.Dispose();
        }

        //private void ProcessCalendarOLD()
        //{
        //    // Calendar
        //    WhereAmI = "ProcessCalendar";

        //    if (ds == null)
        //        ReadBoxScoreIntoDataset();

        //    CalendarRepository cr = new CalendarRepository(db);
        //    TeamRepository tr = new TeamRepository(db);

        //    DataSet ds2 = new DataSet();
        //    XmlTextReader myXMLReader = new XmlTextReader(UrlJuego + "/game.xml");
        //    ds2.ReadXml(myXMLReader);

        //    DateTime FechaJuego = Fecha.AddMinutes(GetMinutes_From_HourMinuteFormat(ds2.Tables[0].Rows[0]["local_game_time"].ToString()));
        //    Home = tr.GetTeamBy_MLB_ShortName(ds.Tables[0].Rows[0]["home_team_code"].ToString());
        //    Away = tr.GetTeamBy_MLB_ShortName(ds.Tables[0].Rows[0]["away_team_code"].ToString());
        //    //Check if the game is in the calendar
        //    cal = cr.GetCalendarByDateAndTeam(SeasonInfo.AñoInicio, SeasonInfo.Phase, FechaJuego, Away.ID, Home.ID);
        //    if (cal == null)
        //    {
        //        cal = new Calendar();
        //        cal.SeasonID = SeasonInfo.AñoInicio;
        //        cal.PhaseID = SeasonInfo.Phase;
        //        cal.GameID = SeasonRepository.GetNewGameID(SeasonInfo.AñoInicio);
        //        cal.Team1 = Away.ID;
        //        cal.Team2 = Home.ID;
        //        cal.Fecha = FechaJuego;
        //        cal.Official = ds.Tables[0].Rows[0]["status_ind"].ToString().ToUpper() == "F";
        //        cal.GameTime = GetGameTime(ds.Tables[0].Rows[0]["game_info"].ToString());
        //        cal.Status = ds.Tables[0].Rows[0]["status_ind"].ToString().ToUpper() == "F" ? "P" : "C";
        //        cal.EstadioID = Convert.ToInt32(Home.EstadioID);
        //        cr.Add(cal);
        //    }
        //    //Clean up Dataset 2 ds2
        //    ds2.Dispose();
        //}

        private void ProcessBoxScore()
        {
            WhereAmI = "ProcessBoxScore";

            if (cal == null)
                throw new Exception("ProcessBoxScore.  Calendar Object cal not available.");

            if (Home == null || Away == null)
                throw new Exception("ProcessBoxScore.  Team Objects Home, Away not available.");

            if (ds == null)
                ReadBoxScoreIntoDataset();

            // BoxScore
            ExtraInning = false;
            BoxScoreRepository bxr = new BoxScoreRepository(db);
            BoxScore pizarra = new BoxScore();
            foreach (DataRow Fila in ds.Tables[2].Rows)
            {
                pizarra = new BoxScore();
                pizarra.SeasonID = SeasonInfo.AñoInicio;
                pizarra.PhaseID = SeasonInfo.Phase;
                pizarra.GameID = cal.GameID;
                pizarra.Inning = int.Parse(Fila["inning"].ToString());
                pizarra.TeamID = Away.ID;
                pizarra.Half = 0; //Top half of the inning
                try
                {
                    pizarra.R = Convert.ToInt32(Fila["away"].ToString());
                }
                catch (Exception)
                {
                }
                bxr.Add(pizarra);
                if (pizarra.Inning == 10)
                    ExtraInning = true;
                pizarra = new BoxScore();
                pizarra.SeasonID = SeasonInfo.AñoInicio;
                pizarra.PhaseID = SeasonInfo.Phase;
                pizarra.GameID = cal.GameID;
                pizarra.Inning = int.Parse(Fila["inning"].ToString());
                pizarra.TeamID = Home.ID;
                pizarra.Half = 1; //Bottom half of the inning
                try
                {
                    pizarra.R = Convert.ToInt32(Fila["home"].ToString());
                }
                catch (Exception)
                {
                }
                bxr.Add(pizarra);
            }
        }

        private void ProcessGameAndStandingInformation()
        {
            WhereAmI = "ProcessGameInformation";

            if (cal == null)
                throw new Exception("ProcessGameAndStandingInformation.  Calendar Object cal not available.");

            if (Home == null || Away == null)
                throw new Exception("ProcessGameAndStandingInformation.  Team Objects Home, Away not available.");

            if (ds == null)
                ReadBoxScoreIntoDataset();

            // Game Information
            GameRepository gr = new GameRepository(db);
            gamea = new Game(); //Away Team Game Info
            gameh = new Game(); //Home Team Game Info
            //Set Summarized Values at Zero for each Game
            SetGameAgrValuesAtZero(gamea);
            SetGameAgrValuesAtZero(gameh);
            gamea.SeasonID = SeasonInfo.AñoInicio;
            gamea.PhaseID = SeasonInfo.Phase;
            gamea.GameID = cal.GameID;
            gamea.TeamID = Away.ID;
            gamea.Team_VS = Home.ID;
            gamea.HomeAway = "A";
            gamea.R = GetInt32Data(ds, 1, 0, "away_team_runs");
            gamea.H = GetInt32Data(ds, 1, 0, "away_team_hits");
            gamea.E = GetInt32Data(ds, 1, 0, "away_team_errors");
            gameh.SeasonID = SeasonInfo.AñoInicio;
            gameh.PhaseID = SeasonInfo.Phase;
            gameh.GameID = cal.GameID;
            gameh.TeamID = Home.ID;
            gameh.Team_VS = Away.ID;
            gameh.HomeAway = "H";
            gameh.R = GetInt32Data(ds, 1, 0, "home_team_runs");
            gameh.H = GetInt32Data(ds, 1, 0, "home_team_hits");
            gameh.E = GetInt32Data(ds, 1, 0, "home_team_errors");
            //TODO: Check if Suspended so W and L are not affected.
            if (gamea.R == gameh.R)
            {
                cal.Status = "X";
                gamea.W = 0; gamea.L = 0; gameh.W = 0; gameh.L = 0;
                gamea.W_Home = 0; gamea.L_Home = 0;
                gamea.W_Away = 0; gamea.L_Away = 0;
                gameh.W_Home = 0; gameh.L_Home = 0;
                gameh.W_Away = 0; gameh.L_Away = 0;
                gamea.W_OneRun = 0;
                gamea.L_OneRun = 0;
                gameh.W_OneRun = 0;
                gameh.L_OneRun = 0;
                gamea.W_ExtraInning = 0;
                gamea.L_ExtraInning = 0;
                gameh.W_ExtraInning = 0;
                gameh.L_ExtraInning = 0;
            }
            else
            {
                gamea.W = gamea.R > gameh.R ? 1 : 0;
                gamea.L = gamea.R < gameh.R ? 1 : 0;
                gameh.W = gamea.R > gameh.R ? 0 : 1;
                gameh.L = gamea.R < gameh.R ? 0 : 1;
                gamea.W_Away = gamea.W;
                gamea.L_Away = gamea.L;
                gamea.W_Home = 0;
                gamea.L_Home = 0;
                gameh.W_Away = 0;
                gameh.L_Away = 0;
                gameh.W_Home = gameh.W;
                gameh.L_Home = gameh.L;
                if (gamea.R - gameh.R == 1 || gameh.R - gamea.R == 1)
                {
                    gamea.W_OneRun = gamea.W;
                    gamea.L_OneRun = gamea.L;
                    gameh.W_OneRun = gameh.W;
                    gameh.L_OneRun = gameh.L;
                }
                else
                {
                    gamea.W_OneRun = 0;
                    gamea.L_OneRun = 0;
                    gameh.W_OneRun = 0;
                    gameh.L_OneRun = 0;
                }
                if (ExtraInning)
                {
                    gamea.W_ExtraInning = gamea.W;
                    gamea.L_ExtraInning = gamea.L;
                    gameh.W_ExtraInning = gameh.W;
                    gameh.L_ExtraInning = gameh.L;
                }
                else
                {
                    gamea.W_ExtraInning = 0;
                    gamea.L_ExtraInning = 0;
                    gameh.W_ExtraInning = 0;
                    gameh.L_ExtraInning = 0;
                }
                //Added 9-11-2011 - Shutout Team and Pitcher
                if (gamea.R > 0 && gameh.R == 0)
                {
                    gamea.P_TeamSHO = 1;
                }
                else if (gameh.R > 0 && gamea.R == 0)
                {
                    gameh.P_TeamSHO = 1;
                }
                //End Added 9-11-2011 - Shutout Team and Pitcher
                cal.TeamWon = gamea.W == 1 ? gamea.TeamID : gameh.TeamID;
                cal.TeamLoss = gamea.L == 1 ? gamea.TeamID : gameh.TeamID;
            }

            // Offensive Information
            foreach (DataRow Fila in ds.Tables[5].Rows)
            {
                if (Fila["team_flag"].ToString().ToLower() == "away")
                {
                    gamea.VB = GetInt32Data(Fila, "ab");
                    gamea.H2 = GetInt32Data(Fila, "d");
                    gamea.H3 = GetInt32Data(Fila, "t");
                    gamea.HR = GetInt32Data(Fila, "hr");
                    gamea.RBI = GetInt32Data(Fila, "rbi");
                    gamea.LOB = GetInt32Data(Fila, "lob");
                    gamea.SO = GetInt32Data(Fila, "so");
                    gamea.BB = GetInt32Data(Fila, "bb");
                    //Se tiene que sumarizar
                    //gamea.PUTOUT = 0; // GetInt32Data(Fila, "po");
                }
                else if (Fila["team_flag"].ToString().ToLower() == "home")
                {
                    gameh.VB = GetInt32Data(Fila, "ab");
                    gameh.H2 = GetInt32Data(Fila, "d");
                    gameh.H3 = GetInt32Data(Fila, "t");
                    gameh.HR = GetInt32Data(Fila, "hr");
                    gameh.RBI = GetInt32Data(Fila, "rbi");
                    gameh.LOB = GetInt32Data(Fila, "lob");
                    gameh.SO = GetInt32Data(Fila, "so");
                    gameh.BB = GetInt32Data(Fila, "bb");
                    //Se tiene que sumarizar
                    //gameh.PUTOUT = 0; // GetInt32Data(Fila, "po");
                }
                else
                    throw new Exception("Boxscore table 5 problem. Team_Flag description is: " +
                        Fila["team_flag"].ToString());
            }

            //Pitching Information
            foreach (DataRow Fila in ds.Tables[3].Rows)
            {
                if (Fila["team_flag"].ToString().ToLower() == "away")
                {
                    gamea.P_H = GetInt32Data(Fila, "h");
                    gamea.P_HR = GetInt32Data(Fila, "hr");
                    gamea.P_R = GetInt32Data(Fila, "r");
                    gamea.P_ER = GetInt32Data(Fila, "er");
                    gamea.P_SO = GetInt32Data(Fila, "so");
                    gamea.P_BB = GetInt32Data(Fila, "bb");
                }
                else if (Fila["team_flag"].ToString().ToLower() == "home")
                {
                    gameh.P_H = GetInt32Data(Fila, "h");
                    gameh.P_HR = GetInt32Data(Fila, "hr");
                    gameh.P_R = GetInt32Data(Fila, "r");
                    gameh.P_ER = GetInt32Data(Fila, "er");
                    gameh.P_SO = GetInt32Data(Fila, "so");
                    gameh.P_BB = GetInt32Data(Fila, "bb");
                }
                else
                    throw new Exception("Boxscore table 3 problem. Team_Flag description is: " +
                        Fila["team_flag"].ToString());
            }

            //Double Plays Turned
            int dpa = 0, dph = 0;
            GetDoublePlays(ref dpa, ref dph);
            gamea.DPT = dpa;
            gameh.DPT = dph;

            gr.Add(gamea);
            gr.Add(gameh);

            ////Standing Repository for the Streak and W/L Update
            ////StandingRepository sr = new StandingRepository(db);
            ////Standing Streak and W/L Update
            //Standing standinga = sr.GetStanding(SeasonInfo.AñoInicio, SeasonInfo.Phase, Away.ID);
            //Standing standingh = sr.GetStanding(SeasonInfo.AñoInicio, SeasonInfo.Phase, Home.ID);
            //standinga.Streak = GetStreakValue((int)gamea.W, (int)standinga.Streak);
            //standingh.Streak = GetStreakValue((int)gameh.W, (int)standingh.Streak);
            //if (gamea.W == 1)
            //    standinga.W++;
            //else
            //    standinga.L++;
            //if (gameh.W == 1)
            //    standingh.W++;
            //else
            //    standingh.L++;
        }

        private void ProcessPlayerInformation()
        {
            WhereAmI = "ProcessPlayerInformation";

            if (cal == null)
                throw new Exception("ProcessPlayerInformation.  Calendar Object cal not available.");

            if (Home == null || Away == null)
                throw new Exception("ProcessPlayerInformation.  Team Objects Home, Away not available.");

            if (ds == null)
                ReadBoxScoreIntoDataset();

            //Setup Processing
            PlayerRepository pr = new PlayerRepository(db);
            Player_Game_StatsRepository pgsr = new Player_Game_StatsRepository(db);
            //6-12-2012
            //Player Season Stats Eliminated and Commented
            //Player_Season_StatsRepository pssr = new Player_Season_StatsRepository(db);
            int Equipo = 0;
            string Orden; //Variable to make more readable the code.
            PitcherAway = 0;
            PitcherHome = 0;

            foreach (DataRow Fila in ds.Tables[6].Rows)
            {
                // 19-1-2013
                // Caso Juan Carlos Pérez
                int tempID = GetInt32Data(Fila, "id");
                if (tempID == 467819)
                    tempID = 543633;
                //Player player = pr.GetPlayerByIDMLB(GetInt32Data(Fila, "id"));
                Player player = pr.GetPlayerByIDMLB(tempID);
                if (player == null)
                {
                    //Adds a New Players and Commits the Changes
                    player = new Player();
                    //player.IDMLB = GetInt32Data(Fila, "id");
                    player.IDMLB = tempID;
                    string[] partes;
                    try
                    {
                        partes = Fila["name_display_first_last"].ToString().Split(Convert.ToChar(" "));
                        player.Nombre = partes[0];
                        if (partes.Count() == 2)
                        {
                            player.Apellido = partes[1];
                        }
                        else
                        {
                            for (int i = 1; i < partes.Count(); i++)
                            {
                                player.Apellido += partes[i] + " ";
                            }
                            player.Apellido = player.Apellido.TrimEnd();
                        }
                        //player.IDAlterno = player.Nombre.Substring(0, 2) + player.Apellido + "9";
                        player.IDAlterno = player.Nombre.Substring(0, 2) + player.Apellido + "9" + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + "9";
                    }
                    catch (Exception)
                    {
                        partes = Fila["name"].ToString().Split(Convert.ToChar(","));
                        if (partes.Count() >= 2)
                            player.Nombre = partes[1].Trim();
                        player.Apellido = partes[0].Trim();
                        //Complex Shortcut If to read.  Right Association of ?: shortcut.
                        if (partes.Count() > 1)
                        {
                            player.IDAlterno = (player.Nombre.Length > 1 ? player.Nombre.Substring(0, 2) :
                                player.Nombre.Length > 0 ? player.Nombre.Substring(0, 1) : "XX") + player.Apellido + "99";
                        }
                        else
                        {
                            player.Nombre = "PENDIENTE";
                            player.IDAlterno = player.Apellido + "999";
                        }
                    }
                    player.FechaNacimiento = new DateTime(1900, 1, 1);
                    player.LugarNacimiento = "PENDIENTE";
                    player.Activo = true;
                    player.Position = Fila["pos"].ToString();
                    player.CurrentTeamID = 1;
                    PlayerRepository.AddAndSave(player);
                }
                Orden = Fila["bo"].ToString();
                if ((Orden == "" || Orden == null) && Fila["pos"].ToString().ToUpper() == "P" && (Equipo == 0 || Equipo == 2))
                    Equipo++;
                else if (Orden != "" && Orden != null && Fila["pos"].ToString().ToUpper() != "P" && Equipo != 0 && Equipo != 2)
                    Equipo++;
                // Get Pitcher Errors to use at PitcherStats Process
                //if (Orden == "" && Fila["pos"].ToString().ToUpper() == "P")
                //Caso Vizcaino 8-1-2011 Emergente por 8vo Bate Esc-Gig
                if (Fila["pos"].ToString().ToUpper() == "P")
                {
                    PitcherError PE = new PitcherError();
                    PE.IDMLB = player.IDMLB;
                    PE.Errors = GetInt32Data(Fila, "e");
                    ErroresPitcher.Add(PE);
                }
                if (Equipo == 0 || Equipo == 2)
                {
                    Player_Game_Stat PlayerGS = new Player_Game_Stat();
                    PlayerGS.SeasonID = SeasonInfo.AñoInicio;
                    PlayerGS.PhaseID = SeasonInfo.Phase;
                    PlayerGS.GameID = cal.GameID;
                    PlayerGS.PlayerID = player.ID;
                    PlayerGS.TeamID = Equipo == 0 ? Home.ID : Away.ID;
                    PlayerGS.Team_VS = Equipo == 0 ? Away.ID : Home.ID;
                    PlayerGS.HomeAway = PlayerGS.TeamID == Home.ID ? "H" : "A";
                    //if (player.LugarNacimiento == "PENDIENTE")
                    //{
                    //    //Fix Current TeamID and Save it to the Database
                    //    if (SeasonInfo.UpdateCurrentTeamID)
                    //        PlayerRepository.UpdateCurrentTeam(player.ID, PlayerGS.TeamID);
                    //    //Update Current Player reference
                    //    //TODO: TEST IF SAVE PERSISTS WITHOUT PRIOR CALL TO UPDATE CURRENTTEAMID.  COMMENT THAT LINE
                    //    player.CurrentTeamID = PlayerGS.TeamID;
                    //}
                    if (SeasonInfo.UpdateCurrentTeamID)
                    {
                        PlayerRepository.UpdateCurrentTeam(player.ID, PlayerGS.TeamID);
                        player.CurrentTeamID = PlayerGS.TeamID;
                    }
                    PlayerGS.Position = Fila["pos"].ToString();
                    PlayerGS.Orden = Convert.ToInt32(Orden);
                    PlayerGS.Emergente = Orden.Substring(1) != "00";
                    PlayerGS.R = GetInt32Data(Fila, "r");
                    PlayerGS.H = GetInt32Data(Fila, "h");
                    PlayerGS.E = GetInt32Data(Fila, "e");
                    PlayerGS.VB = GetInt32Data(Fila, "ab");
                    PlayerGS.H2 = GetInt32Data(Fila, "d");
                    PlayerGS.H3 = GetInt32Data(Fila, "t");
                    PlayerGS.HR = GetInt32Data(Fila, "hr");
                    PlayerGS.RBI = GetInt32Data(Fila, "rbi");
                    PlayerGS.SB = GetInt32Data(Fila, "sb");
                    PlayerGS.SO = GetInt32Data(Fila, "so");
                    PlayerGS.BB = GetInt32Data(Fila, "bb");
                    PlayerGS.HBP = GetInt32Data(Fila, "hbp");
                    PlayerGS.SF = GetInt32Data(Fila, "sf");
                    PlayerGS.PUTOUT = GetInt32Data(Fila, "po");
                    PlayerGS.ASSIST = GetInt32Data(Fila, "a");
                    ProcessGameEvents(PlayerGS, player.IDMLB);
                    pgsr.Add(PlayerGS);
                    //6-12-2012
                    //Player Season Stats Eliminated and Commented
                    //pssr.CreateDefaultPlayerSeasonStat(SeasonInfo.AñoInicio, SeasonInfo.Phase, player.ID, PlayerGS.TeamID);
                }
                else
                {
                    if (Equipo == 1 && PitcherHome == 0)
                    {
                        PitcherHome = player.IDMLB;
                    }
                    else if (Equipo == 3 && PitcherAway == 0)
                    {
                        PitcherAway = player.IDMLB;
                    }
                }
            }
        }

        private void ProcessPitcherInformation()
        {
            WhereAmI = "ProcessPitcherInformation";

            if (cal == null)
                throw new Exception("ProcessPitcherInformation.  Calendar Object cal not available.");

            if (Home == null || Away == null)
                throw new Exception("ProcessPitcherInformation.  Team Objects Home, Away not available.");

            if (gameh == null || gamea == null)
                throw new Exception("ProcessPitcherInformation.  Game Objects gameh, gamea not available.");

            if (PitcherAway == 0 || PitcherHome == 0)
                throw new Exception("ProcessPitcherInformation.  PitcherAway/PitcherHome not available.");

            if (ds == null)
                ReadBoxScoreIntoDataset();

            PlayerRepository pr = new PlayerRepository(db);
            Pitcher_Game_StatsRepository pitr = new Pitcher_Game_StatsRepository(db);
            int TeamID = 0, TeamVS = 0, Orden = 0;

            //Added 14-9-2011
            //6-12-2012
            //Player Season Stats Eliminated and Commented
            //Player_Season_StatsRepository pssr = new Player_Season_StatsRepository(db);

            foreach (DataRow Fila in ds.Tables[4].Rows)
            {
                Player player = pr.GetPlayerByIDMLB(GetInt32Data(Fila, "id"));
                if (player == null)
                    throw new Exception("ProcessPitcherInformation.  Pitcher not found in database.  IDMLB='" + GetInt32Data(Fila, "id").ToString() + "'");
                if (PitcherHome == GetInt32Data(Fila, "id"))
                {
                    TeamID = Home.ID;
                    TeamVS = Away.ID;
                    Orden = 0;
                }
                else if (PitcherAway == GetInt32Data(Fila, "id"))
                {
                    TeamID = Away.ID;
                    TeamVS = Home.ID;
                    Orden = 0;
                }
                Pitcher_Game_Stat PitcherGS = new Pitcher_Game_Stat();
                PitcherGS.SeasonID = SeasonInfo.AñoInicio;
                PitcherGS.PhaseID = SeasonInfo.Phase;
                PitcherGS.GameID = cal.GameID;
                PitcherGS.TeamID = TeamID;
                PitcherGS.PlayerID = player.ID;
                PitcherGS.Team_VS = TeamVS;
                PitcherGS.Orden = ++Orden;
                PitcherGS.HomeAway = PitcherGS.TeamID == Home.ID ? "H" : "A";
                PitcherGS.P_Games = 1;
                PitcherGS.P_Starts = Orden == 1 ? 1 : 0;
                if (SeasonInfo.UpdateCurrentTeamID)
                {
                    PlayerRepository.UpdateCurrentTeam(player.ID, PitcherGS.TeamID);
                    player.CurrentTeamID = PitcherGS.TeamID;
                }
                if (Fila["note"].ToString().Length >= 2)
                {
                    //PitcherGS.W = Fila["note"].ToString().Substring(1, 1).ToUpper() == "W" ? 1 : 0;
                    //PitcherGS.L = Fila["note"].ToString().Substring(1, 1).ToUpper() == "L" ? 1 : 0;
                    //PitcherGS.P_HOLDS = Fila["note"].ToString().Substring(1, 1).ToUpper() == "H" ? 1 : 0;
                    //PitcherGS.P_SV = Fila["note"].ToString().Substring(1, 1).ToUpper() == "S" ? 1 : 0;
                    //PitcherGS.P_BLOWNSAVES = Fila["note"].ToString().Substring(1, 1).ToUpper() == "B" ? 1 : 0;
                    PitcherGS.W = Fila["note"].ToString().ToUpper().IndexOf("(W") >= 0 ? 1 : 0;
                    PitcherGS.L = Fila["note"].ToString().ToUpper().IndexOf("(L") >= 0 ? 1 : 0;
                    PitcherGS.P_HOLDS = Fila["note"].ToString().ToUpper().IndexOf("(H") >= 0 ? 1 : 0;
                    PitcherGS.P_SV = Fila["note"].ToString().ToUpper().IndexOf("(S") >= 0 ? 1 : 0;
                    PitcherGS.P_BLOWNSAVES = Fila["note"].ToString().ToUpper().IndexOf("(B") >= 0 ? 1 : 0;
                }
                else
                {
                    PitcherGS.W = 0;
                    PitcherGS.L = 0;
                    PitcherGS.P_HOLDS = 0;
                    PitcherGS.P_SV = 0;
                    PitcherGS.P_BLOWNSAVES = 0;
                }
                PitcherGS.P_Innings = GetInt32Data(Fila, "out");
                PitcherGS.P_H = GetInt32Data(Fila, "h");
                PitcherGS.P_HR = GetInt32Data(Fila, "hr");
                PitcherGS.P_R = GetInt32Data(Fila, "r");
                PitcherGS.P_ER = GetInt32Data(Fila, "er");
                PitcherGS.P_SO = GetInt32Data(Fila, "so");
                PitcherGS.P_BB = GetInt32Data(Fila, "bb");
                PitcherGS.P_BALK = 0; //Manual input later
                PitcherGS.P_WP = 0; //Manual input later
                PitcherGS.P_PICKOFF = 0; //Manual input later
                PitcherGS.P_BATSFACED = GetInt32Data(Fila, "bf");
                PitcherGS.P_BATSCORED = 0; //Manual input later
                PitcherGS.P_BATSINH = 0; //Manual input later
                PitcherGS.P_SB = 0;  //Manual input later
                PitcherGS.P_RS = TeamID == Home.ID ? gameh.R : gamea.R;
                PitcherGS.P_E = ErroresPitcher.First(m => m.IDMLB == player.IDMLB).Errors;
                //Added 28-10-2011
                PitcherGS.P_CS = 0;  //Manual input later
                //Added 10-11-2011
                PitcherGS.P_TeamSHO = 0;
                //Added 13-10-2011
                if (PitcherGS.P_Starts == 1)
                {
                    //Added 9-11-2011 - Shutout Team and Pitcher
                    if (gamea.P_TeamSHO == 1 && PitcherGS.TeamID == gamea.TeamID)
                        PitcherGS.P_TeamSHO = 1;
                    if (gameh.P_TeamSHO == 1 && PitcherGS.TeamID == gameh.TeamID)
                        PitcherGS.P_TeamSHO = 1;
                    //End Added 9-11-2011 - Shutout Team and Pitcher
                    if (PitcherGS.P_Innings >= 15 && PitcherGS.P_ER <= 2)
                    {
                        PitcherGS.P_QS = 1;
                    }
                    else if (PitcherGS.P_Innings >= 18 && PitcherGS.P_ER <= 3)
                    {
                        PitcherGS.P_QS = 1;
                    }
                    else if (PitcherGS.P_Innings >= 23 && PitcherGS.P_ER <= 4)
                    {
                        PitcherGS.P_QS = 1;
                    }
                    else
                    {
                        PitcherGS.P_QS = 0;
                    }
                }
                else
                {
                    PitcherGS.P_QS = 0;
                }
                //End Added 13-10-2011
                ProcessGameEvents(PitcherGS, player.IDMLB);
                pitr.Add(PitcherGS);
                //Added 14-9-2011
                //6-12-2012
                //Player Season Stats Eliminated and Commented
                //pssr.CreateDefaultPlayerSeasonStat(SeasonInfo.AñoInicio, SeasonInfo.Phase, player.ID, PitcherGS.TeamID);
            }
        }

        private void ProcessHitCharts()
        {
            WhereAmI = "ProcessHitCharts";

            if (cal == null)
                throw new Exception("ProcessHitCharts.  Calendar Object cal not available.");

            if (gameh == null || gamea == null)
                throw new Exception("ProcessPitcherInformation.  Game Objects gameh, gamea not available.");

            if (ds == null)
                ReadBoxScoreIntoDataset();

            HitChartRepository hcr = new HitChartRepository(db);
            PlayerRepository pr = new PlayerRepository(db);

            DataSet ds2 = new DataSet();
            XmlTextReader myXMLReader = new XmlTextReader(UrlJuego + "/inning/inning_hit.xml");
            ds2.ReadXml(myXMLReader);
            int i = 0;
            foreach (DataRow Fila in ds2.Tables[0].Rows)
            {
                HitChart hc = new HitChart();
                hc.SeasonID = SeasonInfo.AñoInicio;
                hc.PhaseID = SeasonInfo.Phase;
                hc.GameID = cal.GameID;
                hc.ID = ++i;
                hc.TeamID = Fila["team"].ToString().ToUpper() == "A" ? gamea.TeamID : gameh.TeamID;
                hc.Team_VS = Fila["team"].ToString().ToUpper() == "A" ? gameh.TeamID : gamea.TeamID;
                hc.PlayerID = pr.GetPlayerByIDMLB(GetInt32Data(Fila, "batter")).ID;
                hc.PitcherID = pr.GetPlayerByIDMLB(GetInt32Data(Fila, "pitcher")).ID;
                hc.HomeAway = hc.TeamID == Home.ID ? "H" : "A";
                try
                {
                    hc.X = Convert.ToDouble(Fila["x"].ToString());
                }
                catch (Exception)
                {
                    hc.X = 0;
                }
                try
                {
                    hc.Y = Convert.ToDouble(Fila["y"].ToString());
                }
                catch (Exception)
                {
                    hc.Y = 0;
                }
                hc.Tipo = Fila["type"].ToString();
                hc.Description = Fila["des"].ToString();
                hc.Inning = GetInt32Data(Fila, "inning");
                hcr.Add(hc);
            }
            //clean up
            ds2.Dispose();
        }

        private void ProcessBatSituations()
        {
            WhereAmI = "ProcessBatSituations";

            if (cal == null)
                throw new Exception("ProcessHitCharts.  Calendar Object cal not available.");

            if (ds == null)
                ReadBoxScoreIntoDataset();

            if (dsGE == null)
                ReadGameEventsIntoDataset();

            BatSituationRepository bsr = new BatSituationRepository(db);
            PlayerRepository pr = new PlayerRepository(db);

            DataRow FilaAnterior = null;
            int i = 0;
            foreach (DataRow Fila in dsGE.Tables[TablaAtBats].Rows)
            {
                string Evento = Fila["event"].ToString().ToLower();
                if (HasData(Fila, "batter"))
                {
                    BatSituation bs = new BatSituation();
                    bs.SeasonID = SeasonInfo.AñoInicio;
                    bs.PhaseID = SeasonInfo.Phase;
                    bs.GameID = cal.GameID;
                    bs.ID = ++i;
                    //bs.ID = GetInt32Data(Fila, "num");
                    bs.TeamID = Fila["teamjn"].ToString() == "A" ? gamea.TeamID : gameh.TeamID;
                    bs.Team_VS = Fila["teamjn"].ToString() == "A" ? gameh.TeamID : gamea.TeamID;
                    bs.PlayerID = pr.GetPlayerByIDMLB(GetInt32Data(Fila, "batter")).ID;
                    bs.PitcherID = pr.GetPlayerByIDMLB(GetInt32Data(Fila, "pitcher")).ID;
                    bs.HomeAway = bs.TeamID == Home.ID ? "H" : "A";
                    bs.SituationID = GetSituationID(FilaAnterior);
                    bs.Outs = GetInt32Data(FilaAnterior, "o");
                    if (Evento.IndexOf("walk") >= 0 ||
                        Evento.IndexOf("hit by pitch") >= 0 ||
                        Evento.IndexOf("sac ") >= 0)
                    {
                        bs.VB = 0;
                    }
                    else
                    {
                        bs.VB = 1;
                    }
                    bs.H = IsEventHit(Evento) ? 1 : 0;
                    bs.H2 = Evento == "double" ? 1 : 0;
                    bs.H3 = Evento == "triple" ? 1 : 0;
                    bs.HR = Evento == "home run" ? 1 : 0;
                    bs.RBI = GetInt32Data(Fila, "rbi");
                    bs.Walk = Evento.IndexOf("walk") >= 0 ? 1 : 0;
                    bs.HBP = Evento.IndexOf("hit by pitch") >= 0 ? 1 : 0;
                    bs.SO = Evento.IndexOf("strikeout") >= 0 ? 1 : 0;
                    bsr.Add(bs);
                }
                //FilaAnterior debe ser Null cuando hicieron el 3er out o cambio de Inning
                if (GetInt32Data(Fila, "o") == 3)
                    FilaAnterior = null;
                else
                    FilaAnterior = Fila;
            }
        }

        public Boolean ProcessGame()
        {
            //View Example or Sample
            //BeisbolDataEntities kkk = new BeisbolDataEntities();
            //var k2 = from t in kkk.V_BoxScore where t.GameID == 4 orderby t.Inning, t.Half select t;
            //var k3 = kkk.V_Official_Games.ToList();
            //var k = kkk.V_BoxScore.Where(m => m.GameID == 4).OrderBy(m => m.Inning).ThenBy(m => m.Half);
            //foreach (V_BoxScore item in k)
            //{
            //    int? l = item.R;
            //}
            //return false;
            WhereAmI = "ProcessGame";
            if (Valid)
            {
                try
                {
                    //setup
                    try
                    {
                        ReadBoxScoreIntoDataset();
                        BoxScoreDisponible = true;
                    }
                    catch (Exception)
                    {
                        BoxScoreDisponible = false;
                    }

                    db = new BeisbolDataEntities();
                    ErroresPitcher = new List<PitcherError>();
                    //Find Game in Calendar or Create New Game in the Calendar
                    ProcessCalendar();
                    //First Delete Current Game Information from the System.
                    //This is BoxScores, Games, Player_Game_Stats, Pitcher_Game_Stats, HitCharts and BatSituations
                    int RecordsAffected;
                    RecordsAffected = db.DeleteGame_Complete(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID);

                    if (BoxScoreDisponible)
                    {
                        ReadGameEventsIntoDataset();

                        ProcessBoxScore();
                        ProcessGameAndStandingInformation();
                        ProcessPlayerInformation();
                        ProcessPitcherInformation();
                        ProcessHitCharts();
                        ProcessBatSituations();
                    }

                    //throw new Exception("Prueba error!");

                    // Grabar cambios en Base de Datos
                    WhereAmI = "ProcessGame.  Before Save Changes";
                    db.SaveChanges();
                    //Complete aggregations for Players and Pitchers for Both Teams in the game
                    if (BoxScoreDisponible)
                    {
                        GameRepository gr = new GameRepository();
                        RecordsAffected = gr.UpdateGameStats(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID);

                        //RecordsAffected = db.UpdateGamesFromPlayerStatsByGame(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID);
                        //RecordsAffected = db.UpdateGamesFromPitcherStatsByGame(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID);

                        //RecordsAffected = db.UpdateGamesFromPlayerStats(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID, cal.Team1);
                        //RecordsAffected = db.UpdateGamesFromPlayerStats(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID, cal.Team2);
                        //RecordsAffected = db.UpdateGamesFromPitcherStats(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID, cal.Team1);
                        //RecordsAffected = db.UpdateGamesFromPitcherStats(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID, cal.Team2);
                        ds.Dispose();
                    }
                    db.Dispose();
                }
                catch (Exception e)
                {
                    ErrorInfo.ErrorDescription = e.Message;
                    ErrorInfo.UrlJuego = this.UrlJuego;
                    ErrorInfo.FechaJuego = this.Fecha;
                    ErrorInfo.Juego = this.Juego;
                    ErrorInfo.URL = URL;
                    ErrorInfo.WhereWereYou = this.WhereAmI;
                    db.Dispose();
                    return false;
                    //throw;
                }
                return true;
            }
            else
                return false;
        }
        public static int UpdateStandingStats(int SeasonID, byte PhaseID)
        {
            StandingRepository sr = new StandingRepository();
            return sr.UpdateStandingStats(SeasonID, PhaseID);
            //BeisbolDataEntities mydb = new BeisbolDataEntities();
            //int i = mydb.UpdateStandingFromGamesBySeasonPhase(SeasonID, PhaseID);
            //mydb.Dispose();
            //return i;
        }
        public static int UpdateSeasonStats(int SeasonID, byte PhaseID)
        {
            Player_Season_StatsRepository psr = new Player_Season_StatsRepository();
            return psr.UpdateSeasonStats(SeasonID, PhaseID);
            //BeisbolDataEntities mydb = new BeisbolDataEntities();
            //int i = mydb.UpdatePlayerSeasonStatsFromPlayerStatsBySeasonPhase(SeasonID, PhaseID);
            //i += mydb.UpdatePlayerSeasonStatsFromPitcherStatsBySeasonPhase(SeasonID, PhaseID);
            //mydb.Dispose();
            //return i;
        }

        public static int UpdateGameStats(int SeasonID, byte PhaseID)
        {
            GameRepository gr = new GameRepository();
            return gr.UpdateGameStats(SeasonID, PhaseID);
        }

        public static int UpdateStreak(int SeasonID, byte PhaseID)
        {
            int i;
            StandingRepository sr = new StandingRepository();
            i = sr.UpdateStreak(SeasonID, PhaseID);
            sr.Save();
            return i;
        }

        public static int UpdateLast10GamesRecord(int SeasonID, byte PhaseID)
        {
            int i;
            StandingRepository sr = new StandingRepository();
            i = sr.UpdateLast10GamesRecord(SeasonID, PhaseID);
            sr.Save();
            return i;
        }
    } //End Class GameData
}
