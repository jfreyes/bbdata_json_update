﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Net;
using System.IO;

namespace BeisbolData_Web_XML_Database_Update
{
    public partial class _Default : System.Web.UI.Page
    {
        System.Diagnostics.Stopwatch crono = new System.Diagnostics.Stopwatch();
        string MLBurl_Base = "http://gd2.mlb.com/components/game/win/year_YYYY/month_MM/day_DD/";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DateTime Fecha = DateTime.Now.AddDays(-1);
                Fecha = Fecha.AddHours(1);
                txtFecha.Text = Fecha.ToShortDateString();
                int SeasonID = Fecha.Year;
                if (Fecha.Month == 1)
                {
                    SeasonID--;
                    if (Fecha.Day >= 18)
                        txtPhaseID.Text = "3";
                    else
                        txtPhaseID.Text = "2";
                }
                else
                {
                    if (Fecha.Month == 12)
                    {
                        if (Fecha.Day >= 26)
                            txtPhaseID.Text = "2";
                        else
                            txtPhaseID.Text = "1";
                    }
                    else
                        txtPhaseID.Text = "1";
                }
                txtSeasonID.Text = SeasonID.ToString();
            }
        }

        private void SetupSeasonInfo()
        {
            SeasonInfo.Phase = Convert.ToByte(txtPhaseID.Text);
            SeasonInfo.AñoInicio = Convert.ToInt32(txtSeasonID.Text);
        }

        string GetURL(DateTime Fecha)
        {
            string myURL;
            myURL = MLBurl_Base.Replace("YYYY", Fecha.Year.ToString());
            myURL = myURL.Replace("MM", Fecha.Month.ToString("00"));
            myURL = myURL.Replace("DD", Fecha.Day.ToString("00"));
            return myURL;
        }

        List<string> ParseHTML(string data)
        {
            List<string> Respuesta = new List<string>();
            string Juego;
            Boolean Salir = false;
            int pos, pos2, searchPos;
            searchPos = 0;
            while (!Salir)
            {
                pos = data.IndexOf("> gid", searchPos);
                //pos = elhtml.IndexOf(">gid", searchpos);
                if (pos > 0)
                {
                    pos2 = data.IndexOf("</a", pos);
                    //pos2 = elhtml.IndexOf("</A", pos);
                    if (pos2 > 0)
                    {
                        Juego = data.Substring(pos + 1, pos2 - pos - 2).Trim();
                        //string[] datos = Juego.Split(Convert.ToChar("_"));
                        searchPos = pos2;
                        Respuesta.Add(Juego);
                    }
                }
                else
                {
                    Salir = true;
                }
            }
            return Respuesta;
        }

        string OpenURL(string url)
        {
            string Respuesta;
            WebClient myClient = new WebClient();
            Stream myStream = myClient.OpenRead(url);
            StreamReader myReader = new StreamReader(myStream);
            Respuesta = myReader.ReadToEnd();
            myReader.Close();
            myStream.Close();
            return Respuesta;
        }

        protected void btOK_Click(object sender, EventArgs e)
        {
            string Directorio = Request.PhysicalApplicationPath + @"temp";
            if (!Directory.Exists(Directorio))
            {
                lblStatus.Text = "Directorio de Trabajo no disponible";
                return;
            }
            try
            {
                string testFile = Request.PhysicalApplicationPath + @"temp\check.txt";
                StreamWriter myWriter = new StreamWriter(testFile);
                myWriter.WriteLine("This is a test");
                myWriter.Flush();
                myWriter.Close();
            }
            catch (Exception)
            {
                lblStatus2.Text = "Can't write to Temp Directory";
                return;
            }
            lblStatus2.Text = Directorio;
            //return;
            crono.Start();
            string myURL;
            DateTime FechaInicio = Convert.ToDateTime(txtFecha.Text);
            DateTime Fecha = FechaInicio;
            DateTime FechaFin = FechaInicio;

            SetupSeasonInfo();

            //List<string> JuegosNoProcesar = txtNoGame.Text.Split(Convert.ToChar(",")).ToList();
            List<string> JuegosNoProcesar = new List<string>();

            while (Fecha <= FechaFin)
            {
                Boolean HayJuegos;
                lblStatus.Text = "Now Processing: " + Fecha.ToShortDateString();
                myURL = GetURL(Fecha);
                //MessageBox.Show(myURL);
                //MessageBox.Show(OpenURL(myURL));
                //MessageBox.Show(ParseHTML(OpenURL(myURL)));
                List<string> JuegosDia = new List<string>();
                try
                {
                    JuegosDia = ParseHTML(OpenURL(myURL));
                    HayJuegos = true;
                }
                catch (Exception)
                {
                    HayJuegos = false;
                    //throw;
                }
                if (HayJuegos)
                {
                    foreach (string Juego in JuegosNoProcesar)
                    {
                        JuegosDia.Remove(Juego);
                    }
                    foreach (string Juego in JuegosDia)
                    {
                        //GameData gamedata = new GameData(Juego, myURL);
                        lblStatus2.Text = "Game: " + Juego;
                        GameData gamedata = new GameData(Juego, myURL);
                        if (gamedata.Valid)
                        {
                            if (gamedata.ProcessGame())
                            {
                                txtLog1.Text += Juego + " OK" + Environment.NewLine;
                            }
                            else
                            {
                                txtLog1.Text += Juego + " BAD" + Environment.NewLine;
                                txtLog1.Text += "Error Description: " + ErrorInfo.ErrorDescription + Environment.NewLine;
                                txtLog1.Text += "Where: " + ErrorInfo.WhereWereYou + Environment.NewLine;
                            }
                        }
                    }
                }
                Fecha = Fecha.AddDays(1);
            } //END WHILE
            Sumarizacion();
            crono.Stop();
            TimeSpan ts = crono.Elapsed;
            lblStatus2.Text = "Total time: " + ts.TotalSeconds.ToString() + " segundos";
            txtLog1.Text += lblStatus2.Text;
        }

        private void Sumarizacion()
        {
            // UPDATE SUMARIZACION GAMES, STANDING AND SEASONSTATS
            int RecordsAffected;
            txtLog1.Text += "Finalizing..." + Environment.NewLine;
            lblStatus.Text = "Finalizing...";

            lblStatus2.Text = "Updating Game Stats";

            RecordsAffected = GameData.UpdateGameStats(SeasonInfo.AñoInicio, SeasonInfo.Phase);
            lblStatus2.Text += ": " + RecordsAffected.ToString() + " records";
            txtLog1.Text += "Updating Game Stats: " + RecordsAffected.ToString() + " records" + Environment.NewLine;

            lblStatus2.Text = "Updating Season Stats";

            RecordsAffected = GameData.UpdateSeasonStats(SeasonInfo.AñoInicio, SeasonInfo.Phase);
            lblStatus2.Text += ": " + RecordsAffected.ToString() + " records";
            txtLog1.Text += "Updating Season Stats (Should Return ZERO): " + RecordsAffected.ToString() + " records" + Environment.NewLine;

            lblStatus2.Text = "Updating Standing";

            RecordsAffected = GameData.UpdateStandingStats(SeasonInfo.AñoInicio, SeasonInfo.Phase);
            lblStatus2.Text += ": " + RecordsAffected.ToString() + " records";
            txtLog1.Text += "Updating Standing: " + RecordsAffected.ToString() + " records" + Environment.NewLine;

            lblStatus2.Text = "Updating Streak...";

            RecordsAffected = GameData.UpdateStreak(SeasonInfo.AñoInicio, SeasonInfo.Phase);
            lblStatus2.Text += ": " + RecordsAffected.ToString() + " records";
            txtLog1.Text += "Updating Streak: " + RecordsAffected.ToString() + " records" + Environment.NewLine;

            lblStatus2.Text = "Updating Last 10 Games Record...";

            RecordsAffected = GameData.UpdateLast10GamesRecord(SeasonInfo.AñoInicio, SeasonInfo.Phase);
            lblStatus2.Text += ": " + RecordsAffected.ToString() + " records";
            txtLog1.Text += "Updating Last 10 Games Record: " + RecordsAffected.ToString() + " records" + Environment.NewLine;

            lblStatus.Text = "Done!";
            lblStatus2.Text = "Updates completed!";
            txtLog1.Text += "Process Completed." + Environment.NewLine;
        }

        protected void btSumarizar_Click(object sender, EventArgs e)
        {
            SetupSeasonInfo();
            Sumarizacion();
        }

        protected void btUpdateTeamID_Click(object sender, EventArgs e)
        {
            BeisbolData.Data.Model.BeisbolDataEntities db = new BeisbolData.Data.Model.BeisbolDataEntities();
            var datos = db.Pitcher_Game_Stats.Where(m => m.SeasonID == 2011 && m.PhaseID == 1)
                .OrderBy(m => m.GameID).ToList();
            int i = 0;
            int j = 0;
            foreach (var item in datos)
            {
                i++;
                BeisbolData.Data.PlayerRepository.UpdateCurrentTeam(item.PlayerID, item.TeamID);
            }

            var datos2 = db.Player_Game_Stats.Where(m => m.SeasonID == 2011 && m.PhaseID == 1)
                .OrderBy(m => m.GameID).ToList();
            foreach (var item in datos2)
            {
                j++;
                BeisbolData.Data.PlayerRepository.UpdateCurrentTeam(item.PlayerID, item.TeamID);
            }
            txtLog1.Text += "Updated " + i.ToString() + " Pitcher records" + Environment.NewLine;
            txtLog1.Text += "Updated " + j.ToString() + " Player records" + Environment.NewLine;
            lblStatus.Text = "Completed!";
            lblStatus2.Text = "Done!";
        }

    }
}
