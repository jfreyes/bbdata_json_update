﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeisbolData_Web_XML_Database_Update
{
    public static class SeasonInfo
    {
        public static int AñoInicio = 2011;
        public static Byte Phase = 1;
        public static int AñoFin { get { return AñoInicio + 1; } }
        public static Boolean UpdateCurrentTeamID = true;

        public static string GetDescription()
        {
            return AñoInicio.ToString() + "-" + (AñoInicio + 1).ToString();
        }
        public static int GetAñoFin()
        {
            return AñoInicio + 1;
        }

    }
}