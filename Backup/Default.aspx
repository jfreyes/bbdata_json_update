﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="BeisbolData_Web_XML_Database_Update._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Welcome to BeisbolData Game Processor
    </h2>
    <div>
        UserID: <asp:TextBox ID="txtUser" runat="server" CssClass="login"></asp:TextBox>
        Password: <asp:TextBox ID="txtPassword" runat="server" CssClass="passwordEntry"></asp:TextBox>
        <br />
        Fecha: <asp:TextBox ID="txtFecha" runat="server" MaxLength="10"></asp:TextBox>
        SeasonID: <asp:TextBox ID="txtSeasonID" runat="server" MaxLength="4" 
            Width="74px"></asp:TextBox>
        PhaseID: <asp:TextBox ID="txtPhaseID" runat="server" MaxLength="1" Width="32px"></asp:TextBox>
    </div>
    <div>
    <asp:Button ID="btOK" runat="server" Text="Procesar" onclick="btOK_Click" /><br /><br />
    <asp:Button ID="btSumarizar" runat="server" Text="Sumarización" 
            onclick="btSumarizar_Click" /><br /><br />
    <asp:Button ID="btUpdateTeamID" runat="server" Text="UpdateTeamID" Enabled="False" 
            onclick="btUpdateTeamID_Click" />
    </div>
    <div>
        Status:<br /><asp:Label ID="lblStatus" runat="server" CssClass="failureNotification"></asp:Label>
    </div>
    <hr />
    <div>
        Status2:<br /><asp:Label ID="lblStatus2" runat="server" CssClass="failureNotification"></asp:Label>
    </div>
    <div>
        Log:<br />
        <asp:TextBox ID="txtLog1" runat="server" 
            Height="121px" TextMode="MultiLine" Width="548px"></asp:TextBox>
    </div>
    <p>
        Visitar <a href="http://www.beisboldata.com" title="BeisbolData Website">BeisbolData.com</a>.
    </p>
    <p>
        <%--You can also find <a href="http://go.microsoft.com/fwlink/?LinkID=152368&amp;clcid=0x409"
            title="MSDN ASP.NET Docs">documentation on ASP.NET at MSDN</a>.--%>
    </p>
</asp:Content>
