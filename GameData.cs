﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.IO;
using System.Data;
using System.Xml;

using BeisbolData.Data;
using BeisbolData.Data.Model;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace BeisbolData_Web_XML_Database_Update
{
    public partial class GameData
    {
        private class PitcherError
        {
            public int IDMLB { get; set; }
            public int Errors { get; set; }
        }

        public DateTime Fecha { get; set; }
        public string Team1 { get; set; }
        public string Team2 { get; set; }
        public int Numero { get; set; }
        public Boolean Valid { get; private set; }
        public string URL { get; set; }

        private string JuegoGI;
        private string Juego;
        private LDLinescore lineScore;
        private FeedLiveRO feedLive;
        private LDBoxscore boxScore;
        private LDPlays gameEvents;
        private string LigaCode;
        private int Orden;
        private _Default.SchRow GameMILB;
        //LIDOM
        public List<string> Teams;
        // Puerto Rico
        //private List<string> Teams = new List<string> { "san", "cag", "car", "may", "tib" };
        // Venezuela
        //private List<string> Teams = new List<string> { "ara", "zul", "lag", "lar", "mag", "leo", "mar", "ori" };
        //private List<string> Teams = new List<string> { "ara", "leo" };

        // Mexico
        //private List<string> Teams = new List<string> { "moc", "cul", "obr", "jal", "mxc", "nav", "maz", "her" };

        //private List<string> Teams = new List<string> { "esc", "lic" };
        //private List<string> Teams = new List<string> { "lic", "gig" };

        // Serie del Caribe
        //private List<string> Teams = new List<string> { "dom", "pur", "mex", "cub", "ven" };

        private string UrlJuego
        {
            get { return URL + Juego; }
        }

        /*private int TablaAtBats
        {
            get
            {
                if (dsGE == null) ReadGameEventsIntoDataset();
                return dsGE.Tables[2].Columns.Count >= 15 ? 2 : 3;
            }
        }*/

        private Boolean BoxScoreDisponible;
        private Boolean ExtraInning;

        private string WhereAmI = "Init";

        private List<PitcherError> ErroresPitcher;

        //Important Definitions for Game Processing.  Global to the class but private outside.
        BeisbolDataEntities db;
        Team Home, Away;
        Game gamea; //Away Team Game Info
        Game gameh; //Home Team Game Info
        Calendar cal;
        //DataSet ds, dsGE;

        //GameEvents Files
        const string GameEventsDownloadFile = "e:\\JFR Backup\\temp\\Events\\gameevents.xml";
        const string GameEventsNewFile = "e:\\JFR Backup\\temp\\Events\\gameevents2.xml";

        public GameData(_Default.GameTuple data, string url)
        {
            FillTeams();
            try
            {
                GameMILB = data.GID;
                Fecha = data.GID.game_time_local.Date;
                Team1 = data.GID.away_team_abbrev.ToLower();
                Team2 = data.GID.home_team_abbrev.ToLower();
                //Team2 = datos.ElementAt(5);
                Numero = data.GID.game_nbr;
                LigaCode = data.GID.home_league_id;
                JuegoGI = data.GPK;
                Juego = "gid_" + data.GID.game_id.Replace("/", "_").Replace("-", "_");
                //URL = url;
                Validate();
            }
            catch (Exception)
            {
                SetInvalid();
                //throw;
            }
        }

        public class tTeam
        {
            public int id { get; set; }
            public string name { get; set; }
            public string link { get; set; }
            //public Venue venue { get; set; }
            public string teamCode { get; set; }
            public string fileCode { get; set; }
            public string abbreviation { get; set; }
            public string teamName { get; set; }
            public string locationName { get; set; }
            public string firstYearOfPlay { get; set; }
            public League league { get; set; }
            //public Sport sport { get; set; }
            public string shortName { get; set; }
            public string parentOrgName { get; set; }
            public int parentOrgId { get; set; }
            public bool active { get; set; }
        }

        public class TeamsRO
        {
            public string copyright { get; set; }
            public List<tTeam> teams { get; set; }
        }
        private void FillTeams()
        {
            Teams = new List<string>();
            TeamsRO teamRO = JsonConvert.DeserializeObject<TeamsRO>(GetJsonResponse("http://statsapi.mlb.com/api/v1/teams?leagueIds=" + LigaCode));
                foreach (var team in teamRO.teams)
                {
                    Teams.Add(team.teamCode);
                }
        }
        private void SetInvalid()
        {
            Valid = false;
            Team1 = string.Empty; Team2 = null; Numero = 0;
        }

        private void Validate()
        {
            //if (Team2 == "agu")
            //    Valid = false;
            if (Teams.Contains(Team1) && Teams.Contains(Team2))
                Valid = true;
            else
                SetInvalid();
        }

        #region Helper Functions and Methods
        private int GetGameTime(string data)
        {
            /*const string SearchString = "<b>T</b>:";
            int pos = data.IndexOf(SearchString);
            if (pos >= 0)*/
            if (data != "")
            {
                //pos += SearchString.Length + 1;
                return GetMinutes_From_HourMinuteFormat(data);
            }
            else
            {
                return 0;
            }
        }

        private int GetMinutes_From_HourMinuteFormat(string data)
        {
            int bracket = data.IndexOf('(');
            if (bracket != -1) data = data.Substring(0, bracket);
            string[] hm = data.Trim().Split(Convert.ToChar(":"));

            int Horas = 0;
            int Minutos = 0;
            Boolean BHoras = false;
            Boolean BMinutos = false;
            BHoras = Int32.TryParse(hm[0], out Horas);
            BMinutos = Int32.TryParse(hm[1], out Minutos);
            if (!BMinutos)
            {
                hm[1] = hm[1].Substring(0, 2);
                BMinutos = Int32.TryParse(hm[1], out Minutos);
            }
            if (BMinutos && BHoras)
                return hm.Length == 2 ? (Horas * 60) + Minutos : 0;
            else
                return 0;
            //return hm.Length == 2 ? (Convert.ToInt32(hm[0]) * 60) + Convert.ToInt32(hm[1]) : 0;
        }

        private int GetMinutes_From_HourMinuteFormat(string data, string ampm)
        {
            string[] hm = data.Trim().Split(Convert.ToChar(":"));
            if (ampm.ToLower() == "pm")
            {
                return hm.Length == 2 ? ((Convert.ToInt32(hm[0]) + 12) * 60) + Convert.ToInt32(hm[1]) : 0;
            }
            else
            {
                return hm.Length == 2 ? (Convert.ToInt32(hm[0]) * 60) + Convert.ToInt32(hm[1]) : 0;
            }
        }

        private int GetInt32Data(DataSet ds, int Tabla, int Fila, string Columna)
        {
            return GetInt32Data(ds.Tables[Tabla].Rows[Fila], Columna);
        }
        private int GetInt32Data(DataRow DR, string Columna)
        {
            try
            {
                return Convert.ToInt32(DR[Columna].ToString());
            }
            catch (Exception)
            {
                return 0;
                //throw;
            }
        }

        private void GetDoublePlays(ref int dpa, ref int dph)
        {
            WhereAmI = "GetDoublePlays";

            /*if (gameEvents == null)
                ReadGameEventsIntoDataset();*/
            var allDPs = gameEvents.allPlays.Where(p => p.result.description.ToLower().IndexOf("double play") >= 0
            || p.result.@event.ToUpper().IndexOf("DP") >= 0);
            dph = allDPs.Where(p => p.about.halfInning == "top").Count();
            dpa = allDPs.Where(p => p.about.halfInning == "bottom").Count();
            /*foreach (DataRow Fila in dsGE.Tables[TablaAtBats].Rows)
            {
                if (Fila["des"].ToString().ToLower().IndexOf("double play") >= 0 ||
                    Fila["event"].ToString().ToUpper().IndexOf("DP") >= 0)
                {
                    if (Fila["teamjn"].ToString() == "A")
                        dph++;
                    else
                        dpa++;
                }
            }*/
        }

        private Boolean HasData(DataRow DR, string Columna)
        {
            return DR[Columna].ToString().Length > 0;
        }

        private Boolean AreRunnersInScoringPosition(DataRow DR)
        {
            if (DR == null)
                return false;
            if (HasData(DR, "b2") || HasData(DR, "b3"))
                return true;
            else
                return false;
        }

        private Boolean AreRunnersOn(DataRow DR)
        {
            if (DR == null)
                return false;
            if (HasData(DR, "b1") || AreRunnersInScoringPosition(DR))
                return true;
            else
                return false;
        }

        private Boolean IsEventHit(string evento)
        {
            if (evento.ToLower() == "single" || evento.ToLower() == "double" ||
                evento.ToLower() == "triple" || evento.ToLower() == "home run")
                return true;
            else
                return false;
        }

        private string GetSituationBase(DataRow DR, string Base)
        {
            return HasData(DR, Base) ? "1" : "0";
        }

        private string GetSituationID(DataRow DR)
        {
            if (DR == null)
                return "S000";
            else
                return "S" + GetSituationBase(DR, "b1") + GetSituationBase(DR, "b2") + GetSituationBase(DR, "b3");
        }

        private string GetFilePathForParsedGameEventsXML(string URL)
        {
            return "";
        }

        private void SetGameAgrValuesAtZero(Game game)
        {
            game.SB = 0;
            game.CS = 0;
            game.RISPAB = 0;
            game.RISPH = 0;
            game.RISPRBI = 0;
            game.ROBAB = 0;
            game.ROBH = 0;
            game.IBB = 0;
            game.HBP = 0;
            game.OUT2RBI = 0;
            game.GIDP = 0;
            game.S = 0;
            game.SF = 0;
            game.PUTOUT = 0;
            game.PASSEDBALL = 0;
            game.ASSIST = 0;
            game.PICKEDOFF = 0;
            game.P_IBB = 0;
            game.P_HBP = 0;
            game.P_BALK = 0;
            game.P_WP = 0;
            game.P_SV = 0;
            game.P_PICKOFF = 0;
            game.P_E = 0;
            game.P_GROUND = 0;
            game.P_FLY = 0;
            game.P_BATSFACED = 0;
            game.P_BATSCORED = 0;
            game.P_BATSINH = 0;
            game.P_HOLDS = 0;
            game.P_SB = 0;
            game.P_BLOWNSAVES = 0;
            game.P_Innings = 0;
            //Added 28-9-2011
            game.FLY = 0;
            game.GROUND = 0;
            game.P_S = 0;
            game.P_SF = 0;
            game.P_GIDP = 0;
            game.P_CG = 0;
            game.P_SHO = 0;
            //Added 8-10-2011
            game.C_CS = 0;
            //Added 28-10-2011
            game.C_SB = 0;
            game.P_CS = 0;
            game.P_QS = 0;
            //Added 10-11-2011
            game.P_TeamSHO = 0;
        }

        //27-10-2013

        
        private void SetGameAgrValuesAtZero2(Game game)
        {
            game.VB = 0;
            game.H2 = 0;
            game.H3 = 0;
            game.HR = 0;
            game.RBI = 0;
            game.LOB = 0;
            game.DPT = 0;
            game.SO = 0;
            game.BB = 0;
            game.P_H = 0;
            game.P_HR = 0;
            game.P_R = 0;
            game.P_ER = 0;
            game.P_SO = 0;
            game.P_BB = 0;
        }
        //private int GetStreakValue(int W, int streak)
        //{
        //    if (W == 1)
        //    {
        //        if (streak > 0)
        //            return ++streak;
        //        else
        //            return 1;
        //    }
        //    else
        //    {
        //        if (streak < 0)
        //            return --streak;
        //        else
        //            return -1;
        //    }
        //}

        #endregion

        private void ProcessGameEvents(Player_Game_Stat PlayerGS, int IDMLB)
        {
            if (IDMLB == 602112)
            {
            }
            /*if (gameEvents == null)
                ReadGameEventsIntoDataset();*/
            //Determine which table is AtBats and which is Stops
            //DataRow FilaAnterior = null;
            var PlayerPlays = gameEvents.allPlays.Where(p => p.matchup.batter.id == IDMLB).ToList();
            var SitPlayerPlays = PlayerPlays.Where(p => p.runners.Where(o => o.movement.start == "2B" || o.movement.start == "3B").Count() > 0);
            PlayerGS.RISPAB = SitPlayerPlays.Where(p => p.result.type == "atBat" && !p.result.@event.ToLower().Contains("walk") && !p.result.@event.ToLower().Contains("sac") && !p.result.@event.ToLower().Contains("hit by pitch")).Count();
            PlayerGS.RISPH = SitPlayerPlays.Where(p => p.result.@event == "Single" || p.result.@event == "Double" || p.result.@event == "Triple" || p.result.@event == "Home Run").Count();
            PlayerGS.RISPRBI = SitPlayerPlays.Select(p => p.result.rbi).DefaultIfEmpty(0).Sum();
            SitPlayerPlays = PlayerPlays.Where(p => p.runners.Where(o => o.movement.start == "1B" || o.movement.start == "2B" || o.movement.start == "3B").Count() > 0);
            PlayerGS.ROBAB = SitPlayerPlays.Where(p => p.result.type == "atBat" && !p.result.@event.ToLower().Contains("walk") && !p.result.@event.ToLower().Contains("sac") && !p.result.@event.ToLower().Contains("hit by pitch")).Count();
            PlayerGS.ROBH = SitPlayerPlays.Where(p => p.result.@event == "Single" || p.result.@event == "Double" || p.result.@event == "Triple" || p.result.@event == "Home Run").Count();
            PlayerGS.IBB = PlayerPlays.Where(p => p.result.@event.Contains("intent walk")).Count();
            PlayerGS.OUT2RBI = PlayerPlays.Where(p => p.count.outs == 2).Select(o => o.result.rbi).DefaultIfEmpty(0).Sum();
            PlayerGS.GIDP = PlayerPlays.Where(p => p.result.@event.ToLower() == "grounded into dp").Count();
            PlayerGS.PICKEDOFF = gameEvents.allPlays.Where(p => p.runners.Any(o => o.details.@event.ToLower().Contains("pickoff caught stealing") && o.details.runner.id == IDMLB)).Count();
            PlayerGS.C_SB = gameEvents.allPlays.Where(p => p.sBCatcherID == IDMLB).Sum(o => o.caughtStealing);
            PlayerGS.C_CS = gameEvents.allPlays.Where(p => p.sBCatcherID == IDMLB).Sum(o => o.stolenBases);
            PlayerGS.GROUND = PlayerPlays.Where(p => p.battedBallType == "G").Count();
            PlayerGS.FLY = PlayerPlays.Where(p => p.battedBallType != "" && p.battedBallType != "G").Count();
            /*
                        foreach (AllPlay GEPlay in gameEvents.allPlays)
                        {
                            //if (GetInt32Data(Fila, "batter") == PlayerGS.Player.IDMLB)
                            // AT BATS Information is Checked here
                            if (GetInt32Data(Fila, "batter") == IDMLB)
                            {
                                string Evento = Fila["event"].ToString().ToLower();
                                if (AreRunnersInScoringPosition(FilaAnterior))
                                {
                                    PlayerGS.RISPAB++;
                                    if (IsEventHit(Evento))
                                    {
                                        PlayerGS.RISPH++;
                                    }
                                    else if (Evento.IndexOf("walk") >= 0 ||
                                        Evento.IndexOf("hit by pitch") >= 0 ||
                                        Evento.IndexOf("sac ") >= 0)
                                    {
                                        PlayerGS.RISPAB--;
                                    }
                                    if (GetInt32Data(Fila, "rbi") >= 0)
                                        PlayerGS.RISPRBI += GetInt32Data(Fila, "rbi");
                                }
                                if (AreRunnersOn(FilaAnterior))
                                {
                                    PlayerGS.ROBAB++;
                                    if (IsEventHit(Evento))
                                        PlayerGS.ROBH++;
                                }
                                if (Evento == "intent walk")
                                    PlayerGS.IBB++;
                                if (GetInt32Data(FilaAnterior, "o") == 2)
                                {
                                    PlayerGS.OUT2RBI += GetInt32Data(Fila, "rbi");
                                }
                                if (Fila["des"].ToString().ToLower().IndexOf("double play") >= 0 ||
                                    Evento.ToUpper().IndexOf("DP") >= 0)
                                {
                                    PlayerGS.GIDP++;
                                }
                                if (Evento.IndexOf("sac bunt") >= 0)
                                    PlayerGS.S++;

                                //GAME EVENTS ANALYSIS FOR GROUND AND FLY BALLS
                                //Added 26-9-2011
                                //Same as Pitcher in its ProcessGameEvents

                                if (Evento.IndexOf("pop out") >= 0 || Evento.IndexOf("flyout") >= 0
                                    || Evento.IndexOf("fly") >= 0 || Evento.IndexOf("lineout") >= 0) //Added 15-9-2011
                                {
                                    PlayerGS.FLY++;
                                }
                                else if (Evento.IndexOf("groundout") >= 0 || Evento.IndexOf("grounded") >= 0
                                    || Evento.IndexOf("sac bunt") >= 0) //Added Grounded 15-9-2011 - Sac Bunt 19-9-2011
                                {
                                    PlayerGS.GROUND++;
                                }
                                else if (IsEventHit(Evento))
                                {
                                    //if (Fila["des"].ToString().ToLower().IndexOf("groundball") >= 0)
                                    if (Fila["des"].ToString().ToLower().IndexOf("ground") >= 0) //15-9-2011
                                        PlayerGS.GROUND++;
                                    else
                                        PlayerGS.FLY++;
                                }
                                //Added 26-9-2011 - 15-9-2011
                                else
                                {
                                    string EventoDes = Fila["des"].ToString().ToLower();
                                    if (EventoDes.IndexOf("ground") >= 0 || EventoDes.IndexOf("sacrifice bunt") >= 0) //Bunt added 26-9-2011 - 19-9-2011
                                        PlayerGS.GROUND++;
                                    else if (EventoDes.IndexOf("lines out") >= 0 || EventoDes.IndexOf("flies out") >= 0 ||
                                        EventoDes.IndexOf("pops out") >= 0)
                                        PlayerGS.FLY++; //End Added
                                    else
                                    { //Added 26-9-2011 - 19-9-2011
                                        if (EventoDes.IndexOf("reaches on fielding error") >= 0 ||
                                            EventoDes.IndexOf("reaches on throwing error") >= 0)
                                            PlayerGS.GROUND++;
                                    }
                                }

                                //END ANALYSIS

                            }
                            // STOPS Information is checked here
                            if (GetInt32Data(Fila, "player") == IDMLB)
                            {
                                string Evento = Fila["event"].ToString().ToLower();
                                if (Evento.IndexOf("caught stealing") >= 0 ||
                                    Evento.IndexOf("runner out") >= 0)
                                {
                                    PlayerGS.CS++;
                                }
                                if ((Evento.IndexOf("picked off stealing") >= 0 ||
                                    Evento.IndexOf("pickoff") >= 0) && Evento.IndexOf("pickoff error") <= 0)
                                {
                                    PlayerGS.PICKEDOFF++;
                                }
                            }
                            //FilaAnterior debe ser Null cuando hicieron el 3er out o cambio de Inning
                            if (GetInt32Data(Fila, "o") == 3)
                                FilaAnterior = null;
                            else
                                FilaAnterior = Fila;
                        }*/
        }

        /*private void ProcessGameEvents(Pitcher_Game_Stat PitcherGS, int IDMLB)
        {
            WhereAmI = "ProcessGameEvents Pitcher";
            if (gameEvents == null)
                ReadGameEventsIntoDataset();
            //Setup Data
            //foreach (DataRow Fila in dsGE.Tables[TablaAtBats].Rows)
            foreach ()
            {
                PitcherGS.P_IBB = 0;
                PitcherGS.P_HBP = 0;
                PitcherGS.P_GROUND = 0;
                PitcherGS.P_FLY = 0;
                //Added 28-9-2011
                PitcherGS.P_S = 0;
                PitcherGS.P_SF = 0;
                PitcherGS.P_GIDP = 0;
                PitcherGS.P_CG = 0;
                PitcherGS.P_SHO = 0;
                if (GetInt32Data(Fila, "pitcher") == IDMLB)
                {
                    string Evento = Fila["event"].ToString().ToLower();
                    if (Evento == "intent walk")
                        PitcherGS.P_IBB++;
                    if (Evento == "hit by pitch")
                        PitcherGS.P_HBP++;
                    //Added 28-9-2011
                    if (Fila["des"].ToString().ToLower().IndexOf("double play") >= 0 ||
                        Evento.ToUpper().IndexOf("DP") >= 0)
                    {
                        PitcherGS.P_GIDP++;
                    }
                    if (Fila["des"].ToString().ToLower().IndexOf("sacrifice fly") >= 0 ||
                        Evento.ToLower().IndexOf("sac fly") >= 0)
                    {
                        PitcherGS.P_SF++;
                    }
                    if (Evento.IndexOf("sac bunt") >= 0)
                        PitcherGS.P_S++;
                    //End Added 28-9-2011
                    if (Evento.IndexOf("pop out") >= 0 || Evento.IndexOf("flyout") >= 0
                        || Evento.IndexOf("fly") >= 0 || Evento.IndexOf("lineout") >= 0) //Added 15-9-2011
                    {
                        PitcherGS.P_FLY++;
                    }
                    else if (Evento.IndexOf("groundout") >= 0 || Evento.IndexOf("grounded") >= 0
                        || Evento.IndexOf("sac bunt") >= 0) //Added Grounded 15-9-2011 - Sac Bunt 19-9-2011
                    {
                        PitcherGS.P_GROUND++;
                    }
                    else if (IsEventHit(Evento))
                    {
                        //if (Fila["des"].ToString().ToLower().IndexOf("groundball") >= 0)
                        if (Fila["des"].ToString().ToLower().IndexOf("ground") >= 0) //15-9-2011
                            PitcherGS.P_GROUND++;
                        else
                            PitcherGS.P_FLY++;
                    }
                    //Added 15-9-2011
                    else
                    {
                        string EventoDes = Fila["des"].ToString().ToLower();
                        if (EventoDes.IndexOf("ground") >= 0 || EventoDes.IndexOf("sacrifice bunt") >= 0) //Bunt added 19-9-2011
                            PitcherGS.P_GROUND++;
                        else if (EventoDes.IndexOf("lines out") >= 0 || EventoDes.IndexOf("flies out") >= 0 ||
                            EventoDes.IndexOf("pops out") >= 0)
                            PitcherGS.P_FLY++; //End Added
                        else
                        { //Added 19-9-2011
                            if (EventoDes.IndexOf("reaches on fielding error") >= 0 ||
                                EventoDes.IndexOf("reaches on throwing error") >= 0)
                                PitcherGS.P_GROUND++;
                        }
                    }
                }
            }
        }*/

        private void ReadBoxScoreIntoDataset()
        {
            if (feedLive == null)
            {
                string jsonResult = GetJsonResponse("https://statsapi.mlb.com/api/v1.1/game/" + JuegoGI + "/feed/live/diffPatch?language=en");
                jsonResult = jsonResult.Replace("\n    \"players\" : {", "\n    \"players\" : [");
                jsonResult = Regex.Replace(jsonResult, "strikeZoneBottom\" : [0-9\\.]+\\n\\s+}\\n\\s+}", delegate (Match match)
                {
                    int len = match.ToString().Length - 1;
                    return match.ToString().Substring(0, len)+"]";
                });
                jsonResult = jsonResult.Replace("},\n          \"batters\"", "],\n          \"batters\"").Replace("\n          \"players\" : {", "\n          \"players\" : [");
                jsonResult = Regex.Replace(jsonResult, "\"ID\\d{6}\" : ", "");
                feedLive = JsonConvert.DeserializeObject<FeedLiveRO>(jsonResult);
            }
            lineScore = feedLive.liveData.linescore;
            boxScore = feedLive.liveData.boxscore;
            gameEvents = feedLive.liveData.plays;
            //ds = new DataSet();
            //XmlTextReader myXMLReader = new XmlTextReader(UrlJuego + "/boxscore.xml");
            //ds.ReadXml(myXMLReader);
        }

        /* private void ReadGameEventsIntoDataset()
         {
             WhereAmI = "ReadGameEventsIntoDataset";
             if (feedLive == null)
             {
                 string jsonResult = GetJsonResponse("https://statsapi.mlb.com/api/v1.1/game/" + JuegoGI.gamePk + "/feed/live/diffPatch?language=en");
                 feedLive = JsonConvert.DeserializeObject<FeedLiveRO>(jsonResult);
             }
             int Inning = 0, Numero = 0;
             string TeamHA = string.Empty, B1 = "", B2 = "", B3 = "", Description = string.Empty;
             Boolean IsAction = false;
             WebClient myClient = new WebClient();
             if (ProxyInfo.UseProxy)
                 myClient.Proxy.Credentials = new NetworkCredential(ProxyInfo.User, ProxyInfo.Password, ProxyInfo.Dominio);
             myClient.DownloadFile(UrlJuego + "/game_events.xml", GameEventsDownloadFile);
             myClient.Dispose();
             StreamReader myReader = new StreamReader(GameEventsDownloadFile);
             StreamWriter myWriter = new StreamWriter(GameEventsNewFile);
             string Linea;
             //while (myReader.Peek()>=0)
             while (myReader.EndOfStream == false)
             {
                 Linea = myReader.ReadLine();
                 if (Linea.ToLower().IndexOf("<inning") >= 0)
                     Inning++;
                 if (Linea.ToLower().IndexOf("<top>") >= 0)
                     TeamHA = "A";
                 else if (Linea.ToLower().IndexOf("<bottom>") >= 0)
                     TeamHA = "H";
                 if (Linea.ToLower().IndexOf("<atbat") >= 0 || Linea.ToLower().IndexOf("<action") >= 0)
                 {
                     Numero++;
                     if (Linea.ToLower().IndexOf("<action") >= 0)
                     {
                         IsAction = true;
                         Linea = Linea.Replace("<action", "<atbat");
                     }
                     else
                     {
                         IsAction = false;
                     }
                     Linea += " numjn=\"" + Numero.ToString() + "\" teamjn=\"" + TeamHA +
                         "\" inningjn=\"" + Inning.ToString() + "\"";
                 }
                 if (Linea.ToLower().IndexOf("b1=") >= 0)
                 {
                     if (Linea.ToLower().IndexOf("b1=\"\"") >= 0)
                         B1 = "";
                     else
                         B1 = "100";
                 }
                 if (Linea.ToLower().IndexOf("b2=") >= 0)
                 {
                     if (Linea.ToLower().IndexOf("b2=\"\"") >= 0)
                         B2 = "";
                     else
                         B2 = "010";
                 }
                 if (Linea.ToLower().IndexOf("b3=") >= 0)
                 {
                     if (Linea.ToLower().IndexOf("b3=\"\"") >= 0)
                         B3 = "";
                     else
                         B3 = "001";
                 }
                 #region IsAction
                 if (IsAction)
                 {
                     if (Linea.ToLower().IndexOf("des=\"") >= 0)
                         Description = Linea.ToLower();
                     //Caught Stealing Event
                     if (Linea.ToLower().IndexOf("event=\"") >= 0)
                     {
                         if (Linea.ToLower().IndexOf("caught stealing 2b") >= 0)
                             B1 = "";
                         if (Linea.ToLower().IndexOf("caught stealing 3b") >= 0)
                             B2 = "";
                         if (Linea.ToLower().IndexOf("caught stealing h") >= 0)
                             B3 = "";
                     }
                     //Balk Event
                     if (Linea.ToLower().IndexOf("balk") >= 0 && Linea.ToLower().IndexOf("event=\"") >= 0)
                     {
                         B1 = ""; B2 = ""; B3 = "";
                         if (Description.IndexOf("advances to 2nd") >= 0)
                             B2 = "010";
                         if (Description.IndexOf("advances to 3rd") >= 0)
                             B3 = "001";
                     }
                     //Wild Pitch Event
                     if (Linea.ToLower().IndexOf("wild pitch") >= 0 && Linea.ToLower().IndexOf("event=\"") >= 0)
                     {
                         if (Description.IndexOf("to 2nd") >= 0)
                         {
                             B1 = "";
                             B2 += "010X";
                         }
                         if (Description.IndexOf("to 3rd") >= 0)
                         {
                             B3 += "001X";
                             if (B2.Length <= 3)
                                 B2 = "";
                         }
                         if (Description.IndexOf("scores") >= 0)
                         {
                             if (B3.Length <= 3)
                                 B3 = "";
                         }
                     }
                     //Passed Ball Event
                     if (Linea.ToLower().IndexOf("passed ball") >= 0 && Linea.ToLower().IndexOf("event=\"") >= 0)
                     {
                         if (Description.IndexOf("to 2nd") >= 0)
                         {
                             B1 = "";
                             B2 += "010X";
                         }
                         if (Description.IndexOf("to 3rd") >= 0)
                         {
                             B3 += "001X";
                             if (B2.Length <= 3)
                                 B2 = "";
                         }
                         if (Description.IndexOf("scores") >= 0)
                         {
                             if (B3.Length <= 3)
                                 B3 = "";
                         }
                     }
                     //Stolen Base Event
                     if (Linea.ToLower().IndexOf("steals") >= 0)
                     {
                         if (Linea.ToLower().IndexOf("2nd base") >= 0)
                         {
                             if (Linea.ToLower().IndexOf("advances to") >= 0)
                             {
                                 B1 = "";
                                 B2 = "";
                                 if (Linea.ToLower().IndexOf("3rd") >= 0)
                                     B3 += "001X";
                                 else
                                     B3 = "";
                             }
                             else
                             {
                                 B1 = "";
                                 B2 += "010X";
                             }
                         }
                         if (Linea.ToLower().IndexOf("3rd base") >= 0)
                         {
                             if (Linea.ToLower().IndexOf("advances") >= 0 || Linea.ToLower().IndexOf("scores") >= 0)
                             {
                                 B3 = "";
                             }
                             else
                             {
                                 B3 = "001X";
                                 if (B2.Length <= 3)
                                     B2 = "";
                             }
                         }
                     }
                     // Pickoff Event
                     if ((Linea.ToLower().IndexOf("pickoff") >= 0 || Linea.ToLower().IndexOf("picked off") >= 0)
                         && Linea.ToLower().IndexOf("event=\"") >= 0)
                     {
                         if ((Linea.ToLower().IndexOf("stealing 2b") >= 0 || Linea.ToLower().IndexOf("off 1b") >= 0)
                             && Linea.ToLower().IndexOf("error") < 0)
                         {
                             B1 = "";
                         }
                         if ((Linea.ToLower().IndexOf("stealing 3b") >= 0 || Linea.ToLower().IndexOf("off 2b") >= 0)
                             && Linea.ToLower().IndexOf("error") < 0)
                         {
                             B2 = "";
                         }
                         if ((Linea.ToLower().IndexOf("stealing h") >= 0 || Linea.ToLower().IndexOf("off 3b") >= 0)
                             && Linea.ToLower().IndexOf("error") < 0)
                         {
                             B3 = "";
                         }
                     }
                     // Pickoff Error Event
                     if (Linea.ToLower().IndexOf("pickoff error") >= 0 && Linea.ToLower().IndexOf("event=\"") >= 0)
                     {
                         if (Description.IndexOf("to 2nd") >= 0)
                         {
                             B1 = "";
                             B2 += "010X";
                         }
                         if (Description.IndexOf("to 3rd") >= 0)
                         {
                             B3 += "001X";
                             if (B2.Length <= 3)
                                 B2 = "";
                         }
                         if (Description.IndexOf("scores") >= 0)
                         {
                             if (B3.Length <= 3)
                                 B3 = "";
                         }
                     }
                     //Set Runners on Base information on Action Event
                     if (Linea.ToLower().IndexOf("player=\"") >= 0)
                     {
                         Linea += " b1=\"" + B1 + "\" b2=\"" + B2 + "\" b3=\"" + B3 + "\"";
                     }
                 }
                 #endregion
                 //Write the line to the output stream
                 if (Linea.ToLower().IndexOf("pitch sv_id") < 0) //Johnny Nouel 19-10-2013 Cambio MLB
                     myWriter.WriteLine(Linea);
             }
             //Cleanup
             myWriter.Flush();
             myWriter.Close();
             myWriter.Dispose();
             myReader.Close();
             myReader.Dispose();
             //Read new XML File GameEvents
             //dsGE = new DataSet();
             //dsGE.ReadXml(GameEventsNewFile);
         }*/

        private void ProcessCalendar_()
        {
            //28-10-2013
            Boolean LineScoreAvailable = true;
            // Calendar
            WhereAmI = "ProcessCalendar";

            //if (ds == null)
            //    ReadBoxScoreIntoDataset();

            CalendarRepository cr = new CalendarRepository(db);
            TeamRepository tr = new TeamRepository(db);
            StandingRepository sr = new StandingRepository(db);

            //DataSet ds2 = new DataSet();
            //XmlTextReader myXMLReader = new XmlTextReader(UrlJuego + "/game.xml");
            //ds2.ReadXml(myXMLReader);

            DataSet ds2 = new DataSet();
            XmlTextReader myXMLReader = new XmlTextReader(UrlJuego + "/linescore.xml");
            //28-10-2013
            try
            {
                ds2.ReadXml(myXMLReader);
            }
            catch (Exception)
            {
                LineScoreAvailable = false;
            }

            //28-10-2013 Changes because of LineScore not being available
            DateTime FechaJuego;
            if (LineScoreAvailable)
            {
                FechaJuego = Fecha.AddMinutes(GetMinutes_From_HourMinuteFormat(ds2.Tables[0].Rows[0]["time"].ToString(), ds2.Tables[0].Rows[0]["ampm"].ToString()));
                Home = tr.GetTeamBy_MLB_ShortName(ds2.Tables[0].Rows[0]["home_code"].ToString());
                Away = tr.GetTeamBy_MLB_ShortName(ds2.Tables[0].Rows[0]["away_code"].ToString());
            }
            else
            {
                FechaJuego = Fecha;
                Home = tr.GetTeamBy_MLB_ShortName(Team2);
                Away = tr.GetTeamBy_MLB_ShortName(Team1);
            }
            //28-10-2013 END Changes because of LineScore not being available

            //Check if the game is in the calendar (with MLB Data)
            Boolean CalNotFound = false;
            cal = cr.GetCalendarByDateTimeAndTeam(SeasonInfo.AñoInicio, SeasonInfo.Phase, FechaJuego, Away.ID, Home.ID, this.Numero);
            //cal = cr.GetCalendarByDateTimeAndTeam(SeasonInfo.AñoInicio, SeasonInfo.Phase, FechaJuego, Away.ID, Home.ID);
            CalNotFound = cal == null;
            if (CalNotFound)
            {
                cal = new Calendar();
                cal.SeasonID = SeasonInfo.AñoInicio;
                cal.PhaseID = SeasonInfo.Phase;
                cal.GameID = SeasonRepository.GetNewGameID(SeasonInfo.AñoInicio);
                cal.Team1 = Away.ID;
                cal.Team2 = Home.ID;
            }
            //Properties Update
            string statusgame, statusDescription;
            cal.Descripcion = Away.Nombre + " vs " + Home.Nombre; //Added 14-9-2011
            //19-9-2011 -> Solicitado por Arturo para Dynamic Data
            cal.Descripcion = cal.GameID.ToString() + " - " + FechaJuego.Day.ToString() + "-" + FechaJuego.Month.ToString() + "-"
                + FechaJuego.Year.ToString() + " - " + Away.NombreCorto + " vs " + Home.NombreCorto;
            cal.Fecha = FechaJuego;
            //28-10-2013 Changes because of LineScore not being available
            //if (LineScoreAvailable)
            {
                //MLBLSGame mLBGame = GetGameStatus(cal.Fecha.ToString("MM/dd/yyyy"), Away.IDMLB, Home.IDMLB, this.Numero);
                //statusgame = ds2.Tables[0].Rows[0]["ind"].ToString().ToUpper();
                //statusDescription = ds2.Tables[0].Rows[0]["status"].ToString();
                statusgame = feedLive.gameData.status.statusCode;
                statusDescription = feedLive.gameData.status.detailedState;
                cal.Noche = FechaJuego.Hour >= 19;
            }
            /*else
            {
                statusgame = "X";
                statusDescription = "xpreview";
            }*/
            //28-10-2013 END Changes because of LineScore not being available
            if (BoxScoreDisponible)
                cal.GameTime = GetGameTime(feedLive.liveData.boxscore.info.Where(p => p.label == "T").FirstOrDefault().value);
            else
                cal.GameTime = 0;
            if (statusgame == "F" || statusgame == "FO" || statusDescription.Trim().ToLower() == "game over" ||
                (cal.GameTime > 0 && statusDescription.ToLower().Trim() == "preview") ||
                (statusgame == "OR" && statusDescription.ToLower().Trim() == "completed early"))
            {
                cal.Official = true;
                cal.Status = "P";
            }
            else
            {
                cal.Official = false;
                cal.Status = "N";
                // Johnny Nouel Nuevo Status S CHECK
                // 20-10-2013
                //if (statusgame == "S" && statusDescription.ToLower().Trim() == "preview")
                //{
                //    cal.Official = true;
                //    cal.Status = "S";
                //}
                //else
                //{
                //    cal.Official = false;
                //    cal.Status = "N";
                //}
            }
            cal.EstadioID = Convert.ToInt32(Home.EstadioID);
            cal.PostPoned = statusDescription.ToLower().Trim() == "postponed";
            cal.StatusDescription_MLB = statusDescription;
            cal.GameNumber_MLB = Numero;
            if (CalNotFound)
            {
                //Add the new cal if it was not found
                cr.Add(cal);
            }
            //Make Sure the teams are in the standing
            sr.CreateDefaultStanding(SeasonInfo.AñoInicio, SeasonInfo.Phase, Home.ID);
            sr.CreateDefaultStanding(SeasonInfo.AñoInicio, SeasonInfo.Phase, Away.ID);
            //Clean up Dataset 2 ds2
            ds2.Dispose();
        }

        private void ProcessCalendar()
        {
            //28-10-2013
            Boolean LineScoreAvailable = true;
            // Calendar
            WhereAmI = "ProcessCalendar";

            //if (ds == null)
            //    ReadBoxScoreIntoDataset();

            CalendarRepository cr = new CalendarRepository(db);
            TeamRepository tr = new TeamRepository(db);
            StandingRepository sr = new StandingRepository(db);

            //DataSet ds2 = new DataSet();
            //XmlTextReader myXMLReader = new XmlTextReader(UrlJuego + "/game.xml");
            //ds2.ReadXml(myXMLReader);

            //DataSet ds2 = new DataSet();
            //XmlTextReader myXMLReader = new XmlTextReader(UrlJuego + "/linescore.xml");
            //28-10-2013

            //string jsonResult = GetJsonResponse("https://statsapi.mlb.com/api/v1.1/game/" + JuegoGI.gamePk + "/feed/live/diffPatch?language=en");
            //jsonResult = jsonResult.Replace(",\\r\\n          \"batters\"", "],\\r\\n          \"batters\"").Replace("          \"players\" : {", "          \"players\" : [{");
            //feedLive = JsonConvert.DeserializeObject<FeedLiveRO>(jsonResult);
            if (lineScore == null)
            {
                try
                {
                    lineScore = feedLive.liveData.linescore;
                }
                catch (Exception)
                {
                    LineScoreAvailable = false;
                }
            }

            //28-10-2013 Changes because of LineScore not being available
            DateTime FechaJuego;
            if (LineScoreAvailable)
            {
                FechaJuego = GameMILB.game_time_local;
                Home = tr.GetTeamBy_MLB_ShortName(Team2);
                Away = tr.GetTeamBy_MLB_ShortName(Team1);
            }
            else
            {
                FechaJuego = Fecha;
                Home = tr.GetTeamBy_MLB_ShortName(Team2);
                Away = tr.GetTeamBy_MLB_ShortName(Team1);
            }
            //28-10-2013 END Changes because of LineScore not being available

            //Check if the game is in the calendar (with MLB Data)
            Boolean CalNotFound = false;
            cal = cr.GetCalendarByDateTimeAndTeam(SeasonInfo.AñoInicio, SeasonInfo.Phase, FechaJuego, Away.ID, Home.ID, this.Numero);
            //cal = cr.GetCalendarByDateTimeAndTeam(SeasonInfo.AñoInicio, SeasonInfo.Phase, FechaJuego, Away.ID, Home.ID);
            CalNotFound = cal == null;
            if (CalNotFound)
            {
                cal = new Calendar();
                cal.SeasonID = SeasonInfo.AñoInicio;
                cal.PhaseID = SeasonInfo.Phase;
                cal.GameID = SeasonRepository.GetNewGameID(SeasonInfo.AñoInicio);
                cal.Team1 = Away.ID;
                cal.Team2 = Home.ID;
            }
            //Properties Update
            string statusgame, statusDescription;
            cal.Descripcion = Away.Nombre + " vs " + Home.Nombre; //Added 14-9-2011
            //19-9-2011 -> Solicitado por Arturo para Dynamic Data
            cal.Descripcion = cal.GameID.ToString() + " - " + FechaJuego.Day.ToString() + "-" + FechaJuego.Month.ToString() + "-"
                + FechaJuego.Year.ToString() + " - " + Away.NombreCorto + " vs " + Home.NombreCorto;
            cal.Fecha = FechaJuego;
            //28-10-2013 Changes because of LineScore not being available
            if (LineScoreAvailable)
            {
                //MLBLSGame mLBGame = GetGameStatus(cal.Fecha.ToString("MM/dd/yyyy"), Away.IDMLB, Home.IDMLB, this.Numero);
                //statusgame = ds2.Tables[0].Rows[0]["ind"].ToString().ToUpper();
                //statusDescription = ds2.Tables[0].Rows[0]["status"].ToString();
                statusgame = feedLive.gameData.status.statusCode;//mLBGame.status.codedGameState;
                statusDescription = feedLive.gameData.status.detailedState;//mLBGame.status.detailedState;
                cal.Noche = FechaJuego.Hour >= 19;
            }
            else
            {
                statusgame = GameMILB.game_status_ind;
                statusDescription = GameMILB.game_status_text;
            }
            //28-10-2013 END Changes because of LineScore not being available
            if (BoxScoreDisponible)
                cal.GameTime = GetGameTime(boxScore.info.Where(p => p.label == "T").FirstOrDefault().value);
            else
                cal.GameTime = 0;
            if (statusgame == "F" || statusgame == "FO" || statusDescription.Trim().ToLower() == "game over" ||
                (cal.GameTime > 0 && statusDescription.ToLower().Trim() == "preview") ||
                (statusgame == "OR" && statusDescription.ToLower().Trim() == "completed early"))
            {
                cal.Official = true;
                cal.Status = "P";
            }
            else
            {
                cal.Official = false;
                cal.Status = "N";
                // Johnny Nouel Nuevo Status S CHECK
                // 20-10-2013
                //if (statusgame == "S" && statusDescription.ToLower().Trim() == "preview")
                //{
                //    cal.Official = true;
                //    cal.Status = "S";
                //}
                //else
                //{
                //    cal.Official = false;
                //    cal.Status = "N";
                //}
            }
            cal.EstadioID = Convert.ToInt32(Home.EstadioID);
            cal.PostPoned = statusDescription.ToLower().Trim() == "postponed";
            cal.StatusDescription_MLB = statusDescription;
            cal.GameNumber_MLB = Numero;
            if (CalNotFound)
            {
                //Add the new cal if it was not found
                cr.Add(cal);
            }
            //Make Sure the teams are in the standing
            sr.CreateDefaultStanding(SeasonInfo.AñoInicio, SeasonInfo.Phase, Home.ID);
            sr.CreateDefaultStanding(SeasonInfo.AñoInicio, SeasonInfo.Phase, Away.ID);
            //Clean up Dataset 2 ds2
        }


        //GetGameStatus 2018-01-09
        private void GetGameStatus(string date, int team1, int team2, int gamenumber)
        {
            /*string jsonResult = GetJsonResponse("https://statsapi.mlb.com/api/v1/schedule?sportId=17&date="+ date + "&sortBy=gameDate");
            MLBLineScore linescore = JsonConvert.DeserializeObject<MLBLineScore>(jsonResult);
            MLBLSGame result = null;
            foreach (MLBLSDate gamedate in linescore.dates)
            foreach (MLBLSGame game in gamedate.games)
            {
                if (game.teams.away.team.id == team1 && game.teams.home.team.id == team2 && game.gameNumber == gamenumber)
                {
                    result = game;
                };
            }
            return result;*/
            //return null;

        }

        private string GetJsonResponse(string url)
        {
            var request = WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();

            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                return sr.ReadToEnd();
            }
        }

        //private void ProcessCalendarOLD()
        //{
        //    // Calendar
        //    WhereAmI = "ProcessCalendar";

        //    if (ds == null)
        //        ReadBoxScoreIntoDataset();

        //    CalendarRepository cr = new CalendarRepository(db);
        //    TeamRepository tr = new TeamRepository(db);

        //    DataSet ds2 = new DataSet();
        //    XmlTextReader myXMLReader = new XmlTextReader(UrlJuego + "/game.xml");
        //    ds2.ReadXml(myXMLReader);

        //    DateTime FechaJuego = Fecha.AddMinutes(GetMinutes_From_HourMinuteFormat(ds2.Tables[0].Rows[0]["local_game_time"].ToString()));
        //    Home = tr.GetTeamBy_MLB_ShortName(ds.Tables[0].Rows[0]["home_team_code"].ToString());
        //    Away = tr.GetTeamBy_MLB_ShortName(ds.Tables[0].Rows[0]["away_team_code"].ToString());
        //    //Check if the game is in the calendar
        //    cal = cr.GetCalendarByDateAndTeam(SeasonInfo.AñoInicio, SeasonInfo.Phase, FechaJuego, Away.ID, Home.ID);
        //    if (cal == null)
        //    {
        //        cal = new Calendar();
        //        cal.SeasonID = SeasonInfo.AñoInicio;
        //        cal.PhaseID = SeasonInfo.Phase;
        //        cal.GameID = SeasonRepository.GetNewGameID(SeasonInfo.AñoInicio);
        //        cal.Team1 = Away.ID;
        //        cal.Team2 = Home.ID;
        //        cal.Fecha = FechaJuego;
        //        cal.Official = ds.Tables[0].Rows[0]["status_ind"].ToString().ToUpper() == "F";
        //        cal.GameTime = GetGameTime(ds.Tables[0].Rows[0]["game_info"].ToString());
        //        cal.Status = ds.Tables[0].Rows[0]["status_ind"].ToString().ToUpper() == "F" ? "P" : "C";
        //        cal.EstadioID = Convert.ToInt32(Home.EstadioID);
        //        cr.Add(cal);
        //    }
        //    //Clean up Dataset 2 ds2
        //    ds2.Dispose();
        //}

        private void ProcessBoxScore()
        {
            WhereAmI = "ProcessBoxScore";

            if (cal == null)
                throw new Exception("ProcessBoxScore.  Calendar Object cal not available.");

            if (Home == null || Away == null)
                throw new Exception("ProcessBoxScore.  Team Objects Home, Away not available.");

            if (boxScore == null)
                ReadBoxScoreIntoDataset();

            // BoxScore
            ExtraInning = false;
            BoxScoreRepository bxr = new BoxScoreRepository(db);
            BoxScore pizarra = new BoxScore();
            foreach (LSInnings inning in lineScore.innings)
            {
                pizarra = new BoxScore();
                pizarra.SeasonID = SeasonInfo.AñoInicio;
                pizarra.PhaseID = SeasonInfo.Phase;
                pizarra.GameID = cal.GameID;
                pizarra.Inning = inning.num;
                pizarra.TeamID = Away.ID;
                pizarra.Half = 0; //Top half of the inning
                try
                {
                    pizarra.R = inning.away.runs;
                }
                catch (Exception)
                {
                }
                bxr.Add(pizarra);
                if (pizarra.Inning == 10)
                    ExtraInning = true;
                pizarra = new BoxScore();
                pizarra.SeasonID = SeasonInfo.AñoInicio;
                pizarra.PhaseID = SeasonInfo.Phase;
                pizarra.GameID = cal.GameID;
                pizarra.Inning = inning.num;
                pizarra.TeamID = Home.ID;
                pizarra.Half = 1; //Bottom half of the inning
                try
                {
                    pizarra.R = inning.home.runs;
                }
                catch (Exception)
                {
                }
                bxr.Add(pizarra);
            }
        }

        private void ProcessGameAndStandingInformation()
        {
            WhereAmI = "ProcessGameInformation";

            if (cal == null)
                throw new Exception("ProcessGameAndStandingInformation.  Calendar Object cal not available.");

            if (Home == null || Away == null)
                throw new Exception("ProcessGameAndStandingInformation.  Team Objects Home, Away not available.");

            if (boxScore == null)
                ReadBoxScoreIntoDataset();

            // Game Information
            GameRepository gr = new GameRepository(db);
            gamea = new Game(); //Away Team Game Info
            gameh = new Game(); //Home Team Game Info
            //Set Summarized Values at Zero for each Game
            SetGameAgrValuesAtZero(gamea);
            SetGameAgrValuesAtZero(gameh);
            gamea.SeasonID = SeasonInfo.AñoInicio;
            gamea.PhaseID = SeasonInfo.Phase;
            gamea.GameID = cal.GameID;
            gamea.TeamID = Away.ID;
            gamea.Team_VS = Home.ID;
            gamea.HomeAway = "A";
            gamea.R = lineScore.teams.away.runs;
            gamea.H = lineScore.teams.away.hits;
            gamea.E = lineScore.teams.away.errors;
            gameh.SeasonID = SeasonInfo.AñoInicio;
            gameh.PhaseID = SeasonInfo.Phase;
            gameh.GameID = cal.GameID;
            gameh.TeamID = Home.ID;
            gameh.Team_VS = Away.ID;
            gameh.HomeAway = "H";
            gameh.R = lineScore.teams.home.runs;
            gameh.H = lineScore.teams.home.hits;
            gameh.E = lineScore.teams.home.errors;
            //TODO: Check if Suspended so W and L are not affected.
            if (gamea.R == gameh.R)
            {
                cal.Status = "X";
                gamea.W = 0; gamea.L = 0; gameh.W = 0; gameh.L = 0;
                gamea.W_Home = 0; gamea.L_Home = 0;
                gamea.W_Away = 0; gamea.L_Away = 0;
                gameh.W_Home = 0; gameh.L_Home = 0;
                gameh.W_Away = 0; gameh.L_Away = 0;
                gamea.W_OneRun = 0;
                gamea.L_OneRun = 0;
                gameh.W_OneRun = 0;
                gameh.L_OneRun = 0;
                gamea.W_ExtraInning = 0;
                gamea.L_ExtraInning = 0;
                gameh.W_ExtraInning = 0;
                gameh.L_ExtraInning = 0;
            }
            else
            {
                gamea.W = gamea.R > gameh.R ? 1 : 0;
                gamea.L = gamea.R < gameh.R ? 1 : 0;
                gameh.W = gamea.R > gameh.R ? 0 : 1;
                gameh.L = gamea.R < gameh.R ? 0 : 1;
                gamea.W_Away = gamea.W;
                gamea.L_Away = gamea.L;
                gamea.W_Home = 0;
                gamea.L_Home = 0;
                gameh.W_Away = 0;
                gameh.L_Away = 0;
                gameh.W_Home = gameh.W;
                gameh.L_Home = gameh.L;
                if (gamea.R - gameh.R == 1 || gameh.R - gamea.R == 1)
                {
                    gamea.W_OneRun = gamea.W;
                    gamea.L_OneRun = gamea.L;
                    gameh.W_OneRun = gameh.W;
                    gameh.L_OneRun = gameh.L;
                }
                else
                {
                    gamea.W_OneRun = 0;
                    gamea.L_OneRun = 0;
                    gameh.W_OneRun = 0;
                    gameh.L_OneRun = 0;
                }
                if (ExtraInning)
                {
                    gamea.W_ExtraInning = gamea.W;
                    gamea.L_ExtraInning = gamea.L;
                    gameh.W_ExtraInning = gameh.W;
                    gameh.L_ExtraInning = gameh.L;
                }
                else
                {
                    gamea.W_ExtraInning = 0;
                    gamea.L_ExtraInning = 0;
                    gameh.W_ExtraInning = 0;
                    gameh.L_ExtraInning = 0;
                }
                //Added 9-11-2011 - Shutout Team and Pitcher
                if (gamea.R > 0 && gameh.R == 0)
                {
                    gamea.P_TeamSHO = 1;
                }
                else if (gameh.R > 0 && gamea.R == 0)
                {
                    gameh.P_TeamSHO = 1;
                }
                //End Added 9-11-2011 - Shutout Team and Pitcher
                cal.TeamWon = gamea.W == 1 ? gamea.TeamID : gameh.TeamID;
                cal.TeamLoss = gamea.L == 1 ? gamea.TeamID : gameh.TeamID;
            }

            // Offensive Information
            gamea.VB = boxScore.teams.away.teamStats.batting.atBats;
            gamea.H2 = boxScore.teams.away.teamStats.batting.doubles;
            gamea.H3 = boxScore.teams.away.teamStats.batting.triples;
            gamea.HR = boxScore.teams.away.teamStats.batting.homeRuns;
            gamea.RBI = boxScore.teams.away.teamStats.batting.rbi;
            gamea.LOB = boxScore.teams.away.teamStats.batting.leftOnBase;
            gamea.SO = boxScore.teams.away.teamStats.batting.strikeOuts;
            gamea.BB = boxScore.teams.away.teamStats.batting.baseOnBalls;
            //Se tiene que sumarizar
            //gamea.PUTOUT = 0; // GetInt32Data(Fila, "po");
            gameh.VB = boxScore.teams.home.teamStats.batting.atBats;
            gameh.H2 = boxScore.teams.home.teamStats.batting.doubles;
            gameh.H3 = boxScore.teams.home.teamStats.batting.triples;
            gameh.HR = boxScore.teams.home.teamStats.batting.homeRuns;
            gameh.RBI = boxScore.teams.home.teamStats.batting.rbi;
            gameh.LOB = boxScore.teams.home.teamStats.batting.leftOnBase;
            gameh.SO = boxScore.teams.home.teamStats.batting.strikeOuts;
            gameh.BB = boxScore.teams.home.teamStats.batting.baseOnBalls;

            //Pitching Information
            /*foreach (DataRow Fila in ds.Tables[3].Rows)
            {
                if (Fila["team_flag"].ToString().ToLower() == "away")
                {*/
            gamea.P_H = boxScore.teams.away.teamStats.pitching.hits;
            gamea.P_HR = boxScore.teams.away.teamStats.pitching.homeRuns;
            gamea.P_R = boxScore.teams.away.teamStats.pitching.runs;
            gamea.P_ER = boxScore.teams.away.teamStats.pitching.earnedRuns;
            gamea.P_SO = boxScore.teams.away.teamStats.pitching.strikeOuts;
            gamea.P_BB = boxScore.teams.away.teamStats.pitching.baseOnBalls;
            /*}
            else if (Fila["team_flag"].ToString().ToLower() == "home")
            {*/
            gameh.P_H = boxScore.teams.home.teamStats.pitching.hits;
            gameh.P_HR = boxScore.teams.home.teamStats.pitching.homeRuns;
            gameh.P_R = boxScore.teams.home.teamStats.pitching.runs;
            gameh.P_ER = boxScore.teams.home.teamStats.pitching.earnedRuns;
            gameh.P_SO = boxScore.teams.home.teamStats.pitching.strikeOuts;
            gameh.P_BB = boxScore.teams.home.teamStats.pitching.baseOnBalls;
            /*}
            else
                throw new Exception("Boxscore table 3 problem. Team_Flag description is: " +
                    Fila["team_flag"].ToString());
        }*/

            //Double Plays Turned
            int dpa = 0, dph = 0;
            GetDoublePlays(ref dpa, ref dph);
            /*var DPdata = boxScore.teams.away.info.Where(p => p.title == "FIELDING").FirstOrDefault().fieldList.Where(p =>  p.label == "DP");
            if (DPdata.Count() > 0) dpa = Convert.ToInt32(DPdata.FirstOrDefault().value.Substring(0, DPdata.FirstOrDefault().value.IndexOf(' ')));*/
            gamea.DPT = dpa;
            /*DPdata = boxScore.teams.home.info.Where(p => p.title == "FIELDING").FirstOrDefault().fieldList.Where(p => p.label == "DP");
            if (DPdata.Count() > 0) dph = Convert.ToInt32(DPdata.FirstOrDefault().value.Substring(0, DPdata.FirstOrDefault().value.IndexOf(' ')));*/
            gameh.DPT = dph;

            gr.Add(gamea);
            gr.Add(gameh);

            ////Standing Repository for the Streak and W/L Update
            ////StandingRepository sr = new StandingRepository(db);
            ////Standing Streak and W/L Update
            //Standing standinga = sr.GetStanding(SeasonInfo.AñoInicio, SeasonInfo.Phase, Away.ID);
            //Standing standingh = sr.GetStanding(SeasonInfo.AñoInicio, SeasonInfo.Phase, Home.ID);
            //standinga.Streak = GetStreakValue((int)gamea.W, (int)standinga.Streak);
            //standingh.Streak = GetStreakValue((int)gameh.W, (int)standingh.Streak);
            //if (gamea.W == 1)
            //    standinga.W++;
            //else
            //    standinga.L++;
            //if (gameh.W == 1)
            //    standingh.W++;
            //else
            //    standingh.L++;
        }

        private void ProcessPlayerInformation()
        {
            WhereAmI = "ProcessPlayerInformation";

            if (cal == null)
                throw new Exception("ProcessPlayerInformation.  Calendar Object cal not available.");

            if (Home == null || Away == null)
                throw new Exception("ProcessPlayerInformation.  Team Objects Home, Away not available.");

            if (boxScore == null)
                ReadBoxScoreIntoDataset();

            //Setup Processing
            //6-12-2012
            //Player Season Stats Eliminated and Commented
            //Player_Season_StatsRepository pssr = new Player_Season_StatsRepository(db);

            foreach (int batPlayers in boxScore.teams.away.batters)
            {
                ProcessBoxPlayer(batPlayers, "A");
            }
            foreach (int batPlayers in boxScore.teams.home.batters)
            {
                ProcessBoxPlayer(batPlayers, "H");
            }
        }

        private void ProcessBoxPlayer(int intBatter, string HomeAway)
        {
            WhereAmI = "ProcessGameEvents Batter " + intBatter.ToString();
            BSPlayer bsPlayer = (HomeAway == "A") ? boxScore.teams.away.players.Where(p => p.person.id == intBatter).FirstOrDefault()
                : boxScore.teams.home.players.Where(p => p.person.id == intBatter).FirstOrDefault();
            PlayerRepository pr = new PlayerRepository(db);
            Player player = pr.GetPlayerByIDMLB(intBatter);
            if (player == null)
            {
                GDPlayer toAddPlayer = feedLive.gameData.players.Where(p => p.id == intBatter).FirstOrDefault();
                //Adds a New Players and Commits the Changes
                player = new Player();
                //player.IDMLB = GetInt32Data(Fila, "id");
                player.IDMLB = intBatter;
                player.Nombre = toAddPlayer.useName;
                player.Apellido = toAddPlayer.lastName;
                player.IDAlterno = player.Nombre.Substring(0, 2) + player.Apellido + "9" + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + "9";
                // 29-01-2017
                if (player.IDAlterno.Length > 25)
                {
                    player.IDAlterno = player.IDAlterno.Substring(0, 25);
                }

                player.FechaNacimiento = DateTime.Parse(toAddPlayer.birthDate);
                player.LugarNacimiento = (toAddPlayer.birthCountry == "USA") 
                    ? toAddPlayer.birthCity + ", " + toAddPlayer.birthStateProvince 
                    : toAddPlayer.birthCity + ", " + toAddPlayer.birthCountry;
                string[] height = toAddPlayer.height.Replace("'", "").Replace("\"", "").Split(' ');
                player.Height = Convert.ToInt32(height[0]) * 12 + Convert.ToInt32(height[1]);
                player.Weight = toAddPlayer.weight;
                player.Bats = toAddPlayer.batSide.code;
                player.Throws = toAddPlayer.pitchHand.code;
                player.Activo = true;
                player.Position = toAddPlayer.primaryPosition.abbreviation;
                player.CurrentTeamID = HomeAway=="H"?Home.ID:Away.ID;
                PlayerRepository.AddAndSave(player);
            }
            if (bsPlayer.battingOrder!=null)
            {
                string Orden; //Variable to make more readable the code.
                Player_Game_StatsRepository pgsr = new Player_Game_StatsRepository(db);
                // 19-1-2013
                // Caso Juan Carlos Pérez
                int tempID = Convert.ToInt32(bsPlayer.person.id);
                if (tempID == 467819)
                    tempID = 543633;
                //Player player = pr.GetPlayerByIDMLB(GetInt32Data(Fila, "id"));

                string playerPos = bsPlayer.position.abbreviation.ToUpper();
                Orden = bsPlayer.battingOrder;
                // Get Pitcher Errors to use at PitcherStats Process
                //if (Orden == "" && Fila["pos"].ToString().ToUpper() == "P")
                //Caso Vizcaino 8-1-2011 Emergente por 8vo Bate Esc-Gig
                if (playerPos == "P")
                {
                    PitcherError PE = new PitcherError();
                    PE.IDMLB = player.IDMLB;
                    PE.Errors = bsPlayer.stats.fielding.errors;
                    ErroresPitcher.Add(PE);
                }
                Player_Game_Stat PlayerGS = new Player_Game_Stat();
                PlayerGS.SeasonID = SeasonInfo.AñoInicio;
                PlayerGS.PhaseID = SeasonInfo.Phase;
                PlayerGS.GameID = cal.GameID;
                PlayerGS.PlayerID = player.ID;
                PlayerGS.TeamID = HomeAway == "H" ? Home.ID : Away.ID;
                PlayerGS.Team_VS = HomeAway == "H" ? Away.ID : Home.ID;
                PlayerGS.HomeAway = HomeAway;
                //if (player.LugarNacimiento == "PENDIENTE")
                //{
                //    //Fix Current TeamID and Save it to the Database
                //    if (SeasonInfo.UpdateCurrentTeamID)
                //        PlayerRepository.UpdateCurrentTeam(player.ID, PlayerGS.TeamID);
                //    //Update Current Player reference
                //    //TODO: TEST IF SAVE PERSISTS WITHOUT PRIOR CALL TO UPDATE CURRENTTEAMID.  COMMENT THAT LINE
                //    player.CurrentTeamID = PlayerGS.TeamID;
                //}
                if (SeasonInfo.UpdateCurrentTeamID)
                {
                    PlayerRepository.UpdateCurrentTeam(player.ID, PlayerGS.TeamID);
                    player.CurrentTeamID = PlayerGS.TeamID;
                }
                PlayerGS.Position = playerPos;
                PlayerGS.Orden = Convert.ToInt32(Orden);
                PlayerGS.Emergente = Orden.Substring(1) != "00";
                PlayerGS.R = bsPlayer.stats.batting.runs;
                PlayerGS.H = bsPlayer.stats.batting.hits;
                PlayerGS.E = bsPlayer.stats.fielding.errors;
                PlayerGS.VB = bsPlayer.stats.batting.atBats;
                PlayerGS.H2 = bsPlayer.stats.batting.doubles;
                PlayerGS.H3 = bsPlayer.stats.batting.triples;
                PlayerGS.HR = bsPlayer.stats.batting.homeRuns;
                PlayerGS.RBI = bsPlayer.stats.batting.rbi;
                PlayerGS.SB = bsPlayer.stats.batting.stolenBases;
                PlayerGS.SO = bsPlayer.stats.batting.strikeOuts;
                PlayerGS.BB = bsPlayer.stats.batting.baseOnBalls;
                PlayerGS.HBP = bsPlayer.stats.batting.hitByPitch;
                PlayerGS.SF = bsPlayer.stats.batting.sacFlies;
                PlayerGS.PUTOUT = bsPlayer.stats.fielding.putOuts;
                PlayerGS.ASSIST = bsPlayer.stats.fielding.assists;
                PlayerGS.PASSEDBALL = bsPlayer.stats.fielding.passedBall;
                PlayerGS.S = bsPlayer.stats.batting.sacBunts;
                PlayerGS.CS = bsPlayer.stats.batting.caughtStealing;
                //Added 26-9-2011
                //Added 8-10-2011

                ProcessGameEvents(PlayerGS, player.IDMLB);
                pgsr.Add(PlayerGS);
                //6-12-2012
                //Player Season Stats Eliminated and Commented
                //pssr.CreateDefaultPlayerSeasonStat(SeasonInfo.AñoInicio, SeasonInfo.Phase, player.ID, PlayerGS.TeamID);
            }
        }


        private void ProcessPitcherInformation()
        {
            WhereAmI = "ProcessPitcherInformation";

            if (cal == null)
                throw new Exception("ProcessPitcherInformation.  Calendar Object cal not available.");

            if (Home == null || Away == null)
                throw new Exception("ProcessPitcherInformation.  Team Objects Home, Away not available.");

            if (gameh == null || gamea == null)
                throw new Exception("ProcessPitcherInformation.  Game Objects gameh, gamea not available.");

            if (boxScore == null)
                ReadBoxScoreIntoDataset();


            //Added 14-9-2011
            //6-12-2012
            //Player Season Stats Eliminated and Commented
            //Player_Season_StatsRepository pssr = new Player_Season_StatsRepository(db);

            //foreach (DataRow Fila in ds.Tables[4].Rows)
            Orden = 0;
            foreach (int bsPitcher in boxScore.teams.away.pitchers)
            {
                ProcessBoxPitcher(bsPitcher, "A");
            }
            Orden = 0;
            foreach (int bsPitcher in boxScore.teams.home.pitchers)
            {
                ProcessBoxPitcher(bsPitcher, "H");
            }

        }
        private void ProcessBoxPitcher(int intPitcher, string HomeAway)
        {
            BSPlayer bsPitcher = (HomeAway == "H") ? boxScore.teams.home.players.Where(p => p.person.id == intPitcher).FirstOrDefault()
                : boxScore.teams.away.players.Where(p => p.person.id == intPitcher).FirstOrDefault();

            WhereAmI = "ProcessBoxPitcher " + bsPitcher.person.id;
            if (bsPitcher.person.id == 516714)
            {

            }
            PlayerRepository pr = new PlayerRepository(db);
            Pitcher_Game_StatsRepository pitr = new Pitcher_Game_StatsRepository(db);
                TeamRepository tr = new TeamRepository(db);
            Player player = pr.GetPlayerByIDMLB(bsPitcher.person.id);
                if (player == null)
                    throw new Exception("ProcessPitcherInformation.  Pitcher not found in database.  IDMLB='" + bsPitcher.person.id.ToString() + "'");
                Pitcher_Game_Stat PitcherGS = new Pitcher_Game_Stat();
                PitcherGS.SeasonID = SeasonInfo.AñoInicio;
                PitcherGS.PhaseID = SeasonInfo.Phase;
                PitcherGS.GameID = cal.GameID;
                PitcherGS.TeamID = (HomeAway=="H")?Home.ID:Away.ID;
                PitcherGS.PlayerID = player.ID;
                PitcherGS.Team_VS = (HomeAway == "A") ? Home.ID : Away.ID;
            PitcherGS.Orden = ++Orden;
                PitcherGS.HomeAway = HomeAway;
                PitcherGS.P_Games = 1;
                PitcherGS.P_Starts = Orden == 1 ? 1 : 0;
                if (SeasonInfo.UpdateCurrentTeamID)
                {
                    PlayerRepository.UpdateCurrentTeam(player.ID, PitcherGS.TeamID);
                    player.CurrentTeamID = PitcherGS.TeamID;
                }
                //if (Fila["note"].ToString().Length >= 2)
                {
                //PitcherGS.W = Fila["note"].ToString().Substring(1, 1).ToUpper() == "W" ? 1 : 0;
                //PitcherGS.L = Fila["note"].ToString().Substring(1, 1).ToUpper() == "L" ? 1 : 0;
                //PitcherGS.P_HOLDS = Fila["note"].ToString().Substring(1, 1).ToUpper() == "H" ? 1 : 0;
                //PitcherGS.P_SV = Fila["note"].ToString().Substring(1, 1).ToUpper() == "S" ? 1 : 0;
                //PitcherGS.P_BLOWNSAVES = Fila["note"].ToString().Substring(1, 1).ToUpper() == "B" ? 1 : 0;
                //PitcherGS.W = Fila["note"].ToString().ToUpper().IndexOf("(W") >= 0 ? 1 : 0;
                //PitcherGS.L = Fila["note"].ToString().ToUpper().IndexOf("(L") >= 0 ? 1 : 0;
                //PitcherGS.P_HOLDS = Fila["note"].ToString().ToUpper().IndexOf("(H") >= 0 ? 1 : 0;
                //PitcherGS.P_SV = Fila["note"].ToString().ToUpper().IndexOf("(S") >= 0 ? 1 : 0;
                //PitcherGS.P_BLOWNSAVES = Fila["note"].ToString().ToUpper().IndexOf("(B") >= 0 ? 1 : 0;
                if (bsPitcher.stats.pitching.note != null)
                {
                    PitcherGS.W = bsPitcher.stats.pitching.note.Contains("(W") ? 1 : 0;
                    PitcherGS.L = bsPitcher.stats.pitching.note.Contains("(L") ? 1 : 0;
                    PitcherGS.P_HOLDS = bsPitcher.stats.pitching.note.Contains("(H") ? 1 : 0;
                    PitcherGS.P_SV = bsPitcher.stats.pitching.note.Contains("(S") ? 1 : 0;
                    PitcherGS.P_BLOWNSAVES = bsPitcher.stats.pitching.note.Contains("(BS") ? 1 : 0;
                }
            }
            /*else
            {
                PitcherGS.W = 0;
                PitcherGS.L = 0;
                PitcherGS.P_HOLDS = 0;
                PitcherGS.P_SV = 0;
                PitcherGS.P_BLOWNSAVES = 0;
            }*/
            PitcherGS.P_Innings = bsPitcher.stats.pitching.outs;
            PitcherGS.P_H = bsPitcher.stats.pitching.hits;
            PitcherGS.P_HR = bsPitcher.stats.pitching.homeRuns;
            PitcherGS.P_R = bsPitcher.stats.pitching.runs;
            PitcherGS.P_ER = bsPitcher.stats.pitching.earnedRuns;
            PitcherGS.P_SO = bsPitcher.stats.pitching.strikeOuts;
            PitcherGS.P_BB = bsPitcher.stats.pitching.baseOnBalls;
                PitcherGS.P_BALK = 0; //Manual input later
                PitcherGS.P_WP = (bsPitcher.stats.pitching.wildPitches==null)?0:bsPitcher.stats.pitching.wildPitches.Value; //Manual input later
                PitcherGS.P_PICKOFF = 0; //Manual input later
                PitcherGS.P_BATSFACED = bsPitcher.stats.pitching.battersFaced;
                PitcherGS.P_BATSCORED = 0; //Manual input later
                PitcherGS.P_BATSINH = 0; //Manual input later
            PitcherGS.P_SB = gameEvents.allPlays.Where(p => p.sBPitcherID == intPitcher).Sum(o => o.stolenBases);
            PitcherGS.P_CS = gameEvents.allPlays.Where(p => p.sBPitcherID == intPitcher).Sum(o => o.caughtStealing);
            PitcherGS.P_RS = HomeAway == "H" ? gameh.R : gamea.R;
                // 16-12-2016.  Vino vacío en juego Esc-Gig en 15-12-2016.  Segundo juego.
                try
                {
                    PitcherGS.P_E = ErroresPitcher.First(m => m.IDMLB == player.IDMLB).Errors;
                }
                catch (Exception)
                {
                    PitcherGS.P_E = 0;
                }
                //Added 28-10-2011
                //Added 10-11-2011
                PitcherGS.P_TeamSHO = 0;
                //Added 13-10-2011
                if (PitcherGS.P_Starts == 1)
                {
                    //Added 9-11-2011 - Shutout Team and Pitcher
                    if (gamea.P_TeamSHO == 1 && PitcherGS.TeamID == gamea.TeamID)
                        PitcherGS.P_TeamSHO = 1;
                    if (gameh.P_TeamSHO == 1 && PitcherGS.TeamID == gameh.TeamID)
                        PitcherGS.P_TeamSHO = 1;
                    //End Added 9-11-2011 - Shutout Team and Pitcher
                    if (PitcherGS.P_Innings >= 15 && PitcherGS.P_ER <= 2)
                    {
                        PitcherGS.P_QS = 1;
                    }
                    else if (PitcherGS.P_Innings >= 18 && PitcherGS.P_ER <= 3)
                    {
                        PitcherGS.P_QS = 1;
                    }
                    else if (PitcherGS.P_Innings >= 23 && PitcherGS.P_ER <= 4)
                    {
                        PitcherGS.P_QS = 1;
                    }
                    else
                    {
                        PitcherGS.P_QS = 0;
                    }
                }
                else
                {
                    PitcherGS.P_QS = 0;
                }
                //End Added 13-10-2011
                //ProcessGameEvents(PitcherGS, player.IDMLB);
                
                var PlayerPlays = gameEvents.allPlays.Where(p => p.matchup.batter.id == bsPitcher.person.id).ToList();
                PitcherGS.P_IBB = PlayerPlays.Where(p => p.result.@event.ToLower().Contains("intent walk")).Count();
                PitcherGS.P_HBP = bsPitcher.stats.pitching.hitBatsmen;
                PitcherGS.P_GROUND = PlayerPlays.Where(p=> (p.result.@event.ToLower().IndexOf("groundout") >= 0 || p.result.@event.ToLower().IndexOf("grounded") >= 0 || p.result.@event.ToLower().IndexOf("sac bunt") >= 0 || p.result.description.IndexOf("ground") >= 0)).Count();
                PitcherGS.P_FLY = PlayerPlays.Where(p => (p.result.@event.Contains(" fly") || p.result.@event.Contains(" flies") || p.result.@event.Contains(" pop") || p.result.@event.Contains(" line"))).Count(); ;
                //Added 28-9-2011
                PitcherGS.P_S = PlayerPlays.Where(p => p.result.@event.ToLower().Contains("sac bunt")).Count(); ;
                PitcherGS.P_SF = PlayerPlays.Where(p => p.result.@event.ToLower().Contains("sac fly")).Count(); ;
                PitcherGS.P_GIDP = PlayerPlays.Where(p => p.result.@event.ToLower() == "grounded into dp").Count();
                BSHomeAway bsPitTeam;
                if (boxScore.teams.away.pitchers.Contains(bsPitcher.person.id))
                {
                    bsPitTeam = boxScore.teams.away;
                }
                else //if(boxScore.teams.home.pitchers.Contains(bsPitcher.person.id))
                {
                    bsPitTeam = boxScore.teams.home;
                }
                PitcherGS.P_CG = bsPitcher.stats.pitching.outs == bsPitTeam.teamStats.pitching.outs?1:0;
                PitcherGS.P_SHO = (bsPitcher.stats.pitching.outs == bsPitTeam.teamStats.pitching.outs && bsPitcher.stats.pitching.runs == 0 ) ? 1 : 0 ;
                pitr.Add(PitcherGS);
                //Added 14-9-2011
                //6-12-2012
                //Player Season Stats Eliminated and Commented
                //pssr.CreateDefaultPlayerSeasonStat(SeasonInfo.AñoInicio, SeasonInfo.Phase, player.ID, PitcherGS.TeamID);
            
        }


        private void ProcessHitCharts()
        {
            WhereAmI = "ProcessHitCharts";

            if (cal == null)
                throw new Exception("ProcessHitCharts.  Calendar Object cal not available.");

            if (gameh == null || gamea == null)
                throw new Exception("ProcessPitcherInformation.  Game Objects gameh, gamea not available.");

            if (boxScore == null)
                ReadBoxScoreIntoDataset();

            TeamRepository tr = new TeamRepository(db);

            //DataSet ds2 = new DataSet();
            //XmlTextReader myXMLReader = new XmlTextReader(UrlJuego + "/inning/inning_hit.xml");
            //ds2.ReadXml(myXMLReader);
            //int i = 0;
            foreach (Playsbyinning PBI in feedLive.liveData.plays.playsByInning.Where(p => p.hits != null))
            //foreach (DataRow Fila in ds2.Tables[0].Rows)
            {
                WhereAmI = "ProcessHitCharts " + PBI.startIndex + " " + PBI.endIndex;

                if (PBI.startIndex == 60)
                {

                }
                ProcessHitChart(PBI);
            }
            //clean up
            //ds2.Dispose();
        }

        private void ProcessHitChart(Playsbyinning hitsData)
        {
            HitChartRepository hcr = new HitChartRepository(db);
            PlayerRepository pr = new PlayerRepository(db);
            int topindex = hitsData.top.Count() > 0 ? hitsData.top[0] : 0;
            int botindex = hitsData.bottom.Count() > 0 ? hitsData.bottom[0] : 0;
            var AllHits = hitsData.hits.away.Select((p,i) => new { id = topindex + i , p.team, p.inning, p.pitcher, p.batter, p.coordinates, p.type, p.description, HomeAway = "A" })
                .Concat(
                hitsData.hits.home.Select((p,i) => new { id = botindex + i, p.team, p.inning, p.pitcher, p.batter, p.coordinates, p.type, p.description, HomeAway = "H" }));
            BBDataLib.BBDataLoader dl = new BBDataLib.BBDataLoader("");
            dl.CreatePlayField();
            foreach (var a in AllHits)
            {
                HitChart hc = new HitChart();
                hc.SeasonID = SeasonInfo.AñoInicio;
                hc.PhaseID = SeasonInfo.Phase;
                hc.GameID = cal.GameID;
                hc.ID = a.id;
                hc.TeamID = a.HomeAway == "A" ? Away.ID : Home.ID;
                hc.Team_VS = a.HomeAway == "H" ? Away.ID : Home.ID;
                hc.PlayerID = pr.GetPlayerByIDMLB(a.batter.id).ID;
                hc.PitcherID = pr.GetPlayerByIDMLB(a.pitcher.id).ID;
                hc.HomeAway = a.HomeAway;
                hc.X = Convert.ToDouble(a.coordinates.x);
                hc.Y = Convert.ToDouble(a.coordinates.y);
                hc.Tipo = a.type;
                hc.Description = a.description;
                hc.Inning = a.inning;
                hc.Zone = dl.GetZone(a.coordinates.x.ToString(), a.coordinates.y.ToString());
                hcr.Add(hc);
            }
        }

        private void ProcessBatSituations()
        {
            WhereAmI = "ProcessBatSituations";

            if (cal == null)
                throw new Exception("ProcessHitCharts.  Calendar Object cal not available.");

            if (boxScore == null)
                ReadBoxScoreIntoDataset();

            /*if (gameEvents == null)
                ReadGameEventsIntoDataset();*/

            BatSituationRepository bsr = new BatSituationRepository(db);
            PlayerRepository pr = new PlayerRepository(db);

            //DataRow FilaAnterior = null;
            int i = 0;
            //foreach (DataRow Fila in dsGE.Tables[TablaAtBats].Rows)
            foreach(LDplay eventRow in gameEvents.allPlays)
            {
                string Evento = eventRow.result.@event.ToLower();
                if (eventRow.result.type== "atBat")
                {
                    BatSituation bs = new BatSituation();
                    bs.SeasonID = SeasonInfo.AñoInicio;
                    bs.PhaseID = SeasonInfo.Phase;
                    bs.GameID = cal.GameID;
                    bs.ID = ++i;
                    //bs.ID = GetInt32Data(Fila, "num");
                    bs.TeamID = eventRow.about.halfInning == "top" ? gamea.TeamID : gameh.TeamID;
                    bs.Team_VS = eventRow.about.halfInning == "top" ? gameh.TeamID : gamea.TeamID;
                    bs.PlayerID = pr.GetPlayerByIDMLB(eventRow.matchup.batter.id).ID;
                    bs.PitcherID = pr.GetPlayerByIDMLB(eventRow.matchup.pitcher.id).ID;
                    bs.HomeAway = eventRow.about.halfInning == "top" ? "A" : "H";
                    bs.SituationID = "S" + (eventRow.runners.Any(p => p.movement.start == "1B")?"1":"0") + (eventRow.runners.Any(p => p.movement.start == "2B") ? "1" : "0") + (eventRow.runners.Any(p => p.movement.start == "3B") ? "1" : "0");
                    bs.Outs = eventRow.count.outs;
                    if (Evento.IndexOf("walk") >= 0 ||
                        Evento.IndexOf("hit by pitch") >= 0 ||
                        Evento.IndexOf("sac ") >= 0)
                    {
                        bs.VB = 0;
                    }
                    else
                    {
                        bs.VB = 1;
                    }
                    bs.H = IsEventHit(Evento) ? 1 : 0;
                    bs.H2 = Evento == "double" ? 1 : 0;
                    bs.H3 = Evento == "triple" ? 1 : 0;
                    bs.HR = Evento == "home run" ? 1 : 0;
                    bs.RBI = eventRow.result.rbi;
                    bs.Walk = Evento.IndexOf("walk") >= 0 ? 1 : 0;
                    bs.HBP = Evento.IndexOf("hit by pitch") >= 0 ? 1 : 0;
                    bs.SO = Evento.IndexOf("strikeout") >= 0 ? 1 : 0;
                    bsr.Add(bs);
                }
                //FilaAnterior debe ser Null cuando hicieron el 3er out o cambio de Inning
            }
        }

        //27-10-2013
        private void ProcessGameEmergencyResult(GameEmergencyResult ger)
        {
            // Game Information
            GameRepository gr = new GameRepository(db);
            Game game1 = new Game();
            Game game2 = new Game();
            SetGameAgrValuesAtZero(game1);
            SetGameAgrValuesAtZero(game2);
            SetGameAgrValuesAtZero2(game1);
            SetGameAgrValuesAtZero2(game2);
            game1.SeasonID = SeasonInfo.AñoInicio;
            game1.PhaseID = SeasonInfo.Phase;
            game1.GameID = cal.GameID;
            game1.TeamID = ger.Team1;
            game1.Team_VS = ger.Team2;
            game1.HomeAway = "A";
            game2.SeasonID = SeasonInfo.AñoInicio;
            game2.PhaseID = SeasonInfo.Phase;
            game2.GameID = cal.GameID;
            game2.TeamID = ger.Team2;
            game2.Team_VS = ger.Team1;
            game2.HomeAway = "H";
            game1.R = ger.R1;
            game2.R = ger.R2;
            game1.H = ger.H1;
            game2.H = ger.H2;
            game1.E = ger.E1;
            game2.E = ger.E2;

            game1.W = game1.R > game2.R ? 1 : 0;
            game1.L = game1.R < game2.R ? 1 : 0;
            game2.W = game1.R > game2.R ? 0 : 1;
            game2.L = game1.R < game2.R ? 0 : 1;
            game1.W_Away = game1.W;
            game1.L_Away = game1.L;
            game1.W_Home = 0;
            game1.L_Home = 0;
            game2.W_Away = 0;
            game2.L_Away = 0;
            game2.W_Home = game2.W;
            game2.L_Home = game2.L;
            if (game1.R - game2.R == 1 || game2.R - game1.R == 1)
            {
                game1.W_OneRun = game1.W;
                game1.L_OneRun = game1.L;
                game2.W_OneRun = game2.W;
                game2.L_OneRun = game2.L;
            }
            else
            {
                game1.W_OneRun = 0;
                game1.L_OneRun = 0;
                game2.W_OneRun = 0;
                game2.L_OneRun = 0;
            }
            if (ger.ExtraInnings)
            {
                game1.W_ExtraInning = game1.W;
                game1.L_ExtraInning = game1.L;
                game2.W_ExtraInning = game2.W;
                game2.L_ExtraInning = game2.L;
            }
            else
            {
                game1.W_ExtraInning = 0;
                game1.L_ExtraInning = 0;
                game2.W_ExtraInning = 0;
                game2.L_ExtraInning = 0;
            }
            //27-10-2013 //Added 9-11-2011 - Shutout Team and Pitcher
            if (game1.R > 0 && game2.R == 0)
            {
                game1.P_TeamSHO = 1;
            }
            else if (game2.R > 0 && game1.R == 0)
            {
                game2.P_TeamSHO = 1;
            }
            //27-10-2013 //End Added 9-11-2011 - Shutout Team and Pitcher
            cal.TeamWon = game1.W == 1 ? game1.TeamID : game2.TeamID;
            cal.TeamLoss = game1.L == 1 ? game1.TeamID : game2.TeamID;
            cal.Official = true;
            gr.Add(game1);
            gr.Add(game2);
        }
        //END 27-10-2013

        public Boolean ProcessGame()
        {
            //View Example or Sample
            //BeisbolDataEntities kkk = new BeisbolDataEntities();
            //var k2 = from t in kkk.V_BoxScore where t.GameID == 4 orderby t.Inning, t.Half select t;
            //var k3 = kkk.V_Official_Games.ToList();
            //var k = kkk.V_BoxScore.Where(m => m.GameID == 4).OrderBy(m => m.Inning).ThenBy(m => m.Half);
            //foreach (V_BoxScore item in k)
            //{
            //    int? l = item.R;
            //}
            //return false;
            WhereAmI = "ProcessGame";
            //if (Valid)
            {
                try
                {
                    //setup
                    try
                    {
                        ReadBoxScoreIntoDataset();
                        BoxScoreDisponible = true;
                    }
                    catch 
                    {
                        BoxScoreDisponible = false;
                    }

                    db = new BeisbolDataEntities();
                    ErroresPitcher = new List<PitcherError>();
                    //Find Game in Calendar or Create New Game in the Calendar
                    ProcessCalendar();
                    //First Delete Current Game Information from the System.
                    //This is BoxScores, Games, Player_Game_Stats, Pitcher_Game_Stats, HitCharts and BatSituations
                    int RecordsAffected;
                    RecordsAffected = db.DeleteGame_Complete(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID);

                    // 27-10-2013
                    // GameEmergencyResult
                    GameEmergencyResult ger;
                    bool GameOverride = false;
                    GameEmergencyResultRepository gerr = new GameEmergencyResultRepository(db);
                    ger = gerr.GetGameEmergencyResultByID(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID);
                    GameOverride = (ger != null && ger.Override);

                    // 27-10-2013
                    if (GameOverride)
                    {
                        // Create Game with Game Emergency Result Data
                        ProcessGameEmergencyResult(ger);
                    }
                    // End GameEmergencyResult

                    if (BoxScoreDisponible && !GameOverride) // 27-10-2013 GameOverride
                    {
                        /*ReadGameEventsIntoDataset();*/

                        ProcessBoxScore();
                        ProcessGameAndStandingInformation();
                        ProcessPlayerInformation();
                        ProcessPitcherInformation();
                        ProcessHitCharts();
                        ProcessBatSituations();
                        // ESTE ES // BBDataLib.BBDataLoader myLoader = new BBDataLib.BBDataLoader("C:\\TEMP\\");
                        //string theJuego = db.GetRetroID(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID).ElementAt(0).ToString();
                        //myLoader.ProcessGame(db.GetRetroID(SeasonInfo.AñoInicio,SeasonInfo.Phase,cal.GameID).ToString());
                        // ESTE ES // myLoader.ProcessGame(Juego);

                    }

                    //throw new Exception("Prueba error!");

                    // Grabar cambios en Base de Datos
                    WhereAmI = "ProcessGame.  Before Save Changes";
                    db.SaveChanges();
                    //Complete aggregations for Players and Pitchers for Both Teams in the game
                    if (BoxScoreDisponible)
                    {
                        GameRepository gr = new GameRepository();
                        RecordsAffected = gr.UpdateGameStats(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID);

                        //RecordsAffected = db.UpdateGamesFromPlayerStatsByGame(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID);
                        //RecordsAffected = db.UpdateGamesFromPitcherStatsByGame(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID);

                        //RecordsAffected = db.UpdateGamesFromPlayerStats(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID, cal.Team1);
                        //RecordsAffected = db.UpdateGamesFromPlayerStats(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID, cal.Team2);
                        //RecordsAffected = db.UpdateGamesFromPitcherStats(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID, cal.Team1);
                        //RecordsAffected = db.UpdateGamesFromPitcherStats(SeasonInfo.AñoInicio, SeasonInfo.Phase, cal.GameID, cal.Team2);
                        //ds.Dispose();
                        //30-10-2013 
                        //EVENTS
                        //BBDataLib.BBDataLoader myBBData = new BBDataLib.BBDataLoader(@"e:\JFR Backup\Temp\events\");
                        try
                        {
                            //myBBData.ProcessGame(Juego);
                        }
                        catch (Exception)
                        {
                            //int j = 0;
                        }
                    }
                    db.Dispose();
                }
                catch (Exception e)
                {
                    ErrorInfo.ErrorDescription = e.Message;
                    ErrorInfo.UrlJuego = this.UrlJuego;
                    ErrorInfo.FechaJuego = this.Fecha;
                    ErrorInfo.Juego = this.Juego;
                    ErrorInfo.URL = URL;
                    ErrorInfo.WhereWereYou = this.WhereAmI;
                    db.Dispose();
                    return false;
                    //throw;
                }
                return true;
            }

        }
        public static int UpdateStandingStats(int SeasonID, byte PhaseID)
        {
            StandingRepository sr = new StandingRepository();
            return sr.UpdateStandingStats(SeasonID, PhaseID);
            //BeisbolDataEntities mydb = new BeisbolDataEntities();
            //int i = mydb.UpdateStandingFromGamesBySeasonPhase(SeasonID, PhaseID);
            //mydb.Dispose();
            //return i;
        }
        public static int UpdateSeasonStats(int SeasonID, byte PhaseID)
        {
            Player_Season_StatsRepository psr = new Player_Season_StatsRepository();
            return psr.UpdateSeasonStats(SeasonID, PhaseID);
            //BeisbolDataEntities mydb = new BeisbolDataEntities();
            //int i = mydb.UpdatePlayerSeasonStatsFromPlayerStatsBySeasonPhase(SeasonID, PhaseID);
            //i += mydb.UpdatePlayerSeasonStatsFromPitcherStatsBySeasonPhase(SeasonID, PhaseID);
            //mydb.Dispose();
            //return i;
        }

        public static int UpdateGameStats(int SeasonID, byte PhaseID)
        {
            GameRepository gr = new GameRepository();
            return gr.UpdateGameStats(SeasonID, PhaseID);
        }

        public static int UpdateStreak(int SeasonID, byte PhaseID)
        {
            int i;
            StandingRepository sr = new StandingRepository();
            i = sr.UpdateStreak(SeasonID, PhaseID);
            sr.Save();
            return i;
        }

        public static int UpdateLast10GamesRecord(int SeasonID, byte PhaseID)
        {
            int i;
            StandingRepository sr = new StandingRepository();
            i = sr.UpdateLast10GamesRecord(SeasonID, PhaseID);
            sr.Save();
            return i;
        }
    } //End Class GameData
}



