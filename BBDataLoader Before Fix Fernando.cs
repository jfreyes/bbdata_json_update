﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Drawing;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using BeisbolData.Data;
using BeisbolData.Data.Model;

namespace BBDataLib
{
    public class BBDataLoader
    {
        private StreamWriter EventFile;
        private XmlDocument BoxDoc, PlayersDoc, EventsDoc, HitDoc;
        private string PlayerID, GameID, Ruta, lastAdvance, strSwitch1, strSwitch2, outputDir;
        public CWLib Wrapper;
        private bool RunnerAdvancement;

        public class FieldZone
        {
            public string Name;
            public Point[] Polygon;
        }

        public string Zona;
        public FieldZone[] PlayField;

        public BBDataLoader(string OutputDirectory) {
            outputDir = OutputDirectory;
        
        }
        private StreamWriter LogFile;

        public bool ProcessGame(string JuegoID)
        {
            try
            {
                LogFile = new StreamWriter(outputDir + JuegoID + ".log");
                LogFile.WriteLine("ProcessGame:" + JuegoID);
                CreatePlayField();
                bool GameAvailable = true;
                GameID = JuegoID;
                Ruta = "http://gd2.mlb.com/components/game/win/year_" + GameID.Substring(4, 4) + "/month_" + GameID.Substring(9, 2) + "/day_" + GameID.Substring(12, 2) + "/";
                Wrapper = new CWLib();

                BoxDoc = new XmlDocument();

                EventFile = new StreamWriter(outputDir + GameID + ".eva");
                string FileName = Ruta + GameID + "/boxscore.xml";
                //listBox1.Items.Add(FileName);

                //string GameID = "gid_2012_10_14_escwin_licwin_1";
                //string GameID = "gid_2012_10_17_estwin_licwin_1";
                //string GameID = "gid_2011_11_26_gigwin_licwin_1";
                LogFile.WriteLine("Processing EventFileHeader...");
                GameIDOut(GameID);
                //EventFile.WriteLine(Ruta + GameID + "/boxscore.xml");
                LogFile.WriteLine("Loading BoxScores...");

                try
                {
                    BoxDoc.Load(Ruta + GameID + "/boxscore.xml");
                }
                catch
                {
                    GameAvailable = false;
                    //throw new System.InvalidOperationException("Boxscore del partido " + GameID + " no está disponible");
                }
                if (GameAvailable)
                {

                    LogFile.WriteLine("Processing BoxScores...");
                    XmlNode BoxNodo;
                    BoxNodo = BoxDoc.DocumentElement.SelectSingleNode("//boxscore");
                    EventFile.WriteLine("info,visteam," + ((BoxNodo.Attributes["away_team_code"] != null) ? BoxNodo.Attributes["away_team_code"].Value.ToUpper() : ""));
                    EventFile.WriteLine("info,hometeam," + ((BoxNodo.Attributes["home_team_code"] != null) ? BoxNodo.Attributes["home_team_code"].Value.ToUpper() : ""));
                    EventFile.WriteLine("info,site," + ((BoxNodo.Attributes["venue_id"] != null) ? BoxNodo.Attributes["venue_id"].Value : ""));
                    EventFile.WriteLine("info,date," + ((BoxNodo.Attributes["game_id"] != null) ? BoxNodo.Attributes["game_id"].Value.Substring(0, 10) : ""));
                    EventFile.WriteLine("info,number,1");
                    if (BoxDoc.DocumentElement.SelectSingleNode("//boxscore/pitching/pitcher[@win='true']") != null)
                    {
                        EventFile.WriteLine("info,wp," + BoxDoc.DocumentElement.SelectSingleNode("//boxscore/pitching/pitcher[@win='true']").Attributes["id"].InnerText);
                    }
                    if (BoxDoc.DocumentElement.SelectSingleNode("//boxscore/pitching/pitcher[@loss='true']") != null)
                    {
                        EventFile.WriteLine("info,lp," + BoxDoc.DocumentElement.SelectSingleNode("//boxscore/pitching/pitcher[@loss='true']").Attributes["id"].InnerText);
                    }
                    if (BoxDoc.DocumentElement.SelectSingleNode("//boxscore/pitching/pitcher[@save='true']") != null)
                    {
                        EventFile.WriteLine("info,save," + BoxDoc.DocumentElement.SelectSingleNode("//boxscore/pitching/pitcher[@save='true']").Attributes["id"].InnerText.ToUpper());
                    }
                    else
                    {
                        EventFile.WriteLine("info,save,");
                    }
                    LogFile.WriteLine("Processing Rosters...");
                    XmlNode nodo = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting[@team_flag='away']");
                    BatLineUp(nodo, "0");
                    nodo = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/pitching[@team_flag='away']/pitcher");
                    PitLineUp(nodo, "0");
                    nodo = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting[@team_flag='home']");
                    BatLineUp(nodo, "1");
                    nodo = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/pitching[@team_flag='home']/pitcher");
                    PitLineUp(nodo, "1");
                    PlayersDoc = new XmlDocument();
                    PlayersDoc.Load(Ruta + GameID + "/players.xml");
                    PrepareRosterFile(GameID.Substring(4, 4), GameID);
                    EventsDoc = new XmlDocument();
                    //EventFile.WriteLine(Ruta + GameID + "/boxscore.xml");
                    //EventsDoc.Load(@"c:\users\josereyes\Downloads\BBData\Events\game_events.xml");
                    EventsDoc.Load(Ruta + GameID + "/game_events.xml");
                    HitDoc = new XmlDocument();
                    HitDoc.Load(Ruta + GameID + "/inning/inning_hit.xml");
                    XmlNodeList Eventos = EventsDoc.DocumentElement.SelectNodes("//game/inning/top/atbat | //game/inning/bottom/atbat | //game/inning/top/action | //game/inning/bottom/action ");
                    //EventFile.WriteLine(nodo.Attributes["event"].InnerText + nodo.Attributes["des"].InnerText);
                    //LastEventRecord = "";
                    LogFile.WriteLine("Processing EventNodes...");
                    foreach (XmlNode EventNode in Eventos)
                    {

                        ProcessEvent(EventNode);
                        //ProcessBEVENT(@"c:\Users\josereyes\Downloads\BBData\Events\BEVENT.exe", @"2011ana.eva -i ANA201104081", @"c:\Users\josereyes\Downloads\BBData\Events\");
                        //listBox1.Items.Add(EventNode.Attributes["des"].InnerText);
                    }
                    XmlNodeList Earned = BoxDoc.DocumentElement.SelectNodes("//boxscore/pitching/pitcher");
                    foreach (XmlNode PitcherNode in Earned)
                    {
                        EventFile.WriteLine("data,er," + PitcherNode.Attributes["id"].Value + "," + PitcherNode.Attributes["er"].Value);
                    }
                    //ProcessEvent(nodo.Attributes["event"].InnerText, nodo.Attributes["des"].InnerText);

                }
                EventFile.Close();
                EventFile.Dispose();
                PrepareRosterFile(GameID.Substring(4, 4), GameID);
                LogFile.WriteLine("Processing FillEventsTable...");
                FillEventsTable(outputDir, GameID, false);
                LogFile.Close();
                LogFile.Dispose();
                return GameAvailable;
            }
            catch
            {
                LogFile.Close();
                LogFile.Dispose();
                EventFile.Close();
                EventFile.Dispose();

                throw;

            }
            //MessageBox.Show("Llegamos");


        }

        private string GetFieldSequence(string input)
        {
            string result = "";
            string pattern = "(to )?(?<FielderPos>\\b[a-z][\\w-]*(\\s+\\b[a-z][\\w-]*)?)\\s+(?<FielderName>\\d{6})";
            MatchCollection Matches = Regex.Matches(input, pattern);


            foreach (Match match in Matches)
            {
                //                EventFile.WriteLine(match.Groups["FielderPos"].Value);
                result = result + PosToNumber(match.Groups["FielderPos"].Value);

            }
            //MessageBox.Show(result + "-");
            return result;

        }

        private void PrepareRosterFile(String Year, String rGameID)
        {
            XmlDocument RosterDoc = new XmlDocument();
            string GameYear = (Convert.ToInt32(GameID.Substring(9, 2)) > 9) ? GameID.Substring(4, 4) : (Convert.ToInt32(GameID.Substring(4, 4)) - 1).ToString();
            for (int i = 0; i < 2; i++)
            {
                RosterDoc.Load(outputDir + GameYear + rGameID.Substring(i * 7 + 15, 3) + "ROS.xml");
                XmlNodeList Nodes = RosterDoc.DocumentElement.SelectNodes("//team/player");
                string TeamID;
                TeamID = rGameID.Substring(i * 7 + 15, 3);
                StreamWriter RosterFile = new StreamWriter(outputDir + TeamID + GameYear + ".ros");
                foreach (XmlNode PlayerNode in Nodes)
                {
                    string Line = "";
                    Line = PlayerNode.Attributes["id"].Value;
                    Line = Line + "," + PlayerNode.Attributes["last_name"].Value;
                    Line = Line + "," + PlayerNode.Attributes["first_name"].Value;
                    Line = Line + "," + PlayerNode.Attributes["bats"].Value;
                    Line = Line + "," + PlayerNode.Attributes["throws"].Value + "," + TeamID.ToUpper();
                    Line = Line + "," + PlayerNode.Attributes["pos"].Value;
                    RosterFile.WriteLine(Line);
                }
                RosterFile.Close();
                RosterFile.Dispose();
            }

        }

        private String GetFielderPos(String PickFielderID, int AwayFlag)
        {
            string Salida = "";
            for (int i = 0; i < 10; i++)
            {
                if (Wrapper.Fielders[i, AwayFlag] == PickFielderID)
                {
                    Salida = Wrapper.LineUp[i, AwayFlag].lnPosition.ToString();
                }
            }
            return Salida;

        }

        private int GetPlayerOrder(String PlayerID, int AwayFlag)
        {
            int Salida = 0;
            for (int i = 0; i < 10; i++)
            {
                if (Wrapper.LineUp[i, AwayFlag].lnPlayerID == PlayerID)
                {
                    Salida = i;
                }
            }
            return Salida;

        }


        private String GetRunningBase(String Player_ID, String Batter)
        {
            String Salida = "";
            if (Player_ID == Batter)
            {
                Salida = "B";
            }
            else
            {
                for (int i = 1; i < 4; i++)
                {
                    if (Player_ID == Wrapper.Runners[i])
                    {
                        Salida = i.ToString();
                    }
                }
            }
            return Salida;
        }

        private String ProcessOthers(XmlNode EventNode, String EventoParser, String SubDes)
        {
            string pattern;
            string AdvanceSeparator = RunnerAdvancement ? ";" : ".";

            String BatterID = (EventNode.Name == "atbat") ? EventNode.Attributes["batter"].InnerText : "";
            MatchCollection Matches;
            pattern = "(?<Runner>\\d{6})\\s+scores";
            Matches = Regex.Matches(SubDes, pattern);
            foreach (Match match in Matches)
            {
                EventoParser = EventoParser + AdvanceSeparator + GetRunningBase(match.Groups["Runner"].Value, BatterID) + "-H";
                RunnerAdvancement = true;
            }
            pattern = "(?<Runner>\\d{6})\\s+to (?<Base>\\d)[\\w-]*";
            Matches = Regex.Matches(SubDes, pattern);
            //MessageBox.Show(pattern + " Num matches:" + Matches.Count.ToString());
            foreach (Match match in Matches)
            {
                EventoParser = EventoParser + AdvanceSeparator + GetRunningBase(match.Groups["Runner"].Value, BatterID) + "-" + match.Groups["Base"].Value;
                RunnerAdvancement = true;
            }
            //Wil Nieves steals (1) 2nd base.
            pattern = "(?<StName>\\d{6}) steals(\\s\\(\\d+\\))? (?<Base>\\w|h)";
            Matches = Regex.Matches(SubDes, pattern);
            //MessageBox.Show(pattern + " Num matches:" + Matches.Count.ToString());
            foreach (Match match in Matches)
            {
                //listBox1.Items.Add(match.Groups["Fielder"].Value);
                AdvanceSeparator = (EventoParser == "K" || EventoParser.Contains("K.")) ? "+" : AdvanceSeparator;
                if (EventoParser.Contains("."))
                {
                    EventoParser = EventoParser.Replace(".", AdvanceSeparator + "SB" + match.Groups["Base"].Value.ToUpper() + ".");
                    RunnerAdvancement = true;
                }
                else
                {
                    EventoParser = EventoParser + AdvanceSeparator + "SB" + match.Groups["Base"].Value.ToUpper();
                }
            }
            //Missed catch error by first baseman 488919
            pattern = "Missed catch error by (?<Fielder>([a-z][\\w-]*(\\s+[a-z][\\w-]*)?) (\\d{6}))(,\\s+assist\\s+(?<FieldAssist>([a-z][\\w-]+)(\\s+[a-z][\\w-]+)* \\d{6}))?";
            Matches = Regex.Matches(SubDes, pattern);
            //MessageBox.Show(pattern + " Num matches:" + Matches.Count.ToString());
            foreach (Match match in Matches)
            {
                //listBox1.Items.Add(match.Groups["Fielder"].Value);
                if (match.Groups["FieldAssist"].Value != "" && EventoParser == "E1/SH/BG")
                {
                    //listBox1.Items.Add(match.Groups["FieldAssist"].Value);
                    EventoParser = GetFieldSequence(match.Groups["FieldAssist"].Value) + "E" + GetFieldSequence(match.Groups["Fielder"].Value) + "/SH/BG";
                }
                else
                {
                    EventoParser = EventoParser + "(E" + GetFieldSequence(match.Groups["Fielder"].Value) + ")";
                }
                //RunnerAdvancement = true;
            }
            //Julio Lugo advances to 3rd, on a missed catch error by shortstop Dee Gordon.
            pattern = "(?<Runner>\\d{6}) advances to (?<Base>\\d)[\\w-]+(,)? on (a )?((?<ErrorType>missed catch|throwing|fielding) error by (?<Fielder>([a-z][\\w-]*(\\s+[a-z][\\w-]*)?) (\\d{6}))|balk)";
            Matches = Regex.Matches(SubDes, pattern);
            //MessageBox.Show(pattern + " Num matches:" + Matches.Count.ToString());
            foreach (Match match in Matches)
            {
                //listBox1.Items.Add(match.Groups["Fielder"].Value);
                string ErrorType = (match.Groups["ErrorType"].Value == "throwing") ? "/TH" : "";
                AdvanceSeparator = (EventNode.Attributes["event"].Value == "Stolen Base 2B" ||
                                    EventNode.Attributes["event"].Value == "Stolen Base 3B" ||
                                    EventNode.Attributes["event"].Value == "Stolen Base Home") ? "." : AdvanceSeparator;
                EventoParser = EventoParser + AdvanceSeparator + GetRunningBase(match.Groups["Runner"].Value, BatterID) + "-" + match.Groups["Base"].Value + ((match.Groups["ErrorType"].Value != "") ? "(E" + GetFieldSequence(match.Groups["Fielder"].Value) + ErrorType + ")" : "");
                RunnerAdvancement = true;
            }
            //Throwing error by catcher Wilkin Castillo.
            //Throwing error by pitcher 471183 on the pickoff attempt.  542642 out at home, center fielder 501317 to second baseman 458210 to catcher 433697.  
            pattern = "(Throwing error by (?<Fielder>([a-z][\\w-]*(\\s+[a-z][\\w-]*)?)) (\\d{6}))(\\s+on the pickoff attempt)?";
            Matches = Regex.Matches(SubDes, pattern);
            //MessageBox.Show(pattern + " Num matches:" + Matches.Count.ToString());
            foreach (Match match in Matches)
            {
                if (EventoParser == "")
                {
                    EventoParser = "PO";

                }
                else
                {
                    //listBox1.Items.Add(match.Groups["Fielder"].Value);
                    if (EventoParser.Substring(0, 1) == "E" && EventoParser.Substring(2, 3) == "/SH")
                    {
                        EventoParser = EventoParser.Insert(2, "/TH");
                    }
                    else
                    {
                        EventoParser = EventoParser + "(E" + PosToNumber(match.Groups["Fielder"].Value) + "/TH)";
                    }
                }
            }
            //Mauro Gomez out at 3rd on the throw, center fielder Leonys Martin to third baseman Carlos Triunfel. 
            //Anderson Hernandez out at home, left fielder Freddy Guzman to third baseman Fernando Tatis
            pattern = "(?<Runner>\\d{6}) out at (?<Base>[\\w])[\\w]+( on the throw)?, (?<FieldSequence>([a-z][\\w-]*(\\s+[a-z][\\w-]*)?) \\d{6}((\\s)*([a-z][\\w-]*(\\s+[a-z][\\w-]*)*) \\d{6})*)";
            Matches = Regex.Matches(SubDes, pattern);
            //MessageBox.Show(pattern + " Num matches:" + Matches.Count.ToString());
            foreach (Match match in Matches)
            {
                //listBox1.Items.Add(match.Groups["Base"].Value);
                EventoParser = EventoParser + AdvanceSeparator + GetRunningBase(match.Groups["Runner"].Value, BatterID) + "X" + match.Groups["Base"].Value.ToUpper() + "(" + GetFieldSequence(match.Groups["FieldSequence"].Value) + ")";
                RunnerAdvancement = true;
            }
            lastAdvance = "";
            strSwitch1 = "";
            strSwitch2 = "";
            return FixAdvanceOrder(EventoParser);
        }

        private String IsAheadBase(Match m)
        {
            string Cadena = m.ToString().Replace('B', '0');
            if (lastAdvance != "")
            {
                if (lastAdvance.CompareTo(Cadena) < 0)
                {
                    strSwitch2 = lastAdvance.Replace('0', 'B');
                    strSwitch1 = Cadena.Replace('0', 'B');
                }
            }
            lastAdvance = Cadena;
            return m.ToString();
        }

        private String FixAdvanceOrder(String EventParsed)
        {
            String pattern = "(B|\\d)-(H|\\d)";
            MatchCollection Matches = Regex.Matches(EventParsed, pattern);
            //listBox1.Items.Add(Regex.Replace(EventParsed, pattern, "\\1"));
            Regex.Replace(EventParsed, pattern, new MatchEvaluator(IsAheadBase));
            //foreach (Match match in Matches)
            //{

            //}
            if (strSwitch1 != "")
            {
                EventParsed = EventParsed.Replace(strSwitch1, "xxxx");
                EventParsed = EventParsed.Replace(strSwitch2, strSwitch1);
                EventParsed = EventParsed.Replace("xxxx", strSwitch2);
            }
            return EventParsed;
        }


        private String ProcessEventDes(String EventoDesc)
        {

            foreach (XmlNode Nodo in BoxDoc.DocumentElement.SelectNodes("//boxscore/batting/batter"))
            {
                EventoDesc = EventoDesc.Replace(DisplayFirstLast(Nodo), Nodo.Attributes["id"].Value);
            }
            //listBox1.Items.Add(EventoDesc);
            return EventoDesc;
        }

        private String DisplayFirstLast(XmlNode BoxPlayerNode)
        {
            string FirstLast;
            try
            {
                FirstLast = BoxPlayerNode.Attributes["name_display_first_last"].Value;
            }
            catch
            {
                FirstLast = BoxPlayerNode.Attributes["id"].Value + " - " + GameID + " No está";
            }
            return FirstLast;
        }

 
        private String GetNameByID(String PlayerID)
        {
            return BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting/batter[@id='" + PlayerID + "']").Attributes["name_display_first_last"].Value;
        }


        private void ProcessEvent(XmlNode EventNode)
        {
            RunnerAdvancement = false;

            int i;
            int FO = 0;
            bool isSub = false;
            bool GoProcessOthers = true;
            string DesEvent = EventNode.Attributes["des"].InnerText;
            DesEvent = DesEvent.Replace("Jose A.   Ramirez", "Jose A. Ramirez");
            DesEvent = DesEvent.Replace("J.  C.   Linares", "J.C. Linares");
            DesEvent = DesEvent.Replace("C.   Linares", "J.C. Linares");
            DesEvent = DesEvent.Replace("Tony Pena", "Tony Pena Jr.");
            DesEvent = DesEvent.Replace("Tony Pena", "Tony Pena Jr.");
            DesEvent = DesEvent.Replace("Tony Pena Jr. Jr.", "Tony Pena Jr.");
            DesEvent = DesEvent.Replace("C.   Cron", "C.J. Cron");
            DesEvent = DesEvent.Replace("C.  J.   Cron", "C.J. Cron");
            DesEvent = DesEvent.Replace("C.  J.   Cron", "C.J. Cron");
            DesEvent = DesEvent.Replace(".   Cron advances", "C.J. Cron advances");
            DesEvent = DesEvent.Replace("C.  JC.J. Cron advances", "C.J. Cron advances");
            string Teams = GameID.Substring(15, 3) + GameID.Substring(22, 3);
            switch (Teams)
            {
                case "gigagu":
                case "agugig":
                    if (DesEvent.Contains("Pitcher Change: Juan Perez") || DesEvent.Contains("Pitching Change: Juan Perez"))
                    {
                        DesEvent = DesEvent.Replace("Juan Perez", "448722");
                    }
                    else
                    {
                        DesEvent = DesEvent.Replace("Juan Perez", "543633");
                    }
                    break;
                case "licagu":
                case "agulic":
                    if (DesEvent.Contains("Juan Carlos Perez"))
                    {
                        DesEvent = DesEvent.Replace("Juan Carlos Perez", "467819");
                    }
                    break;
                default:
                    break;
            }
            DesEvent = ProcessEventDes(DesEvent);
            String[] arrEvents = DesEvent.Split('.');
            //MessageBox.Show(arrEvents.Length.ToString());
            string EventoParser;
            string HeaderParser;
            EventoParser = "";
            string pattern;
            MatchCollection Matches;
            PlayerID = (EventNode.Name == "atbat") ? EventNode.Attributes["batter"].InnerText : PlayerID;
            string Evento = EventNode.Attributes["event"].InnerText;
            if (Evento.Contains("Caught Stealing"))
            {

                Evento = "Caught Stealing";
            }
            if (Evento.Contains("Pickoff Error"))
            {

                Evento = "Pickoff Error";
            }
            string inning = EventNode.ParentNode.ParentNode.Attributes["num"].InnerText;
            string inhalf;
            if (EventNode.ParentNode.Name == "top")
            {
                inhalf = "0";
            }
            else
            {
                inhalf = "1";
            }
            HeaderParser = "play," + inning + "," + inhalf + "," + PlayerID + ",??,";
            XmlNode HitNodo;
            switch (Evento)
            {
                case "Caught Stealing":
                case "Base Running Double Play":
                case "Balk":
                case "Strikeout":
                case "Walk":
                case "Wild Pitch":
                case "Pitching Substitution":
                case "Stolen Base 2B":
                case "Stolen Base Home":
                case "Stolen Base 3B":
                case "Offensive sub":
                case "Defensive Switch":
                case "Defensive Sub":
                case "Hit By Pitch":
                case "Intent Walk":
                case "Defensive Indiff":
                case "Player Injured":
                case "Game Advisory":
                case "Passed Ball":
                case "Runner Out":
                case "Ejection":
                case "Picked off stealing 2B":
                case "Picked off stealing 3B":
                case "Picked off stealing Home":
                case "Picked off stealing home":
                case "Strikeout - DP":
                case "Catcher Interference":
                case "Batter Interference":
                case "Runner Advance":
                case "Pickoff 1B":
                case "Pickoff 2B ":
                case "Pickoff 2B":
                case "Pickoff 3B":
                case "Batter Turn":
                case "Pickoff Error":
                    break;
                case "Single":
                case "Double":
                case "Triple":
                case "Home Run":
                    HitNodo = HitDoc.SelectSingleNode("//hitchart/hip[@inning='" + EventNode.ParentNode.ParentNode.Attributes["num"].Value + "' and @team='" + ((EventNode.ParentNode.Name == "bottom") ? "H" : "A") + "' and @type ='H'  and @pitcher='" + EventNode.Attributes["pitcher"].Value + "' and @batter='" + EventNode.Attributes["batter"].Value + "']");
                    Zona = GetZone(HitNodo.Attributes["x"].Value, HitNodo.Attributes["y"].Value);
                    break;
                case "Error":
                case "Field Error":
                case "N/A":
                    if (DesEvent.Substring(0, 12) == "Dropped foul")
                    {
                        HitNodo = HitDoc.SelectSingleNode("//hitchart/hip[@inning='" + EventNode.ParentNode.ParentNode.Attributes["num"].Value + "' and @team='" + ((EventNode.ParentNode.Name == "bottom") ? "H" : "A") + "' and @type ='E']");
                    }
                    else
                    {
                        HitNodo = HitDoc.SelectSingleNode("//hitchart/hip[@inning='" + EventNode.ParentNode.ParentNode.Attributes["num"].Value + "' and @team='" + ((EventNode.ParentNode.Name == "bottom") ? "H" : "A") + "' and @type ='E'  and @pitcher='" + EventNode.Attributes["pitcher"].Value + "' and @batter='" + EventNode.Attributes["batter"].Value + "']");
                    }
                    Zona = GetZone(HitNodo.Attributes["x"].Value, HitNodo.Attributes["y"].Value);
                    break;
                default:
                    HitNodo = HitDoc.SelectSingleNode("//hitchart/hip[@inning='" + EventNode.ParentNode.ParentNode.Attributes["num"].Value + "' and @team='" + ((EventNode.ParentNode.Name == "bottom") ? "H" : "A") + "' and @type ='O'  and @pitcher='" + EventNode.Attributes["pitcher"].Value + "' and @batter='" + EventNode.Attributes["batter"].Value + "']");
                    Zona = GetZone(HitNodo.Attributes["x"].Value, HitNodo.Attributes["y"].Value);
                    break;
            }

            switch (Evento)
            {
                case "Pitching Substitution":
                    isSub = true;
                    pattern = "Pitch(ing|er) Change: (?<BatterName>\\d{6}) replaces";
                    Matches = Regex.Matches(DesEvent, pattern);
                    foreach (Match match in Matches)
                    {
                        string PitcherID = match.Groups["BatterName"].Value;
                        HeaderParser = HeaderParser + ",NP\n";
                        CheckInRoster(PitcherID, (inhalf == "1") ? BoxDoc.DocumentElement.SelectSingleNode("//boxscore").Attributes["away_team_code"].Value : BoxDoc.DocumentElement.SelectSingleNode("//boxscore").Attributes["home_team_code"].Value, "pitchers");
                        EventoParser = "sub," + PitcherID + ",\"" + GetNameByID(PitcherID) + "\"," + (1 - Convert.ToInt32(inhalf)).ToString() + ",0,1";
                        Wrapper = Substitute(PitcherID, match.Groups["BatterName"].Value,
                            1 - Convert.ToInt32(inhalf), 0, 1, Wrapper);

                    }
                    break;
                case "Defensive Switch":
                    isSub = true;
                    pattern = "(?<BatterName>\\d{6}) remains in the game as the (?<position>([a-z][\\w-]*(\\s+[a-z][\\w-]*)?))";
                    Matches = Regex.Matches(DesEvent, pattern);
                    foreach (Match match in Matches)
                    {
                        int BatOrder = GetPlayerOrder(match.Groups["BatterName"].Value, 1 - Convert.ToInt32(inhalf));
                        HeaderParser = HeaderParser + ",NP\n";
                        String NewPos = PosToNumber(match.Groups["position"].Value);
                        CheckInRoster(match.Groups["BatterName"].Value, (inhalf == "1") ? BoxDoc.DocumentElement.SelectSingleNode("//boxscore").Attributes["away_team_code"].Value : BoxDoc.DocumentElement.SelectSingleNode("//boxscore").Attributes["home_team_code"].Value, "pitchers");
                        EventoParser = "sub," + match.Groups["BatterName"].Value + ",\"" + GetNameByID(match.Groups["BatterName"].Value) + "\"," + (1 - Convert.ToInt32(inhalf)).ToString() + "," + BatOrder.ToString() + "," + NewPos.ToString();
                        Wrapper = Substitute(match.Groups["BatterName"].Value, GetNameByID(match.Groups["BatterName"].Value),
                            1 - Convert.ToInt32(inhalf), BatOrder, Convert.ToInt32(NewPos), Wrapper);
                    }
                    break;
                case "Defensive Sub":
                    isSub = true;
                    //Defensive Substitution: 520976 replaces second baseman 435038, batting 2nd, playing right field.  
                    //Defensive Substitution:  429708 replaces 501659, batting 8th, playing third base.  
                    pattern = "Defensive Substitution:\\s+(?<BatterName>\\d{6})\\s+replaces (([a-z][\\w-]*(\\s+[a-z][\\w-]*)?)\\s+)?(?<SubName>\\d{6}), batting (?<BatOrder>\\d)\\w\\w, playing (?<position>([a-z][\\w-]*(\\s+[a-z][\\w-]*)?))";
                    Matches = Regex.Matches(DesEvent, pattern);
                    foreach (Match match in Matches)
                    {
                        int BatOrder = Convert.ToInt32(match.Groups["BatOrder"].Value);
                        string SubPlayerID = match.Groups["BatterName"].Value;
                        HeaderParser = HeaderParser + ",NP\n";
                        string NewPos = PosToNumber(match.Groups["position"].Value);
                        CheckInRoster(SubPlayerID, (inhalf == "1") ? BoxDoc.DocumentElement.SelectSingleNode("//boxscore").Attributes["away_team_code"].Value : BoxDoc.DocumentElement.SelectSingleNode("//boxscore").Attributes["home_team_code"].Value, "batters");
                        EventoParser = "sub," + SubPlayerID + ",\"" + GetNameByID(SubPlayerID) + "\"," + (1 - Convert.ToInt32(inhalf)).ToString() + "," + match.Groups["BatOrder"].Value + "," + NewPos;
                        Wrapper = Substitute(SubPlayerID, GetNameByID(SubPlayerID),
                            1 - Convert.ToInt32(inhalf), BatOrder, Convert.ToInt32(NewPos), Wrapper);
                    }
                    break;
                case "Offensive sub":
                    isSub = true;
                    pattern = "Offensive Substitution: Pinch(-| )(?<OfSubType>runner|hitter) (?<BatterName>\\d{6})\\s+replaces (?<SubName>\\d{6})";
                    Matches = Regex.Matches(DesEvent, pattern);
                    foreach (Match match in Matches)
                    {
                        string SubPlayerID = match.Groups["BatterName"].Value;
                        HeaderParser = HeaderParser + ",NP\n";
                        int NewPos = (match.Groups["OfSubType"].Value == "runner") ? 12 : 11;
                        CheckInRoster(SubPlayerID, (inhalf == "0") ? BoxDoc.DocumentElement.SelectSingleNode("//boxscore").Attributes["away_team_code"].Value : BoxDoc.DocumentElement.SelectSingleNode("//boxscore").Attributes["home_team_code"].Value, "batters");
                        EventoParser = "sub," + SubPlayerID + ",\"" + GetNameByID(SubPlayerID) + "\"," + inhalf + "," + GetPlayerOrder(match.Groups["SubName"].Value, Convert.ToInt32(inhalf)).ToString() + "," + NewPos.ToString();
                        Wrapper = Substitute(SubPlayerID, GetNameByID(SubPlayerID),
                            Convert.ToInt32(inhalf), GetPlayerOrder(match.Groups["SubName"].Value, Convert.ToInt32(inhalf)), NewPos, Wrapper);
                    }
                    break;
                case "Batter Turn":
                    isSub = false;
                    pattern = "(?<BatterName>\\d{6}) turns around to bat (?<BatHand>right|left)-handed.";
                    Matches = Regex.Matches(DesEvent, pattern);
                    HeaderParser = "";
                    foreach (Match match in Matches)
                    {
                        string SubPlayerID = match.Groups["BatterName"].Value;
                        EventoParser = "badj," + SubPlayerID + "," + match.Groups["BatHand"].Value.Substring(0,1).ToUpper() ;
                        GoProcessOthers = false;
                    }
                    break;
                case "Grounded Into DP":
                case "Sac Fly DP":
                    //Hector Luna grounds into a double play, second baseman Jonathan Galvez to shortstop Junior Lake to first baseman Todd Linden. Chris Coghlan out at 2nd.
                    //517370 grounds into double play, shortstop 435078 to second baseman 521310 to first baseman 519068.    501317 to 3rd.    471107 out at 2nd.
                    //Raul Reyes flies into sacrifice double play, center fielder Alfredo Silverio to second baseman Danny Richar. Andy Parrino scores. Erick Almonte out at 2nd.
                    pattern = "(?<BatterName>\\d{6}) (?<OutType>grounds|flies) into (?<SacFlag>a |sacrifice )?double play, (?<FieldSequence>([a-z][\\w-]*(\\s+[a-z][\\w-]*)?) \\d{6}((\\s)*([a-z][\\w-]*(\\s+[a-z][\\w-]*)*) \\d{6})*)(?<OtherEvent>(.\\s+\\d{6}\\s(to 3rd|scores))*).\\s+(?<RunnerName>\\d{6})\\s+out";
                    //listBox1.Items.Add("-->" + Evento);
                    Matches = Regex.Matches(DesEvent, pattern);
                    foreach (Match match in Matches)
                    {
                        //listBox1.Items.Add(Evento);
                        pattern = "(?<OtherEvent>.\\s+\\d{6}\\s(to 3rd|scores))";
                        MatchCollection Matches2;
                        Matches2 = Regex.Matches(match.Groups["OtherEvent"].Value, pattern);
                        foreach (Match match2 in Matches2)
                        {
                            FO++;
                            EventoParser = ProcessOthers(EventNode, EventoParser, match2.Groups["OtherEvent"].Value);
                        }
                        string SacFlag = (match.Groups["SacFlag"].Value == "sacrifice ") ? "/SF" : "";
                        HeaderParser = HeaderParser + ",";
                        string FieldSeq = GetFieldSequence(match.Groups["FieldSequence"].Value);
                        //listBox1.Items.Add(match.Groups["FieldSequence"].Value);
                        string RunnBase = GetRunningBase(match.Groups["RunnerName"].Value, "");
                        FieldSeq = FieldSeq.Insert(FieldSeq.Length - 1, "(" + RunnBase + ")");
                        string OutType = match.Groups["OutType"].Value.Substring(0, 1).ToUpper();
                        EventoParser = FieldSeq + SacFlag + "/" + OutType + "DP" + "/" + OutType + Zona.Substring(1,Zona.Length-1) + EventoParser;
                        //listBox1.Items.Add(EventoParser);
                    }
                    break;
                case "Double Play":
                    //"150367 pops into a double play, third baseman 123107 to first baseman 488919.   490314 scores.    520976 doubled off 1st.  Missed catch error by first baseman 488919.  "
                    pattern = "(?<BatterName>\\d{6}) (?<outtype>grounds|pops|flies|(?<IsBunt>bunt )?lines|pops) into (a )?double play(?<IsFoul> in foul territory)?, (?<FieldSequence>([a-z][\\w-]*(\\s+[a-z][\\w-]*)?) \\d{6}((\\s)*([a-z][\\w-]*(\\s+[a-z][\\w-]*)*) \\d{6})*)(?<OtherEvent>.\\s+\\d{6}\\s+[\\w-]+)?.\\s+(?<RunnerNameOE>\\d{6}) (doubled off|to|out at) (?<BaseOE>\\w+)(?<OtherEventDP>.\\s+(?<RunnerName>\\d{6}) (doubled off|to|out at) (?<Base>\\w))?";
                   //listBox1.Items.Add(Evento);
                    Matches = Regex.Matches(DesEvent, pattern);
                    //listBox1.Items.Add(DesEvent);
                    foreach (Match match in Matches)
                    {
                        HeaderParser = HeaderParser + ",";

                        string FieldSeq = GetFieldSequence(match.Groups["FieldSequence"].Value);
                        //listBox1.Items.Add(match.Groups["FieldSequence"].Value);
                        string RunnBase = GetRunningBase(match.Groups["RunnerNameOE"].Value, "");
                        string OutType = (match.Groups["outtype"].Value == "pops") ? "P/" : match.Groups["outtype"].Value.Substring(0, 1).ToUpper();
                        string IsFoul = (match.Groups["RunnerName"].Value == "") ? "" : "" + OutType.Substring(0, 1) + FieldSeq.Substring(0, 1) + "F/";
                        EventoParser = FieldSeq.Substring(0, 1) + "/" + IsFoul + OutType + "DP." + RunnBase + "X" + match.Groups["BaseOE"].Value.ToUpper().Substring(0,1) + "(" + FieldSeq + ")";
                        RunnerAdvancement = true;
                        //listBox1.Items.Add(EventoParser);
                    }

                    break;
                case "Single":
                case "Double":
                case "Triple":
                case "Home Run":
                    pattern = "(?<BatterName>\\d{6})\\s+(?<HitType>singles|doubles|triples|hits a ground-rule double|hits an inside-the-park home run|hits a grand slam|homers)(\\s+\\(\\d+\\))?( on a )?(?<BatIntensity>(soft|sharp)\\s)?(?<BatType>line drive|(bunt\\s)?pop( up)?|fly ball|(bunt )?ground ball)?\\s+(down the (?<GRD>right|left)-field line)?(?<FieldSequence>([a-z][\\w-]*(\\s+[a-z][\\w-]*)+)( \\d{6})?)?";
                    Matches = Regex.Matches(DesEvent, pattern);
                    foreach (Match match in Matches)
                    {
                        //MessageBox.Show(Evento);

                        //EventFile.WriteLine("Here goes one");
                        //XmlNode nodo = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting/batter[@name_display_first_last='Octavio Dotel'*
                        string HitType = (match.Groups["HitType"].Value == "hits a ground-rule double") ? "D" : match.Groups["HitType"].Value.Substring(0, 1).ToUpper();
                        string FieldSeq;
                        if (HitType == "H")
                        {
                            FieldSeq = PosToNumber(match.Groups["FieldSequence"].Value + "er");
                        }
                        else
                        {
                            FieldSeq = GetFieldSequence(match.Groups["FieldSequence"].Value);
                        }
                        if (match.Groups["GRD"].Value == "right") FieldSeq = "9";
                        if (match.Groups["GRD"].Value == "left") FieldSeq = "7";
                        string BatIntensity;
                        switch (match.Groups["BatIntensity"].Value)
                        {
                            case "sharp ":
                                BatIntensity = "+";
                                break;
                            case "soft ":
                                BatIntensity = "-";
                                break;
                            default:
                                BatIntensity = "";
                                break;
                        }
                        string BatType;
                        switch (match.Groups["BatType"].Value)
                        {
                            case "bunt ground ball":
                                BatType = "/BG";
                                break;
                            case "bunt pop":
                                BatType = "/BP";
                                break;
                            case "":
                                BatType = "";
                                break;
                            default:
                                BatType = "/" + match.Groups["BatType"].Value.Substring(0, 1).ToUpper();
                                break;
                        }
                        string CSBase = match.Groups["HitType"].Value.Substring(0, 1).ToUpper();
                        HeaderParser = HeaderParser + ",";
                        FieldSeq = (FieldSeq.Length == 2) ? "/" + FieldSeq : FieldSeq;
                        EventoParser = HitType + FieldSeq + BatType + BatIntensity + Zona.Substring(1, Zona.Length - 1) + ((match.Groups["HitType"].Value == "hits an inside-the-park home run") ? "/IPHR" : "");
                    }
                    if (DesEvent.Contains("hit by batted ball"))
                    {
                        //Alen Hanson singles on a ground ball. Teoscar Hernandez out at 2nd, hit by batted ball, second baseman Andy Fermin unassisted. 
                        //Jason Castro singled to second. On the play, Jose Altuve out at second, hit by batted ball, second baseman unassisted. Runners on first and third with two outs and Chris Carter due up.
                        pattern = "(?<BatterName>\\d{6})\\s+(singles|singled)\\s+(on a ground ball)?.\\s+(?<RunnerName>\\d{6})\\s+out at (?<OutBase>\\w+), hit by batted ball, (?<FieldSequence>([a-z][\\w-]*(\\s+[a-z][\\w-]*)+)( \\d{6})?)?\\s+unassisted";
                        Matches = Regex.Matches(DesEvent, pattern);
                        foreach (Match match in Matches)
                        {
                            EventoParser = "S/BR/G." + GetRunningBase(match.Groups["RunnerName"].Value, PlayerID) + "X" + match.Groups["OutBase"].Value.Substring(0, 1).ToUpper() + "(" + GetFieldSequence(match.Groups["FieldSequence"].Value) + ")";


                        }
                    }

                    break;
                case "Runner Out":
                    //With 516768 batting, 518170 steals (7) 3rd base.  518170 out at home, catcher 433697 to pitcher 491688.  
                    //With 516768 batting, 518170 steals (7) 3rd base.  518170 out at home, catcher 433697 to pitcher 491688.  
                    //999999 out at home, catcher 999999 to pitcher 99999.  
                    if (EventNode.Name != "atbat")
                    {
                        pattern = "With (?<BatterName>\\d{6}) batting, (?<StEvent>(?<StName>\\d{6}) steals(\\s\\(\\d+\\))? (?<Base>2nd|3rd|home))?((?<EvType>passed ball|wild pitch) by \\d{6}, )?((?<RunnerName>\\d{6}) (to (?<Base>\\w)|(?<Base>s)cores))?";
                        Matches = Regex.Matches(DesEvent, pattern);
                        if (Matches.Count > 0)
                        {
                            foreach (Match match in Matches)
                            {
                                string EventType = "";
                                if (match.Groups["EvType"].Value != "")
                                {
                                    EventType = (match.Groups[""].Value == "passed ball") ? "PB" : "WP";
                                }
                                else
                                {
                                    EventType = ProcessOthers(EventNode, EventType, match.Groups["StEvent"].Value);
                                    RunnerAdvancement = false;
                                    EventType = EventType.Replace(".", "");
                                    EventType = EventType.Replace(";", ".");
                                }
                                HeaderParser = HeaderParser + ",";
                                EventoParser = EventType;
                            }
                        }
                        else
                        {
                            HeaderParser = HeaderParser + ",";
                            RunnerAdvancement = true;
                            EventoParser = "OA" + ProcessOthers(EventNode, EventoParser, DesEvent);
                            EventoParser = EventoParser.Replace("OA;", "OA.");
                        }
                    }
                    else
                    {
                        GoProcessOthers = false;
                    }
                    break;
                case "Pickoff Error":
                    pattern = "With (?<BatterName>\\d{6}) batting, (?<ErrorType>throwing|missed catch) error by (?<ErrorName>\\d{6}) on the pickoff attempt,(\\s+(?<OtherEvent>\\d{6} (to 2nd|to 3rd|scores)))?";
                    Matches = Regex.Matches(DesEvent, pattern);
                    foreach (Match match in Matches)
                    {
                        EventoParser = ProcessOthers(EventNode, EventoParser, match.Groups["OtherEvent"].Value);
                        string ErrorPos = GetFielderPos(match.Groups["ErrorName"].Value, 1 - Convert.ToInt32(inhalf));
                        HeaderParser = HeaderParser + ",";
                        EventoParser = "PO" + EventoParser.Substring(1, 1) + "(E" + ErrorPos + ")" + EventoParser;
                    }
                    break;

                case "Base Running Double Play":
                    //"Double play, 542340 caught stealing home, catcher 446868 to third baseman 517370.   542340 out at home.    516952 out at 3rd.  "
                    pattern = "Double play, (?<RunnerName>\\d{6}) caught stealing (?<CSBase>1st base|2nd base|3rd base|home), (?<FieldSequence>([a-z][\\w-]+)(\\s+[a-z][\\w-]+)* \\d{6}( to ([a-z][\\w-]+)(\\s+[a-z][\\w-]+)* \\d{6})*).\\s+(?<RunnerName1>\\d{6}) out at (?<CSBase1>1st|2nd|3rd|home).\\s+(?<RunnerName2>\\d{6}) out at (?<CSBase2>1st|2nd|3rd|home)";
                    Matches = Regex.Matches(DesEvent, pattern);
                    foreach (Match match in Matches)
                    {
                        //MessageBox.Show(Evento);


                        //XmlNode nodo = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting/batter[@name_display_first_last='Octavio Dotel'*
                        //                      string PlayerID = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting/batter[@name_display_first_last='" + match.Groups["BatterName"].Value + "']").Attributes["id"].Value;
                        string FieldSeq = GetFieldSequence(match.Groups["FieldSequence"].Value);

                        //string RunnerName = match.Groups["RunnerName"].Value;
                        string CSBase = match.Groups["CSBase"].Value.Substring(0, 1).ToUpper();
                        string CSBase2 = match.Groups["CSBase2"].Value.Substring(0, 1).ToUpper();
                        string RunBase = (match.Groups["RunnerName2"].Value == "") ? "" : GetRunningBase(match.Groups["RunnerName2"].Value, "");
                        HeaderParser = HeaderParser + ",";
                        EventoParser = "CS" + CSBase + "(2)/DP." + RunBase + "X" + CSBase2 + "(" + FieldSeq + ")";
                    }
                    break;

                case "Caught Stealing":
                    //pattern = "With (?<BatterName>([A-Z][\\w-]*(\\s+[A-Z][\\w-]*)+)) batting, (?<Runner>([A-Z][\\w-]*(\\s+[A-Z][\\w-]*)+)) caught stealing (?<CSBase>1st base|2nd base|3rd base|home), (?<FieldSequence>([a-z][\\w-]*(\\s+[a-z][\\w-]*)+) [A-Z][\\w-]*(\\s+[A-Z][\\w-]*)+)";
                    //With 452671 batting, 468429 caught stealing 3rd base, catcher 460077 to third baseman 465017,  608070 to 2nd on the throw.  
                    pattern = "With (?<BatterName>\\d{6}) batting, (?<RunnerName>\\d{6}) caught stealing (?<CSBase>1st base|2nd base|3rd base|home), (?<FieldSequence>([a-z][\\w-]+)(\\s+[a-z][\\w-]+)* \\d{6}( to ([a-z][\\w-]+)(\\s+[a-z][\\w-]+)* \\d{6})*)(,\\s+(?<AdvName>\\d{6}) (?<AdvBase>to 2nd|to 3rd|scores) on the throw)?";
                    Matches = Regex.Matches(DesEvent, pattern);
                    foreach (Match match in Matches)
                    {
                        //MessageBox.Show(Evento);


                        //XmlNode nodo = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting/batter[@name_display_first_last='Octavio Dotel'*
                        //                      string PlayerID = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting/batter[@name_display_first_last='" + match.Groups["BatterName"].Value + "']").Attributes["id"].Value;
                        string FieldSeq = GetFieldSequence(match.Groups["FieldSequence"].Value);

                        //string RunnerName = match.Groups["RunnerName"].Value;
                        string CSBase = match.Groups["CSBase"].Value.Substring(0, 1).ToUpper();
                        string AdvRun = "";
                        if (match.Groups["AdvBase"].Value != "")
                        {
                            AdvRun = "." + GetRunningBase(match.Groups["AdvName"].Value, "") + "-" + match.Groups["AdvBase"].Value.Substring(3, 1).ToUpper().Replace("R", "H");
                            RunnerAdvancement = true;
                        }
                        HeaderParser = HeaderParser + ",";
                        EventoParser = "CS" + CSBase + "(" + FieldSeq + ")" + AdvRun;
                    }
                    break;
                case "Fielders Choice Out":
                case "Fielders Choice":
                    //"Tim Fedroff reaches on a fielder's choice out, catcher Robinzon Diaz to third baseman Nate Spears. Jonathan Villar out at 3rd. 
                    //Michael Young reaches on a fielder's choice out, third baseman Alberto Callaspo to second baseman Maicer Izturis. Elvis Andrus out at 3rd.
                    //
                    //Wilkin Ramirez reaches on a fielder's choice, fielded by third baseman Mike Cervenak. Alejandro De Aza scores. ;
                    pattern = "(?<BatterName>\\d{6}) reaches on a fielder's choice( out)?, (fielded by )?(?<FieldSequence>([a-z][\\w-]+)(\\s+[a-z][\\w-]+)* \\d{6}(\\s+unassisted)?( to ([a-z][\\w-]+)(\\s+[a-z][\\w-]+)* \\d{6})*)(?<OtherEvent>.\\s+\\d{6}\\s+(to \\d[\\w-]+|scores))?(.\\s+(?<RunnerName>\\d{6}) out at (?<OutBase>[\\w-]*))?";
                    Matches = Regex.Matches(DesEvent, pattern);
                    foreach (Match match in Matches)
                    {
                        //MessageBox.Show(Evento);


                        //XmlNode nodo = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting/batter[@name_display_first_last='Octavio Dotel'*
                        //                      string PlayerID = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting/batter[@name_display_first_last='" + match.Groups["BatterName"].Value + "']").Attributes["id"].Value;
                        string FieldSeq = GetFieldSequence(match.Groups["FieldSequence"].Value);
                        string OutBase = (match.Groups["OutBase"].Value == "") ? "" : match.Groups["OutBase"].Value.Substring(0, 1).ToUpper();
                        RunnerAdvancement = !(OutBase == "");
                        if (match.Groups["OtherEvent"].Value != "")
                        {
                            EventoParser = ProcessOthers(EventNode, EventoParser, match.Groups["OtherEvent"].Value);
                        }
                        string RunBase = (match.Groups["RunnerName"].Value == "") ? "" : GetRunningBase(match.Groups["RunnerName"].Value, PlayerID);
                        HeaderParser = HeaderParser + ",";
                        EventoParser = "FC" + FieldSeq.Substring(0, 1) + "/G" + Zona.Substring(1,Zona.Length-1) + ((OutBase == "") ? "" : ("." + RunBase + "X" + OutBase + "(" + FieldSeq + ")")) + EventoParser;
                    }
                    break;
                case "Hit By Pitch":
                    pattern = "(?<BatterName>\\d{6})\\s+hit by pitch";
                    Matches = Regex.Matches(DesEvent, pattern);
                    foreach (Match match in Matches)
                    {
                        HeaderParser = HeaderParser + ",";
                        EventoParser = "HP";
                    }
                    break;
                case "Sac Fly":
                case "Sac Bunt":
                    //"Tim Fedroff reaches on a fielder's choice out, catcher Robinzon Diaz to third baseman Nate Spears. Jonathan Villar out at 3rd. 
                    //Michael Young reaches on a fielder's choice out, third baseman Alberto Callaspo to second baseman Maicer Izturis. Elvis Andrus out at 3rd.
                    //FC5/G+.2X3(54)
                    //pattern = "With (?<BatterName>([A-Z][\\w-]*(\\s+[A-Z][\\w-]*)+)) batting, (?<Runner>([A-Z][\\w-]*(\\s+[A-Z][\\w-]*)+)) caught stealing (?<CSBase>1st base|2nd base|3rd base|home), (?<FieldSequence>([a-z][\\w-]*(\\s+[a-z][\\w-]*)+) [A-Z][\\w-]*(\\s+[A-Z][\\w-]*)+)";
                    //Eury Perez hits a sacrifice bunt. Cole Figueroa to 2nd. Eury Perez to 1st.
                    pattern = "(?<BatterName>\\d{6}) (?<OutSafe>hits|out on) a sacrifice (?<SacType>fly|bunt)(,)?( (?<FieldSequence>([a-z][\\w-]+)(\\s+[a-z][\\w-]+)* \\d{6}( to ([a-z][\\w-]+)(\\s+[a-z][\\w-]+)* \\d{6})*))?(?<OtherEvent>(.\\s+\\d{6}\\s(to 2nd|to 3rd|scores))*.\\s+(?<OutRunner>\\d{6})\\s+out at [\\w-]*)?";
                    Matches = Regex.Matches(DesEvent, pattern);
                    //listBox1.Items.Add(DesEvent);
                    foreach (Match match in Matches)
                    {
                        //listBox1.Items.Add(DesEvent);
                        string FieldSeq = (match.Groups["OutSafe"].Value == "hits") ? "E1" : GetFieldSequence(match.Groups["FieldSequence"].Value);
                        HeaderParser = HeaderParser + ",";
                        string SacType = (match.Groups["SacType"].Value == "fly") ? "/SF" : "/SH/BG";
                        string ForcedOut = "";
                        if (match.Groups["OutRunner"].Value != "")
                        {
                            ForcedOut = "/F";
                            SacType = "(" + GetRunningBase(match.Groups["OutRunner"].Value, "") + ")/FO" + SacType;
                        }
                        EventoParser = FieldSeq + SacType + ForcedOut + Zona.Substring(1, Zona.Length - 1);

                    }
                    break;
                case "Catcher Interference":
                    //Felix Pie reaches on catcher interference by Mario Mercedes. Juan Diaz to 2nd. Felix Pie to 1st. 
                    pattern = "\\d{6} reaches on catcher interference by \\d{6}(?<OtherEvent>(.\\s+\\d{6}\\s(to 2nd|to 3rd|scores))*).\\s+\\d{6} to 1st";
                    Matches = Regex.Matches(DesEvent, pattern);
                    foreach (Match match in Matches)
                    {
                        pattern = "(?<OtherEvent>.\\s+\\d{6}\\s(to 2nd|to 3rd|scores))";
                        MatchCollection Matches2;
                        Matches2 = Regex.Matches(match.Groups["OtherEvent"].Value, pattern);
                        foreach (Match match2 in Matches2)
                        {
                            FO++;
                            EventoParser = ProcessOthers(EventNode, EventoParser, match2.Groups["OtherEvent"].Value);
                        }
                        HeaderParser = HeaderParser + ",";
                        EventoParser = "C/E2" + EventoParser;
                    }
                    break;

                case "Field Error":
                case "Error":
                case "N/A":
                    //"Tim Fedroff reaches on a fielder's choice out, catcher Robinzon Diaz to third baseman Nate Spears. Jonathan Villar out at 3rd. 
                    //Michael Young reaches on a fielder's choice out, third baseman Alberto Callaspo to second baseman Maicer Izturis. Elvis Andrus out at 3rd.
                    //464426 reaches on fielding error by shortstop 488818.  
                    //Denis Phipps reaches on missed catch error by first baseman Efren Navarro, assist to third baseman Hector Luna. Donell Linares to 3rd. 
                    //500207 reaches on force attempt, fielding error by shortstop 465753    346796 to 3rd.  
                    if (DesEvent.Substring(0, 12) == "Dropped foul")
                    {
                        pattern = "error by (?<FieldSequence>([a-z][\\w-]+)(\\s+[a-z][\\w-]+)* \\d{6})";
                        Matches = Regex.Matches(DesEvent, pattern);
                        foreach (Match match in Matches)
                        {
                            EventoParser = "FLE" + GetFieldSequence(match.Groups["FieldSequence"].Value);
                        }
                    }
                    else
                    {
                        pattern = "(?<BatterName>\\d{6}) reaches on (a )?(force attempt, )?(?<ErrorType>fielding|missed catch|throwing) error by (?<FieldSequence>([a-z][\\w-]+)(\\s+[a-z][\\w-]+)* \\d{6})(,\\s+assist\\s+(?<FieldAssist>([a-z][\\w-]+)(\\s+[a-z][\\w-]+)* \\d{6}))?(\\s+(?<OtherEvent>\\d{6} (to 2nd|to 3rd|scores)))?";
                        Matches = Regex.Matches(DesEvent, pattern);
                        foreach (Match match in Matches)
                        {
                                //MessageBox.Show(Evento);


                            //XmlNode nodo = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting/batter[@name_display_first_last='Octavio Dotel'*
                            //                      string PlayerID = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting/batter[@name_display_first_last='" + match.Groups["BatterName"].Value + "']").Attributes["id"].Value;
                            string FieldSeq = GetFieldSequence(match.Groups["FieldSequence"].Value);
                            if (match.Groups["OtherEvent"].Value != "")
                            {
                                EventoParser = ProcessOthers(EventNode, EventoParser, match.Groups["OtherEvent"].Value);
                            }
                            string FieldAssist = (match.Groups["FieldSequence"].Value == "") ? "" : GetFieldSequence(match.Groups["FieldAssist"].Value);
                            string ErrorType = (match.Groups["ErrorType"].Value == "throwing") ? "TH" : "F";
                            HeaderParser = HeaderParser + ",";
                            EventoParser = FieldAssist + "E" + FieldSeq + "/" + ErrorType + "/G" + Zona.Substring(1, Zona.Length - 1) + EventoParser;
                        }
                    }
                    break;
                case "Flyout":
                case "Lineout":
                case "Groundout":
                case "Pop Out":
                case "Bunt Pop Out":
                case "Bunt Groundout":
                case "Batter Interference":
                    pattern = "(?<BatterName>\\d{6})\\s+(?<outtype>flies out|lines out|bunt pops out|pops out|grounds out|bunt grounds out)( (?<BatIntensity>sharply|softly))?( to|( )?,)( )?(?<FieldSequence>([a-z][\\w-]*(\\s+[a-z][\\w-]*)?) \\d{6}((\\s)*([a-z][\\w-]*(\\s+[a-z][\\w-]*)*) \\d{6})*)";
                    Matches = Regex.Matches(DesEvent, pattern);
                    //listBox1.Items.Add(DesEvent);
                    foreach (Match match in Matches)
                    {

                        //EventFile.WriteLine(match.Groups["FieldSequence"].Value + "<---");
                        //listBox1.Items.Add("Entró");
                        //XmlNode nodo = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting/batter[@name_display_first_last='Octavio Dotel'*
                        //string PlayerID = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting/batter[@name_display_first_last='" + match.Groups["BatterName"].Value + "']").Attributes["id"].Value;
                        string FieldSeq = GetFieldSequence(match.Groups["FieldSequence"].Value);
                        //MessageBox.Show(match.Groups["FieldSequence"].Value);
                        string BatIntensity;
                        switch (match.Groups["outtype"].Value)
                        {
                            case "softly":
                                BatIntensity = "-";
                                break;
                            case "sharply":
                                BatIntensity = "+";
                                break;
                            default:
                                BatIntensity = "";
                                break;
                        }
                        string OutType;
                        if (Evento == "Bunt Groundout")
                        { OutType = "BG"; }
                        else
                        {
                            if (Evento == "Bunt Pop Out")
                            {
                                OutType = "BP" + FieldSeq + "F";
                            }
                            else
                            {
                                OutType = match.Groups["outtype"].Value.Substring(0, 1).ToUpper();
                            }
                        }
                        HeaderParser = HeaderParser + ",";
                        EventoParser = FieldSeq + "/" + OutType + BatIntensity + ((Evento == "Batter Interference")? "/INT" : Zona.Substring(1, Zona.Length - 1));
                    }
                    break;
                case "Balk":
                case "Defensive Indiff":
                    //With 467070 batting, 542340 advances to 2nd on a balk.
                    pattern = "With (?<BatterName>\\d{6}) batting, (?<RunnerName>\\d{6}) (advances to (?<Base>\\w+)|(?<Base>s)cores) on (?<Playtype>a balk|defensive indifference)";
                    Matches = Regex.Matches(DesEvent, pattern);
                    RunnerAdvancement = true;
                    foreach (Match match in Matches)
                    {
                        HeaderParser = HeaderParser + ",";
                        string DestBase = (match.Groups["Base"].Value == "s") ? "H" : match.Groups["Base"].Value.Substring(0, 1);
                        EventoParser = ((match.Groups["RunnerName"].Value == "a balk") ? "BK." : "DI.") + GetRunningBase(match.Groups["RunnerName"].Value, match.Groups["BatterName"].Value) + "-" + DestBase;
                    }
                    break;
                case "Passed Ball":
                case "Wild Pitch":
                    //With Luis Jimenez batting, wild pitch by Raul Valdes, Denis Phipps to 3rd. JD Closser to 2nd.
                    //With 543829 batting, 520976 steals (1) 2nd base, 520976 to 3rd.  Wild pitch by pitcher 450580.  
                    pattern = "With (?<BatterName>\\d{6}) batting, ((?<EvType>passed ball|wild pitch) by \\d{6}|\\d{6} steals \\(\\d+\\) ((?<SBBase>\\d)\\w+ base|(?<SBBase>h)ome)), (?<RunnerName>\\d{6})\\s+(to (?<Base>\\w+)|(?<Base>s)cores)(.\\s+(?<EvType>Passed ball|Wild pitch))?";
                    Matches = Regex.Matches(DesEvent, pattern);
                    RunnerAdvancement = true;
                    foreach (Match match in Matches)
                    {
                        //string OutType = match.Groups["outtype"].Value.Substring(0, 1).ToUpper();
                        HeaderParser = HeaderParser + ",";
                        string DestBase = (match.Groups["Base"].Value == "s") ? "H" : match.Groups["Base"].Value.Substring(0, 1);
                        string EventType = (match.Groups["EvType"].Value == "passed ball") ? "PB" : "WP";
                        if (match.Groups["SBBase"].Value != "")
                        {
                            EventoParser = "SB" + match.Groups["SBBase"].Value.ToUpper() + "." + GetRunningBase(match.Groups["RunnerName"].Value, match.Groups["BatterName"].Value) + "-" + DestBase + "(" + EventType + ")";
                        }
                        else
                        {
                            EventoParser = EventType + "." + GetRunningBase(match.Groups["RunnerName"].Value, match.Groups["BatterName"].Value) + "-" + DestBase;
                        }
                        //listBox1.Items.Add(match.Groups["Base"].Value);
                    }
                    break;
                case "Stolen Base 2B":
                case "Stolen Base Home":
                case "Stolen Base 3B":
                    //With 435038 batting, 543829 steals (1) 2nd base, 543829 scores.  Missed catch error by center fielder 453568.  Throwing error by catcher 576788.
                    pattern = "With (?<BatterName>\\d{6})\\s+batting, (?<StName>\\d{6}) steals(\\s\\(\\d+\\))? ((?<Base>\\d)\\w+ base|(?<Base>h)ome)(, (?<OtherEvent>\\d{6} (to \\d\\w+|scores)))?";
                    Matches = Regex.Matches(DesEvent, pattern);
                    foreach (Match match in Matches)
                    {
                        EventoParser = ProcessOthers(EventNode, EventoParser, match.Groups["OtherEvent"].Value);
                        HeaderParser = HeaderParser + ",";
                        EventoParser = "SB" + match.Groups["Base"].Value + EventoParser;
                        RunnerAdvancement = true;
                    }
                    break;
                case "Picked off stealing 2B":
                case "Picked off stealing 3B":
                case "Picked off stealing Home":
                    pattern = "With (?<BatterName>\\d{6}) batting, (?<RunnerName>\\d{6}) picked off and caught stealing ((?<Base>\\d)\\w\\w base|(?<Base>h)ome),\\s+(?<FieldSequence>([a-z][\\w-]*(\\s+[a-z][\\w-]*)?) \\d{6}((\\s)*([a-z][\\w-]*(\\s+[a-z][\\w-]*)*) \\d{6})*)";
                    Matches = Regex.Matches(DesEvent, pattern);
                    foreach (Match match in Matches)
                    {
                        string FieldSeq = GetFieldSequence(match.Groups["FieldSequence"].Value);
                        HeaderParser = HeaderParser + ",";
                        EventoParser = "POCS" + match.Groups["Base"].Value + "(" + FieldSeq + ")";
                    }
                    break;
                case "Pickoff 2B ":
                case "Pickoff 1B":
                case "Pickoff 3B":
                    pattern = "With (?<BatterName>\\d{6}) batting, (?<POName>\\d{6}) picks off \\d{6} at (?<Base>\\d)\\w\\w on throw to (?<THName>\\d{6})";
                    Matches = Regex.Matches(DesEvent, pattern);
                    foreach (Match match in Matches)
                    {
                        string FieldSeq = GetFieldSequence(match.Groups["FieldSequence"].Value);
                        HeaderParser = HeaderParser + ",";
                        EventoParser = FieldSeq + "PO" + match.Groups["Base"].Value + "(" + GetFielderPos(match.Groups["POName"].Value, 1 - Convert.ToInt32(inhalf)) + GetFielderPos(match.Groups["THName"].Value, 1 - Convert.ToInt32(inhalf)) + ")";
                    }
                    break;
                case "Forceout":
                    //521310 grounds into a force out, third baseman 460621 to second baseman 468981.   543149 out at 2nd.    521310 to 1st.  
                    //Eury Perez grounds into a force out, fielded by second baseman Jordany Valdespin.    Abraham Almonte out at 2nd.  
                    //446868 grounds into a force out, fielded by second baseman 518170.    444859 scores.    516782 to 3rd.    465753   out at 2nd.    446868 to 1st.  
                    //pattern = "(?<BatterName>\\d{6}) (?<BatType>grounds|flies) into a force out, (fielded by )?(?<FieldSequence>[a-z]+(\\s+[a-z]*)? \\d{6}(\\s[a-z]+(\\s+[a-z][\\w-]*)+ \\d{6})*)(?<OtherEvent>(.\\s+\\d{6}\\s(to 3rd|scores))*).\\s+\\d{6} out at (?<OutBase>[\\w-]*)";
                    pattern = "(?<BatterName>\\d{6}) (?<BatType>grounds|bunt pops|lines|pops|ground bunts|flies) into a force out, (fielded by )?(?<FieldSequence>[a-z]+(\\s+[a-z]*)? \\d{6}(\\s+[a-z]+(\\s+[a-z][\\w-]*)+ \\d{6})*)(?<OtherEvent>(.\\s+\\d{6}\\s(to 3rd|scores))*).\\s+(?<OutRunner>\\d{6})\\s+out at [\\w-]*";
                    Matches = Regex.Matches(DesEvent, pattern);
                    //listBox1.Items.Add(DesEvent);
                    foreach (Match match in Matches)
                    {
                        //listBox1.Items.Add("-->" + match.Groups["FieldSequence"].Value);
                        pattern = "(?<OtherEvent>.\\s+\\d{6}\\s(to 3rd|scores))";
                        MatchCollection Matches2;
                        Matches2 = Regex.Matches(match.Groups["OtherEvent"].Value, pattern);
                        foreach (Match match2 in Matches2)
                        {
                            FO++;
                            EventoParser = ProcessOthers(EventNode, EventoParser, match2.Groups["OtherEvent"].Value);
                        }
                        string FieldSeq = GetFieldSequence(match.Groups["FieldSequence"].Value);
                        string BatType;
                        if (match.Groups["BatType"].Value == "ground bunts")
                        {
                            BatType = "BG";
                        }
                        else
                        {
                            if (match.Groups["BatType"].Value == "bunt pops")
                            {
                                BatType = "BP";
                            }
                            else
                            {
                                BatType = match.Groups["BatType"].Value.Substring(0, 1).ToUpper();
                            }
                        }
                        string OutBase = GetRunningBase(match.Groups["OutRunner"].Value, "");
                        //OutBase = (match.Groups["OutBase"].Value == "home") ? "3" : (Convert.ToInt32(match.Groups["OutBase"].Value.Substring(0, 1).ToUpper()) - 1).ToString();
                        //MessageBox.Show(match.Groups["FieldSequence"].Value);
                        HeaderParser = HeaderParser + ",";
                        EventoParser = FieldSeq + "(" + OutBase + ")/FO/" + BatType + Zona.Substring(1,Zona.Length-1) + EventoParser;
                    }
                    break;
                case "Game Advisory":
                    EventoParser = "";
                    break;
                case "Strikeout":
                case "Strikeout - DP":
                    pattern = "(?<BatterName>\\d{6})\\s+(?<KOType>strikes out swinging|strikes out on (a\\s)?foul (bunt|tip)|called out on strikes)((\\s+and \\d{6} caught stealing ((?<Base>\\d)\\w+|(?<Base>h)ome))?, (?<FieldSequence>[a-z]+(\\s+[a-z]*)? \\d{6}(\\s+[a-z]+(\\s+[a-z][\\w-]*)+ \\d{6})*))?";
                    string KType;
                    Matches = Regex.Matches(DesEvent, pattern);
                    foreach (Match match in Matches)
                    {
                        //EventFile.WriteLine("Here goes one");
                        //XmlNode nodo = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting/batter[@name_display_first_last='Octavio Dotel'*
                        //string PlayerID = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting/batter[@name_display_first_last='" + match.Groups["BatterName"].Value + "']").Attributes["id"].Value;
                        switch (match.Groups["KOType"].Value)
                        {
                            case "called out on strikes":
                                KType = "C";
                                break;
                            case "strikes out on foul tip":
                            case "strikes out on a foul tip":
                                KType = "T";
                                break;
                            default:
                                KType = "S";
                                break;
                        }
                        HeaderParser = HeaderParser + KType + ",";

                        EventoParser = "K" + (((match.Groups["Base"].Value == "" && match.Groups["FieldSequence"].Value != "")) ? GetFieldSequence(match.Groups["FieldSequence"].Value) : "") + ((match.Groups["Base"].Value != "") ? "+CS" + match.Groups["Base"].Value.ToUpper() + "(" + GetFieldSequence(match.Groups["FieldSequence"].Value) + ")/DP" : "");
                    }
                    break;
                case "Walk":
                case "Intent Walk":
                    //pattern = "(?<BatterName>\\d{6}) walks";
                    //Matches = Regex.Matches(DesEvent, pattern);
                    //foreach (Match match in Matches)
                    //{
                    //EventFile.WriteLine("Here goes one");
                    //XmlNode nodo = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting/batter[@name_display_first_last='Octavio Dotel'*
                    //string PlayerID = BoxDoc.DocumentElement.SelectSingleNode("//boxscore/batting/batter[@name_display_first_last='" + match.Groups["BatterName"].Value + "']").Attributes["id"].Value;
                    HeaderParser = HeaderParser + ",";
                    EventoParser = (Evento == "Walk") ? "W" : "IW";
                    //}
                    break;
                default:
                    break;
            }
            if (GoProcessOthers)
            {
                for (i = 1; i < arrEvents.Length - 1; i++)
                {
                    if (((Evento != "Forceout" && Evento != "Sac Fly DP" && Evento != "Grounded Into DP") || i > FO) && (Evento != "Catcher Interference" || i > FO + 1) && ((Evento != "Fielders Choice Out" && Evento != "Fielders Choice") || i != 1))
                    {
                        EventoParser = ProcessOthers(EventNode, EventoParser, arrEvents[i]);
                    }
                }
            }
            if (EventNode.Attributes["rbi"] != null || (EventNode.Attributes["rbi"] == null && EventoParser.Contains("-H")))
            {
                int RBIs = (EventNode.Attributes["rbi"] == null) ? 0 : Convert.ToInt32(EventNode.Attributes["rbi"].Value);
                EventoParser = SetUnearned(EventoParser, RBIs);
            }
            if (EventoParser != "")
            {
                EventFile.WriteLine(HeaderParser + EventoParser);
            }
            if (Wrapper.Innings != Convert.ToInt32(inning) || Wrapper.BatTeam != Convert.ToInt32(inhalf))
            {
                for (i = 1; i < 4; i++)
                {
                    Wrapper.Runners[i] = "";
                }
                Wrapper.Outs = 0;
                Wrapper.BatTeam = Convert.ToInt32(inhalf);
                Wrapper.Innings = Convert.ToInt32(inning);
            }
            Wrapper.Runners[0] = PlayerID;
            //EventFile.WriteLine("  Antes de " + EventoParser + "  1ra:" + Wrapper.Runners[1] + " 2da:" + Wrapper.Runners[2] + " 3ra:" + Wrapper.Runners[3]);
            if (!isSub && Evento != "Game Advisory" && EventoParser != "")
            {
                Wrapper.UpdateGame(EventoParser, Wrapper);
            }
            //EventFile.WriteLine(DesEvent);
            //EventFile.WriteLine("Despues de " + EventoParser + "  1ra:" + Wrapper.Runners[1] + " 2da:" + Wrapper.Runners[2]  + " 3ra:" + Wrapper.Runners[3]);
            /*EventoParser = "";
            for (int j = 0; j < 2; j++)
            {
                EventoParser = EventoParser + "Fielders " + j.ToString() + ": ";

                for (i = 0; i < 10; i++)
                {
                    EventoParser = EventoParser + Wrapper.Fielders[i,j];
                }
                EventoParser = EventoParser + "\n";
            }
            EventFile.Write(EventoParser);
            label1.Text = Wrapper.Runners[0];
            label2.Text = Wrapper.Runners[1];
            label3.Text = Wrapper.Runners[2];
            label4.Text = Wrapper.Runners[3];
            label5.Text = Wrapper.Innings.ToString();
            label6.Text = Wrapper.BatTeam.ToString();
            label7.Text = Wrapper.Outs.ToString();*/


        }


        string SetUnearned(string evParser, int RBIs)
        {
            string pattern = "(\\d|B)-H";
            MatchCollection Matches = Regex.Matches(evParser, pattern);
            if (Matches.Count > RBIs)
            {
                for (int i = RBIs; i < Matches.Count; i++)
                {
                    evParser = evParser.Replace(Matches[i].ToString(), Matches[i].ToString() + "(NR)(UR)");
                }
            }
            return evParser;
        }

        private void CheckInRoster(String PlayerID, String TeamID, String PlayerType)
        {
            string GameYear = (Convert.ToInt32(GameID.Substring(9, 2)) > 9) ? GameID.Substring(4, 4) : (Convert.ToInt32(GameID.Substring(4, 4)) - 1).ToString();
            if (!File.Exists(outputDir + GameYear + TeamID + "ROS.xml"))
            {
                //StreamWriter Roster = new StreamWriter(GameID.Substring(4, 4) + TeamID + "ROS.xml");
                XmlWriter writer = XmlWriter.Create(outputDir + GameID.Substring(4, 4) + TeamID + "ROS.xml");
                writer.WriteStartElement("team");
                writer.WriteAttributeString("id", TeamID);
                writer.WriteEndElement();
                writer.Close();
            }
            XmlDocument RosterDoc = new XmlDocument();
            RosterDoc.Load(outputDir + GameYear + TeamID + "ROS.xml");
            XmlNode PlayerNode = RosterDoc.DocumentElement.SelectSingleNode("//team/player[@id='" + PlayerID + "']");
            if (PlayerNode == null)
            {
                try
                {
                    XmlDocument PlayerDoc = new XmlDocument();
                    PlayerDoc.Load(Ruta + "/" + GameID + "/" + PlayerType + "/" + PlayerID + ".xml");
                    XmlNode TeamNode = RosterDoc.DocumentElement.SelectSingleNode("//team");
                    XmlElement Element = RosterDoc.CreateElement("player");
                    XmlAttribute attr = RosterDoc.CreateAttribute("id");
                    attr.Value = PlayerID;
                    Element.SetAttributeNode(attr);
                    attr = RosterDoc.CreateAttribute("first_name");
                    attr.Value = PlayerDoc.DocumentElement.SelectSingleNode("//Player").Attributes["first_name"].Value;
                    Element.SetAttributeNode(attr);
                    attr = RosterDoc.CreateAttribute("last_name");
                    attr.Value = PlayerDoc.DocumentElement.SelectSingleNode("//Player").Attributes["last_name"].Value;
                    Element.SetAttributeNode(attr);
                    attr = RosterDoc.CreateAttribute("bats");
                    attr.Value = (PlayerDoc.DocumentElement.SelectSingleNode("//Player").Attributes["bats"].Value == "S") ? "B" : PlayerDoc.DocumentElement.SelectSingleNode("//Player").Attributes["bats"].Value;
                    Element.SetAttributeNode(attr);
                    attr = RosterDoc.CreateAttribute("throws");
                    attr.Value = PlayerDoc.DocumentElement.SelectSingleNode("//Player").Attributes["throws"].Value;
                    Element.SetAttributeNode(attr);
                    attr = RosterDoc.CreateAttribute("team_id");
                    attr.Value = PlayerDoc.DocumentElement.SelectSingleNode("//Player").Attributes["team"].Value;
                    Element.SetAttributeNode(attr);
                    attr = RosterDoc.CreateAttribute("pos");
                    attr.Value = PlayerDoc.DocumentElement.SelectSingleNode("//Player").Attributes["pos"].Value;
                    Element.SetAttributeNode(attr);
                    attr = RosterDoc.CreateAttribute("game");
                    attr.Value = GameID;
                    Element.SetAttributeNode(attr);
                    PlayerNode = TeamNode.AppendChild(Element);
                }
                catch
                {
                    string Url = "http://www.milb.com/player/index.jsp?player_id=" + PlayerID;
                    HtmlWeb web = new HtmlWeb();
                    HtmlAgilityPack.HtmlDocument MiLB = web.Load(Url);
                    HtmlAgilityPack.HtmlNode Nodo = MiLB.DocumentNode.SelectSingleNode("//*[@id='mc']/script[1]");
                    if (Nodo != null)
                    {
                        string Texto = Nodo.InnerText;
                        //string[20] words;
                        //words[0] = "Nuevo";
                        string FirstName, LastName, Position, Bats, Throws;
                        ;
                        string pattern = "\\\"pi_name_first\\\"\\s+:\\s+\\\"(?<FName>\\w+)\\\"";
                        MatchCollection Matches = Regex.Matches(Texto, pattern);
                        FirstName = Matches[0].Groups["FName"].Value;
                        pattern = "\\\"pi_name_last\\\"\\s+:\\s+\\\"(?<FName>\\w+( \\w+)?( \\w+)?)\\\"";
                        LastName = Regex.Matches(Texto, pattern)[0].Groups["FName"].Value;
                        pattern = "\\\"pt_primary_position\\\"\\s+:\\s+\\\"(?<FName>\\w+)\\\"";
                        Position = Regex.Matches(Texto, pattern)[0].Groups["FName"].Value;
                        pattern = "\\\"pi_bats\\\"\\s+:\\s+\\\"(?<FName>\\w+)\\\"";
                        Bats = Regex.Matches(Texto, pattern)[0].Groups["FName"].Value;
                        pattern = "\\\"pi_throws\\\"\\s+:\\s+\\\"(?<FName>\\w+)\\\"";
                        Throws = Regex.Matches(Texto, pattern)[0].Groups["FName"].Value;
                        XmlNode TeamNode = RosterDoc.DocumentElement.SelectSingleNode("//team");
                        XmlElement Element = RosterDoc.CreateElement("player");
                        XmlAttribute attr = RosterDoc.CreateAttribute("id");
                        attr.Value = PlayerID;
                        Element.SetAttributeNode(attr);
                        attr = RosterDoc.CreateAttribute("first_name");
                        attr.Value = FirstName;
                        Element.SetAttributeNode(attr);
                        attr = RosterDoc.CreateAttribute("last_name");
                        attr.Value = LastName;
                        Element.SetAttributeNode(attr);
                        attr = RosterDoc.CreateAttribute("bats");
                        attr.Value = (Bats == "S") ? "B" : Bats;
                        Element.SetAttributeNode(attr);
                        attr = RosterDoc.CreateAttribute("throws");
                        attr.Value = Throws;
                        Element.SetAttributeNode(attr);
                        attr = RosterDoc.CreateAttribute("team_id");
                        attr.Value = TeamID.ToUpper();
                        Element.SetAttributeNode(attr);
                        attr = RosterDoc.CreateAttribute("pos");
                        attr.Value = Position;
                        Element.SetAttributeNode(attr);
                        attr = RosterDoc.CreateAttribute("game");
                        attr.Value = GameID;
                        Element.SetAttributeNode(attr);
                        PlayerNode = TeamNode.AppendChild(Element);
                    }
                    else
                    {
                        EventFile.WriteLine("Nada");
                    }

                };

                RosterDoc.Save(outputDir + GameYear + TeamID + "ROS.xml");

            }
            else
            {

            }
            //listBox1.Items.Add(GameID.Substring(4,4) + TeamID + ".ros");

        }

        private void PitLineUp(XmlNode nodo, String AwayFlag)
        {
            Wrapper.LineUp[0, Convert.ToInt32(AwayFlag)].lnPosition = 1;
            Wrapper.LineUp[0, Convert.ToInt32(AwayFlag)].lnPlayerID = nodo.Attributes["id"].InnerText;
            Wrapper.LineUp[0, Convert.ToInt32(AwayFlag)].lnName = nodo.Attributes["name_display_first_last"].InnerText;
            Wrapper.Fielders[0, Convert.ToInt32(AwayFlag)] = nodo.Attributes["id"].InnerText;
            string FirstLast;
            try
            {
                FirstLast = nodo.Attributes["name_display_first_last"].InnerText;
            }
            catch
            {
                FirstLast = nodo.Attributes["id"].InnerText;
            }
            EventFile.WriteLine("start," + nodo.Attributes["id"].InnerText + ",\"" + FirstLast + "\"," + AwayFlag + ",0,1");
            CheckInRoster(nodo.Attributes["id"].Value, (AwayFlag == "0") ? GameID.Substring(15, 3).ToUpper() : GameID.Substring(22, 3).ToUpper(), "pitchers");
        }

        private void BatLineUp(XmlNode nodo, String AwayFlag)
        {
            foreach (XmlNode node in nodo)
            {
                if (node.Name == "batter" && node.Attributes["pos"].InnerText != "P")
                {
                    if (node.Attributes["bo"].InnerText.Substring(1, 2) == "00")
                    {
                        int BatOrder = Convert.ToInt32(node.Attributes["bo"].InnerText.Substring(0, 1));
                        Wrapper.Fielders[BatOrder, Convert.ToInt32(AwayFlag)] = node.Attributes["id"].InnerText;
                        Wrapper.LineUp[BatOrder, Convert.ToInt32(AwayFlag)].lnPosition = Convert.ToInt32(PosToNumber(node.Attributes["pos"].InnerText.Split('-')[0]));
                        Wrapper.LineUp[BatOrder, Convert.ToInt32(AwayFlag)].lnPlayerID = node.Attributes["id"].InnerText;
                        string FirstLast;
                        try
                        {
                            FirstLast = node.Attributes["name_display_first_last"].InnerText;
                        }
                        catch
                        {
                            FirstLast = node.Attributes["id"].InnerText;
                        }
                        Wrapper.LineUp[BatOrder, Convert.ToInt32(AwayFlag)].lnName = FirstLast;
                        EventFile.WriteLine("start," + node.Attributes["id"].InnerText + ",\"" + FirstLast + "\"," + AwayFlag + "," + node.Attributes["bo"].InnerText.Substring(0, 1) + "," + PosToNumber(node.Attributes["pos"].InnerText.Split('-')[0]));
                        CheckInRoster(node.Attributes["id"].Value, (AwayFlag == "0") ? GameID.Substring(15, 3).ToUpper() : GameID.Substring(22, 3).ToUpper(), "batters");
                    }
                }
            }
        }

        private void GameIDOut(string GameID)
        {
            EventFile.WriteLine("id," + GameID.Substring(22, 3).ToUpper() + GameID.Substring(4, 4) + GameID.Substring(9, 2) + GameID.Substring(12, 2) + GameID.Substring(29, 1));
            EventFile.WriteLine("version,2");
        }

        private string PosToNumber(string PosCode)
        {
            PosCode = PosCode.Replace("to ", "");
            PosCode = PosCode.Replace("fielder", "field");
            PosCode = PosCode.Replace("baseman", "base"); //MessageBox.Show(PosCode);
            switch (PosCode)
            {
                case "C":
                case "catcher":
                    return "2";
                case "P":
                case "pitcher":
                    return "1";
                case "1B":
                case "first base":
                    return "3";
                case "2B":
                case "second base":
                    return "4";
                case "3B":
                case "third base":
                    return "5";
                case "SS":
                case "shortstop":
                    return "6";
                case "RF":
                case "right field":
                    return "9";
                case "CF":
                case "center field":
                    return "8";
                case "LF":
                case "left field":
                    return "7";
                case "right center field":
                    return "98";
                case "left center field":
                    return "78";
                case "DH":
                case "designated hitter":
                    return "10";
                default:
                    return "_";
            }
        }


        private CWLib Substitute(String PlayerID, String Name, int Team, int Slot, int Pos, CWLib Libreria)
        {
            String removedPlayer = Libreria.LineUp[Slot, Team].lnPlayerID;
            int removedPosition = Libreria.LineUp[Slot, Team].lnPosition;

            Libreria.LineUp[Slot, Team].lnPlayerID = PlayerID;
            Libreria.LineUp[Slot, Team].lnName = Name;
            Libreria.LineUp[Slot, Team].lnPosition = Pos;

            if (Pos <= 9)
            {
                Libreria.Fielders[Pos, Team] = PlayerID;
                if (Pos == 1 && Slot > 0 && Libreria.LineUp[0, Team].lnPlayerID != "")
                {
                    /* Substituting a pitcher into the batting order, eliminating
                     * * the DH.  Clear out slot zero.
                     */
                    Libreria.LineUp[0, Team].lnPlayerID = "";
                    Libreria.LineUp[0, Team].lnName = "";
                    //Libreria.dh_slot[team] = 0;
                }
            }
            else if (Pos == 11)
            {
                //Libreria.removed_for_ph = removedPlayer;
                //Libreria.ph_flag = 1;
                //Libreria.removed_position = removedPosition;
            }
            else if (Pos == 12)
            {
                if (Libreria.Runners[1] == removedPlayer)
                {
                    //Libreria.removed_for_pr[1] = removedPlayer;
                    Libreria.Runners[1] = PlayerID;
                }
                else if (Libreria.Runners[2] == removedPlayer)
                {
                    //Libreria.removed_for_pr[2] = removedPlayer;
                    Libreria.Runners[2] = PlayerID;
                }
                else if (Libreria.Runners[3] == removedPlayer)
                {
                    //Libreria.removed_for_pr[3] = removedPlayer;
                    Libreria.Runners[3] = PlayerID;
                }
            }

            if (Slot > 0 && Libreria.LineUp[0, Team].lnPlayerID != "" && Libreria.LineUp[0, Team].lnPlayerID != PlayerID)
            {
                /* Substituting a pitcher into the batting order, eliminating
                 * * the DH.  Clear out slot zero.
                 * * This circumstance ought to be illegal, but has happened
                 * * on at least one occasion, on 1976/9/5 when Catfish Hunter
                 * * came in as a pinch-hitter for a player other than the DH.
                 */
                Libreria.LineUp[0, Team].lnPlayerID = "";
                Libreria.LineUp[0, Team].lnName = "";
                //Libreria.dh_slot[team] = 0;
            }

            return Libreria;
        }

        public int GetTeamBBData(String GDTID)
        {
            switch (GDTID)
            {
                case "AGU":
                case "agu":
                    return 1;
                case "EST":
                case "est":
                    return 2;
                case "gig":
                case "GIG":
                    return 3;
                case "ESC":
                case "esc":
                    return 4;
                case "LIC":
                case "lic":
                    return 5;
                case "TOR":
                case "tor":
                    return 6;
                default:
                    return 0;
            }

        }

        public int GetPlayerID(string MLB)
        {
            try 
            {	        
                PlayerRepository pr = new PlayerRepository();
                int idMLB = Int32.Parse(MLB);
                Player thePlayer = pr.GetPlayerByIDMLB(idMLB);
                return thePlayer == null ? 0 : thePlayer.ID;
            }
            catch (Exception)
            {
                return 0;
            }
        }


        public void getPlayerList()
        {
            //int SeasonID = 2014;
            //int TeamID = 6;
            //string queryString = @"SELECT DISTINCT PlayerID FROM BeisbolDataEntities.Player_Game_Stats AS PGS WHERE PGS.SeasonID = @SeasonID and PGS.TeamID = TeamID";
            //ObjectQuery<Player_Game_Stat> productQuery = new ObjectQuery<Player_Game_Stat>(queryString, context, MergeOption.NoTracking);
            //BeisbolDataEntities db = new BeisbolDataEntities();
            
            //var PlayerGameStats = db.Player_Game_Stats.Where(m => m.SeasonID == SeasonID && m.TeamID ==  TeamID).ToList();
        }
        public void CreateHitChart(int GameID, string PitcherHand)
        {
            BeisbolDataEntities db = new BeisbolDataEntities();
            XmlDocument ChartSVG = new XmlDocument();
            ChartSVG.Load(outputDir + "Field.xml");
            XmlNode DrawNode = ChartSVG.DocumentElement.ChildNodes[3].ChildNodes[1];
            XmlNode NuevoNodo;
            string TipoColor;
            var HitCharts = db.HitCharts.Where(m => m.PlayerID ==  GameID && m.Description == PitcherHand).ToList();
            foreach (var RowData in HitCharts)
            {
                NuevoNodo = DrawNode.CloneNode(true);
                TipoColor = (RowData.HomeAway == "H") ? "ff" : "88";
                TipoColor = (RowData.Tipo=="H") ? ("0000" + TipoColor ) : (TipoColor + "0000");
                DrawNode.ParentNode.AppendChild(AddHitLocation(NuevoNodo, RowData.X, RowData.Y, TipoColor));
                
            }
            TipoColor = DrawNode.Attributes[8].Value;
            TipoColor = TipoColor.Replace("fill:#ffff00", "fill:none");
            DrawNode.Attributes[8].Value = TipoColor;
            ChartSVG.Save(outputDir + "Fields\\Fields_" + GameID.ToString() + PitcherHand + ".svg");
        }

        public XmlNode AddHitLocation(XmlNode nuevoNodo, Double x, Double y, string tipoColor)
        {
            Double ConVx = ((x - 90.36) * (3.389826) - 1315.97);
            Double ConVy = ((y - 117.47) * 3.389826 - 826.076);
            string Color = nuevoNodo.Attributes[8].Value;
            Color = Color.Replace("fill:#ffff00", "fill:#" + tipoColor );
            nuevoNodo.Attributes[1].Value = "matrix(3.9589982,0,0,3.9589982," + ConVx.ToString() + "," + ConVy.ToString() + ")";
            nuevoNodo.Attributes[8].Value = Color;
            return nuevoNodo;
        }


        public void FillEventsTable(String outputDir, String GameDayID, bool ReProcess)
        {
            if (!File.Exists(outputDir + GameDayID + ".eva") || ReProcess)
            {
                BBDataLib.BBDataLoader myLoader = new BBDataLib.BBDataLoader(outputDir);
                myLoader.ProcessGame(GameDayID);
            }
            BeisbolDataEntities db = new BeisbolDataEntities();
            int Season_ID = Convert.ToInt32(GameDayID.Substring(4, 4));
            Season_ID = (GameDayID.Substring(9, 1)) == "1" ? Season_ID : Season_ID - 1;
            int Team = GetTeamBBData(GameDayID.Substring(22, 3));
            DateTime Fecha = new DateTime(Convert.ToInt32(GameDayID.Substring(4, 4)), Convert.ToInt32(GameDayID.Substring(9, 2)), Convert.ToInt32(GameDayID.Substring(12, 2)));
            int GameNumber = Convert.ToInt32(GameDayID.Substring(29, 1));
            var Cals = db.Calendars.Where(m => m.SeasonID == Season_ID && m.Team2 == Team && m.Fecha.Month == Fecha.Month && m.Fecha.Day == Fecha.Day && m.GameNumber_MLB == GameNumber && m.Official).ToList();
            //Byte Phase_ID = Cals[0].PhaseID;
            db.DeleteEventGame(GameDayID.Substring(22, 3).ToUpper() + GameDayID.Replace("_", "").Substring(3, 8) + GameDayID.Substring(29, 1));
            //db.SaveChanges();
            
            if (Cals.Count > 0)
                {
                    if (Cals[0].Official)
                    {
                        // Johnny Nouel - 5-11-2014
                        // To Avoid error when suspended or non-official game
                        Byte Phase_ID = Cals[0].PhaseID;
                        //db.DeleteEventGame(GameDayID.Substring(22, 3).ToUpper() + GameDayID.Replace("_", "").Substring(3, 8) + GameDayID.Substring(29, 1));
                        //db.SaveChanges();
                        // End

                        int Game_ID = Cals[0].GameID;
                        //var cals = db.Calendars.Where(m => m.SeasonID == SeasonID).ToList();
                        var dictionary = db.Players.ToDictionary(m => m.IDMLB, m => m.ID);
                        CWLib MyLib = new CWLib();
                        MyLib.EventGame(outputDir, GameDayID, Season_ID.ToString(), MyLib);
                        for (int i = 0; i < MyLib.RecordsCount; i++)
                        {
                            //MessageBox.Show(MyLib.Records[i]);
                            Event Evento = new Event();
                            string[] Fields = MyLib.Records[i].Split(',');
                            Evento.SeasonID = Season_ID;
                            Evento.PhaseID = Phase_ID;
                            Evento.GameID = Game_ID;
                            Evento.GAME_ID = Fields[0].Replace("\"", "");
                            Evento.AWAY_TEAM_ID = Fields[1].Replace("\"", "");

                            Evento.Team1 = GetTeamBBData(Evento.AWAY_TEAM_ID);

                            Evento.INN_CT = Convert.ToInt16(Fields[2]);
                            Evento.BAT_HOME_ID = Convert.ToInt16(Fields[3]);
                            Evento.OUTS_CT = Convert.ToInt16(Fields[4]);
                            Evento.BALLS_CT = Convert.ToInt16(Fields[5]);
                            Evento.STRIKES_CT = Convert.ToInt16(Fields[6]);
                            Evento.PITCH_SEQ_TX = Fields[7].Replace("\"", "");
                            Evento.AWAY_SCORE_CT = Convert.ToInt16(Fields[8]);
                            Evento.HOME_SCORE_CT = Convert.ToInt16(Fields[9]);
                            Evento.BAT_ID = Fields[10].Replace("\"", "");

                            Evento.PlayerID = dictionary[Convert.ToInt32(Evento.BAT_ID)];

                            Evento.BAT_HAND_CD = Fields[11].Replace("\"", "");
                            Evento.RESP_BAT_ID = Fields[12].Replace("\"", "");
                            Evento.RESP_BAT_HAND_CD = Fields[13].Replace("\"", "");
                            Evento.PIT_ID = Fields[14].Replace("\"", "");

                            Evento.PitcherID = dictionary[Convert.ToInt32(Evento.PIT_ID)];

                            Evento.PIT_HAND_CD = Fields[15].Replace("\"", "");
                            Evento.RES_PIT_ID = Fields[16].Replace("\"", "");
                            Evento.RES_PIT_HAND_CD = Fields[17].Replace("\"", "");
                            Evento.POS2_FLD_ID = Fields[18].Replace("\"", "");
                            Evento.POS3_FLD_ID = Fields[19].Replace("\"", "");
                            Evento.POS4_FLD_ID = Fields[20].Replace("\"", "");
                            Evento.POS5_FLD_ID = Fields[21].Replace("\"", "");
                            Evento.POS6_FLD_ID = Fields[22].Replace("\"", "");
                            Evento.POS7_FLD_ID = Fields[23].Replace("\"", "");
                            Evento.POS8_FLD_ID = Fields[24].Replace("\"", "");
                            Evento.POS9_FLD_ID = Fields[25].Replace("\"", "");

                            Evento.PlayerPos2ID = dictionary[Convert.ToInt32(Evento.POS2_FLD_ID)];
                            Evento.PlayerPos3ID = dictionary[Convert.ToInt32(Evento.POS3_FLD_ID)];
                            Evento.PlayerPos4ID = dictionary[Convert.ToInt32(Evento.POS4_FLD_ID)];
                            Evento.PlayerPos5ID = dictionary[Convert.ToInt32(Evento.POS5_FLD_ID)];
                            Evento.PlayerPos6ID = dictionary[Convert.ToInt32(Evento.POS6_FLD_ID)];
                            Evento.PlayerPos7ID = dictionary[Convert.ToInt32(Evento.POS7_FLD_ID)];
                            Evento.PlayerPos8ID = dictionary[Convert.ToInt32(Evento.POS8_FLD_ID)];
                            Evento.PlayerPos9ID = dictionary[Convert.ToInt32(Evento.POS9_FLD_ID)];

                            Evento.BASE1_RUN_ID = Fields[26].Replace("\"", "");
                            Evento.BASE2_RUN_ID = Fields[27].Replace("\"", "");
                            Evento.BASE3_RUN_ID = Fields[28].Replace("\"", "");
                            Evento.PlayerRun1ID = 0; 
                            Evento.PlayerRun2ID = 0; 
                            Evento.PlayerRun3ID = 0; 
                            int OutValue;
                            if (Evento.BASE1_RUN_ID != "")
                            {
                                OutValue = 0;
                                if (dictionary.TryGetValue(Convert.ToInt32(Evento.BASE1_RUN_ID), out OutValue)) Evento.PlayerRun1ID = OutValue;
                            }
                            if (Evento.BASE2_RUN_ID != "")
                            {
                                OutValue = 0;
                                if (dictionary.TryGetValue(Convert.ToInt32(Evento.BASE2_RUN_ID), out OutValue)) Evento.PlayerRun2ID = OutValue;
                            }
                            if (Evento.BASE3_RUN_ID != "")
                            {
                                OutValue = 0;
                                if (dictionary.TryGetValue(Convert.ToInt32(Evento.BASE3_RUN_ID), out OutValue)) Evento.PlayerRun3ID = OutValue;
                            }

                            
//                            Evento.PlayerRun1ID = (Evento.BASE1_RUN_ID == "") ? 0 : dictionary[Convert.ToInt32(Evento.BASE1_RUN_ID)];
                            //Evento.PlayerRun2ID = (Evento.BASE2_RUN_ID == "") ? 0 : dictionary[Convert.ToInt32(Evento.BASE2_RUN_ID)];
                            //Evento.PlayerRun3ID = (Evento.BASE3_RUN_ID == "") ? 0 : dictionary[Convert.ToInt32(Evento.BASE3_RUN_ID)];

                            Evento.EVENT_TX = Fields[29].Replace("\"", "");
                            Evento.LEADOFF_FL = Fields[30].Replace("\"", "");
                            Evento.PH_FL = Fields[31].Replace("\"", "");
                            Evento.BAT_FLD_CD = Convert.ToInt16(Fields[32]);
                            Evento.BAT_LINEUP_ID = Convert.ToInt16(Fields[33]);
                            Evento.EVENT_CD = Convert.ToInt16(Fields[34]);
                            Evento.BAT_EVENT_FL = Fields[35].Replace("\"", "");
                            Evento.AB_FL = Fields[36].Replace("\"", "");
                            Evento.H_CD = Convert.ToInt16(Fields[37]);
                            Evento.SH_FL = Fields[38].Replace("\"", "");
                            Evento.SF_FL = Fields[39].Replace("\"", "");
                            Evento.EVENT_OUTS_CT = Convert.ToInt16(Fields[40]);
                            Evento.DP_FL = Fields[41].Replace("\"", "");
                            Evento.TP_FL = Fields[42].Replace("\"", "");
                            Evento.RBI_CT = Convert.ToInt16(Fields[43]);
                            Evento.WP_FL = Fields[44].Replace("\"", "");
                            Evento.PB_FL = Fields[45].Replace("\"", "");
                            Evento.FLD_CD = Convert.ToInt16(Fields[46]);
                            Evento.BATTEDBALL_CD = Fields[47].Replace("\"", "");
                            Evento.BUNT_FL = Fields[48].Replace("\"", "");
                            Evento.FOUL_FL = Fields[49].Replace("\"", "");
                            Evento.BATTEDBALL_LOC_TX = Fields[50].Replace("\"", "");
                            Evento.ERR_CT = Convert.ToInt16(Fields[51]);
                            Evento.ERR1_FLD_CD = Convert.ToInt16(Fields[52]);
                            Evento.ERR1_CD = Fields[53].Replace("\"", "");
                            Evento.ERR2_FLD_CD = Convert.ToInt16(Fields[54]);
                            Evento.ERR2_CD = Fields[55].Replace("\"", "");
                            Evento.ERR3_FLD_CD = Convert.ToInt16(Fields[56]);
                            Evento.ERR3_CD = Fields[57].Replace("\"", "");
                            Evento.BAT_DEST_ID = Convert.ToInt16(Fields[58]);
                            Evento.RUN1_DEST_ID = Convert.ToInt16(Fields[59]);
                            Evento.RUN2_DEST_ID = Convert.ToInt16(Fields[60]);
                            Evento.RUN3_DEST_ID = Convert.ToInt16(Fields[61]);
                            Evento.BAT_PLAY_TX = Fields[62].Replace("\"", "");
                            Evento.RUN1_PLAY_TX = Fields[63].Replace("\"", "");
                            Evento.RUN2_PLAY_TX = Fields[64].Replace("\"", "");
                            Evento.RUN3_PLAY_TX = Fields[65].Replace("\"", "");
                            Evento.RUN1_SB_FL = Fields[66].Replace("\"", "");
                            Evento.RUN2_SB_FL = Fields[67].Replace("\"", "");
                            Evento.RUN3_SB_FL = Fields[68].Replace("\"", "");
                            Evento.RUN1_CS_FL = Fields[69].Replace("\"", "");
                            Evento.RUN2_CS_FL = Fields[70].Replace("\"", "");
                            Evento.RUN3_CS_FL = Fields[71].Replace("\"", "");
                            Evento.RUN1_PK_FL = Fields[72].Replace("\"", "");
                            Evento.RUN2_PK_FL = Fields[73].Replace("\"", "");
                            Evento.RUN3_PK_FL = Fields[74].Replace("\"", "");
                            Evento.RUN1_RESP_PIT_ID = Fields[75].Replace("\"", "");
                            Evento.RUN2_RESP_PIT_ID = Fields[76].Replace("\"", "");
                            Evento.RUN3_RESP_PIT_ID = Fields[77].Replace("\"", "");
                            Evento.GAME_NEW_FL = Fields[78].Replace("\"", "");
                            Evento.GAME_END_FL = Fields[79].Replace("\"", "");
                            Evento.PR_RUN1_FL = Fields[80].Replace("\"", "");
                            Evento.PR_RUN2_FL = Fields[81].Replace("\"", "");
                            Evento.PR_RUN3_FL = Fields[82].Replace("\"", "");
                            Evento.REMOVED_FOR_PR_RUN1_ID = Fields[83].Replace("\"", "");
                            Evento.REMOVED_FOR_PR_RUN2_ID = Fields[84].Replace("\"", "");
                            Evento.REMOVED_FOR_PR_RUN3_ID = Fields[85].Replace("\"", "");
                            Evento.REMOVED_FOR_PH_BAT_ID = Fields[86].Replace("\"", "");
                            Evento.REMOVED_FOR_PH_BAT_FLD_CD = Convert.ToInt16(Fields[87]);
                            Evento.PO1_FLD_CD = Convert.ToInt16(Fields[88]);
                            Evento.PO2_FLD_CD = Convert.ToInt16(Fields[89]);
                            Evento.PO3_FLD_CD = Convert.ToInt16(Fields[90]);
                            Evento.ASS1_FLD_CD = Convert.ToInt16(Fields[91]);
                            Evento.ASS2_FLD_CD = Convert.ToInt16(Fields[92]);
                            Evento.ASS3_FLD_CD = Convert.ToInt16(Fields[93]);
                            Evento.ASS4_FLD_CD = Convert.ToInt16(Fields[94]);
                            Evento.ASS5_FLD_CD = Convert.ToInt16(Fields[95]);
                            Evento.EVENT_ID = Convert.ToInt16(Fields[96]);
                            Evento.HOME_TEAM_ID = Fields[97].Replace("\"", "");
                            Evento.Team2 = GetTeamBBData(Evento.HOME_TEAM_ID);
                            Evento.BAT_TEAM_ID = Fields[98].Replace("\"", "");
                            Evento.FLD_TEAM_ID = Fields[99].Replace("\"", "");

                            if (Evento.BAT_TEAM_ID == Evento.AWAY_TEAM_ID)
                                Evento.HomeAway = "A";
                            else
                                Evento.HomeAway = "H";

                            Evento.BAT_LAST_ID = Convert.ToInt16(Fields[100]);
                            Evento.INN_NEW_FL = Fields[101].Replace("\"", "");
                            Evento.INN_END_FL = Fields[102].Replace("\"", "");
                            Evento.START_BAT_SCORE_CT = Convert.ToInt16(Fields[103]);
                            Evento.START_FLD_SCORE_CT = Convert.ToInt16(Fields[104]);
                            Evento.INN_RUNS_CT = Convert.ToInt16(Fields[105]);
                            Evento.GAME_PA_CT = Convert.ToInt16(Fields[106]);
                            Evento.INN_PA_CT = Convert.ToInt16(Fields[107]);
                            Evento.PA_NEW_FL = Fields[108].Replace("\"", "");
                            Evento.PA_TRUNC_FL = Fields[109].Replace("\"", "");
                            Evento.START_BASES_CD = Convert.ToInt16(Fields[110]);
                            Evento.END_BASES_CD = Convert.ToInt16(Fields[111]);
                            Evento.BAT_START_FL = Fields[112].Replace("\"", "");
                            Evento.RESP_BAT_START_FL = Fields[113].Replace("\"", "");
                            //Evento.BAT_ON_DECK_ID = Fields[114].Replace("\"", "");
                            //Evento.BAT_ON_HOLD_ID = Fields[115].Replace("\"", "");
                            Evento.PIT_START_FL = Fields[116].Replace("\"", "");
                            Evento.RESP_PIT_START_FL = Fields[117].Replace("\"", "");
                            Evento.RUN1_FLD_CD = Convert.ToInt16(Fields[118]);
                            Evento.RUN1_LINEUP_ID = Convert.ToInt16(Fields[119]);
                            Evento.RUN1_ORIGIN_EVENT_ID = Convert.ToInt16(Fields[120]);
                            Evento.RUN2_FLD_CD = Convert.ToInt16(Fields[121]);
                            Evento.RUN2_LINEUP_ID = Convert.ToInt16(Fields[122]);
                            Evento.RUN2_ORIGIN_EVENT_ID = Convert.ToInt16(Fields[123]);
                            Evento.RUN3_FLD_CD = Convert.ToInt16(Fields[124]);
                            Evento.RUN3_LINEUP_ID = Convert.ToInt16(Fields[125]);
                            Evento.RUN3_ORIGIN_EVENT_ID = Convert.ToInt16(Fields[126]);
                            Evento.RUN1_RESP_CATCH_ID = Fields[127].Replace("\"", "");
                            Evento.RUN2_RESP_CATCH_ID = Fields[128].Replace("\"", "");
                            Evento.RUN3_RESP_CATCH_ID = Fields[129].Replace("\"", "");
                            Evento.PA_BALL_CT = Convert.ToInt16(Fields[130]);
                            Evento.PA_CALLED_BALL_CT = Convert.ToInt16(Fields[131]);
                            Evento.PA_INTENT_BALL_CT = Convert.ToInt16(Fields[132]);
                            Evento.PA_PITCHOUT_BALL_CT = Convert.ToInt16(Fields[133]);
                            Evento.PA_HIT_BALL_CT = Convert.ToInt16(Fields[134]);
                            Evento.PA_OTHER_BALL_CT = Convert.ToInt16(Fields[135]);
                            Evento.PA_STRIKE_CT = Convert.ToInt16(Fields[136]);
                            Evento.PA_CALLED_STRIKE_CT = Convert.ToInt16(Fields[137]);
                            Evento.PA_SWINGMISS_STRIKE_CT = Convert.ToInt16(Fields[138]);
                            Evento.PA_FOUL_STRIKE_CT = Convert.ToInt16(Fields[139]);
                            Evento.PA_BIP_STRIKE_CT = Convert.ToInt16(Fields[140]);
                            Evento.PA_OTHER_STRIKE_CT = Convert.ToInt16(Fields[141]);
                            Evento.EVENT_RUNS_CT = Convert.ToInt16(Fields[142]);
                            Evento.FLD_ID = Fields[143].Replace("\"", "");
                            Evento.BASE2_FORCE_FL = Fields[144].Replace("\"", "");
                            Evento.BASE3_FORCE_FL = Fields[145].Replace("\"", "");
                            Evento.BASE4_FORCE_FL = Fields[146].Replace("\"", "");
                            Evento.BAT_SAFE_ERR_FL = Fields[147].Replace("\"", "");
                            Evento.BAT_FATE_ID = Convert.ToInt16(Fields[148]);
                            Evento.RUN1_FATE_ID = Convert.ToInt16(Fields[149]);
                            Evento.RUN2_FATE_ID = Convert.ToInt16(Fields[150]);
                            Evento.RUN3_FATE_ID = Convert.ToInt16(Fields[151]);
                            Evento.FATE_RUNS_CT = Convert.ToInt16(Fields[152]);
                            Evento.ASS6_FLD_CD = Convert.ToInt16(Fields[153]);
                            Evento.ASS7_FLD_CD = Convert.ToInt16(Fields[154]);
                            Evento.ASS8_FLD_CD = Convert.ToInt16(Fields[155]);
                            Evento.ASS9_FLD_CD = Convert.ToInt16(Fields[156]);
                            Evento.ASS10_FLD_CD = Convert.ToInt16(Fields[157]);
                            /*Evento.UNKNOWN_OUT_EXC_FL = Fields[158].Replace("\"", "");
                            Evento.UNCERTAIN_PLAY_EXC_FL = Fields[159].Replace("\"", "");*/

                            db.Events.AddObject(Evento);
                        }
                        db.SaveChanges();
                    }
                }

        }
        String GetZone(string x, string y)
        {
            String Zona = "";
            int ConVx = Convert.ToInt32(((Convert.ToDouble(x) - 31.12) * (596.49506 + 31.819805) / (205.82 - 31.12) - 31.819805) * 1000);
            int ConVy = Convert.ToInt32(((Convert.ToDouble(y) - 62.15) * (505.86966 - 255.8569) / (131.53 - 62.25) + 255.8569) * 1000);
            ConVx = (ConVx > 710110) ? 709000 : ((ConVx < -68608) ? -67000 : ConVx);
            ConVy = (ConVy > 943822) ? 942000 : ((ConVy < 99919) ? 100000 : ConVy);
            Point HitLocation = new Point(ConVx, ConVy);
            bool ExitLoop;
            int i = 0;
            do
            {
                if (PointInPolygon(HitLocation, PlayField[i].Polygon))
                {
                    Zona = PlayField[i].Name;
                    ExitLoop = true;
                }
                i++;
                ExitLoop = (i == PlayField.Length);
            }
            while (!ExitLoop);

            //MessageBox.Show("x: " + ConVx.ToString() + " y: " + ConVy.ToString());
            return Zona;
        }
        static bool PointInPolygon(Point p, Point[] poly)
        {
            Point p1, p2;
            bool inside = false;
            if (poly.Length < 3)
            {
                return inside;
            }

            Point oldPoint = new Point(
            poly[poly.Length - 1].X, poly[poly.Length - 1].Y);
            for (int i = 0; i < poly.Length; i++)
            {
                Point newPoint = new Point(poly[i].X, poly[i].Y);
                if (newPoint.X > oldPoint.X)
                {
                    p1 = oldPoint;
                    p2 = newPoint;
                }
                else
                {
                    p1 = newPoint;
                    p2 = oldPoint;
                }
                if ((newPoint.X < p.X) == (p.X <= oldPoint.X)
                && ((long)p.Y - (long)p1.Y) * (long)(p2.X - p1.X)
                 < ((long)p2.Y - (long)p1.Y) * (long)(p.X - p1.X))
                {
                    inside = !inside;
                }
                oldPoint = newPoint;
            }
            return inside;
        }

        private void CreatePlayField()
        {
            LogFile.WriteLine("CreatePlayField");
            XmlDocument FieldSVG = new XmlDocument();
            FieldSVG.Load(outputDir + "Hit-Locations-Vec-2.xml");
            XmlNodeList Zonas = FieldSVG.DocumentElement.SelectNodes("//g");
            foreach (XmlNode Nodo in FieldSVG.DocumentElement)
            {
                int i, j, LastX, LastY;
                if (Nodo.Name == "g")
                {
                    i = 0;
                    PlayField = new FieldZone[Nodo.ChildNodes.Count];

                    foreach (XmlNode Objeto in Nodo)
                    {
                        PlayField[i] = new FieldZone();
                        PlayField[i].Name = Objeto.Attributes["id"].Value;
                        //listBox1.Items.Add(Objeto.Attributes["id"].Value);
                        string pattern = "(?<x>(-)?\\d+(.)?\\d+),(?<y>(-)?\\d+(.)?\\d+)";
                        MatchCollection Matches = Regex.Matches(Objeto.Attributes["d"].Value, pattern);
                        PlayField[i].Polygon = new Point[Matches.Count];
                        j = 0;
                        LastX = 0;
                        LastY = 0;
                        foreach (Match match in Matches)
                        {
                            LastX = (Objeto.Attributes["d"].Value.Substring(0, 1) == "m" ? LastX : 0) + Convert.ToInt32(Convert.ToDouble(match.Groups["x"].Value) * 1000);
                            LastY = (Objeto.Attributes["d"].Value.Substring(0, 1) == "m" ? LastY : 0) + Convert.ToInt32(Convert.ToDouble(match.Groups["y"].Value) * 1000);
                            PlayField[i].Polygon[j] = new Point(LastX, LastY);

                            j++;
                            //                            listBox1.Items.Add(Objeto.Attributes["id"].Value + " x:" + match.Groups["x"].Value + " y:" + match.Groups["y"].Value);
                        }
                        i++;
                    }

                }

            }


            //string connstring = @"metadata=res://*/BeisbolData.csdl|res://*/BeisbolData.ssdl|res://*/BeisbolData.msl;provider=System.Data.SqlClient;provider connection string='data source=HOME-PC\SQLEXPRESS;initial catalog=BeisbolData;integrated security=True;multipleactiveresultsets=True;App=EntityFramework'";
            //Event Evento = new Event();
            //Evento
        }




    }
}
