﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Net;
using System.IO;
using Newtonsoft.Json;
using BeisbolData.Data;

namespace BeisbolData_Web_XML_Database_Update
{
    public partial class _automatic : System.Web.UI.Page
    {
        System.Diagnostics.Stopwatch crono = new System.Diagnostics.Stopwatch();
        string MLBurl_Base = "http://gd2.mlb.com/components/game/win/year_YYYY/month_MM/day_DD/";

        int SeasonID, PhaseID;
        private string LigaCode;

        protected void Page_Load(object sender, EventArgs e)
        {
            LigaCode = _Default.GetLigaCode();
            if (!Page.IsPostBack)
            {
                DateTime Fecha = DateTime.Now;    //.AddDays(-1);
                Fecha = Fecha.AddHours(4); //2
                if (Fecha.Hour <= 11)
                    Fecha = Fecha.AddDays(-1);
                txtFecha.Text = Fecha.ToShortDateString();
                SeasonID = Fecha.Year;
                if (Fecha.Month == 1)
                {
                    SeasonID--;
                    if (Fecha.Day >= 18)
                        txtPhaseID.Text = "3";
                    else
                        txtPhaseID.Text = "2";
                }
                else
                {
                    if (Fecha.Month == 12)
                    {
                        if (Fecha.Day >= 26)
                            txtPhaseID.Text = "2";
                        else
                            txtPhaseID.Text = "1";
                    }
                    else
                        txtPhaseID.Text = "1";
                }
                txtSeasonID.Text = SeasonID.ToString();
                PhaseID = Convert.ToInt32(txtPhaseID.Text);
                ExecuteUpdate();
            }
        }

        private void SetupSeasonInfo()
        {
            SeasonInfo.Phase = Convert.ToByte(txtPhaseID.Text);
            SeasonInfo.AñoInicio = Convert.ToInt32(txtSeasonID.Text);
        }

        string GetURL(DateTime Fecha)
        {
            string myURL;
            myURL = MLBurl_Base.Replace("YYYY", Fecha.Year.ToString());
            myURL = myURL.Replace("MM", Fecha.Month.ToString("00"));
            myURL = myURL.Replace("DD", Fecha.Day.ToString("00"));
            return myURL;
        }

        List<_Default.GameTuple> ParseHTML(DateTime fecha)
        {
            List<_Default.GameTuple> Respuesta = new List<_Default.GameTuple>();
            string url = "http://lookup-service-prod.mlb.com/lookup/json/named.schedule_vw_complete.bam?game_date=%27" + fecha.ToString("YYYY/MM/DD") + "%27&season=2018&sort_column=game_time_et%27&league_id=" + LigaCode;
            _Default.ScheduleRO SRO = JsonConvert.DeserializeObject<_Default.ScheduleRO>(_Default.GetJsonResponse(url));
            List<_Default.SchRow> schRowList = new List<_Default.SchRow>();
            if (SRO.schedule_vw_complete.queryResults.totalSize == 1)
            {
                _Default.SchRow schrow = JsonConvert.DeserializeObject<_Default.SchRow>(SRO.schedule_vw_complete.queryResults.row.ToString());
                schRowList.Add(schrow);
            }
            else
            {
                schRowList = JsonConvert.DeserializeObject<List<_Default.SchRow>>(SRO.schedule_vw_complete.queryResults.row.ToString());
            }
            if (SRO.schedule_vw_complete.queryResults.totalSize != 0)
                foreach (_Default.SchRow sRow in schRowList)
                    Respuesta.Add(new _Default.GameTuple { GID = sRow, GPK = sRow.game_pk });
            return Respuesta;
        }

        string OpenURL(string url)
        {
            string Respuesta;
            WebClient myClient = new WebClient();
            Stream myStream = myClient.OpenRead(url);
            StreamReader myReader = new StreamReader(myStream);
            Respuesta = myReader.ReadToEnd();
            myReader.Close();
            myStream.Close();
            return Respuesta;
        }

        protected void AddLog(string data)
        {
            string testFile = Request.PhysicalApplicationPath + @"temp\update.txt";
            StreamWriter myWriter = new StreamWriter(testFile, true);
            DateTime Ahora = DateTime.Now.AddHours(1);
            myWriter.WriteLine(Ahora.ToShortDateString() + " " + Ahora.ToLongTimeString() + ": " + data);
            myWriter.Flush();
            myWriter.Close();
        }

        protected void ExecuteUpdate()
        {
            string Directorio = Request.PhysicalApplicationPath + @"temp";
            if (!Directory.Exists(Directorio))
            {
                lblStatus.Text = "Directorio de Trabajo no disponible";
                return;
            }
            try
            {
                string testFile = Request.PhysicalApplicationPath + @"temp\check.txt";
                StreamWriter myWriter = new StreamWriter(testFile);
                myWriter.WriteLine("This is a test");
                myWriter.Flush();
                myWriter.Close();
            }
            catch (Exception)
            {
                lblStatus2.Text = "Can't write to Temp Directory";
                return;
            }
            lblStatus2.Text = Directorio;
            AddLog("Inicio Proceso");
            AddLog("SeasonID: " + SeasonID + ". PhaseID: " + txtPhaseID.Text);
            //return;
            crono.Start();
            string myURL;
            DateTime FechaInicio = Convert.ToDateTime(txtFecha.Text);
            DateTime Fecha = FechaInicio;
            DateTime FechaFin = FechaInicio;

            SetupSeasonInfo();

            //List<string> JuegosNoProcesar = txtNoGame.Text.Split(Convert.ToChar(",")).ToList();
            List<string> JuegosNoProcesar = new List<string>();

            while (Fecha <= FechaFin)
            {
                Boolean HayJuegos;
                lblStatus.Text = "Now Processing: " + Fecha.ToShortDateString();
                myURL = GetURL(Fecha);
                //MessageBox.Show(myURL);
                //MessageBox.Show(OpenURL(myURL));
                //MessageBox.Show(ParseHTML(OpenURL(myURL)));
                List<_Default.GameTuple> JuegosDia = new List<_Default.GameTuple>();
                try
                {
                    //JuegosDia = ParseHTML(OpenURL(myURL));
                    // 17-1-2018
                    JuegosDia = ParseHTML(Fecha);
                    HayJuegos = true;
                }
                catch (Exception)
                {
                    HayJuegos = false;
                    //throw;
                }
                if (HayJuegos)
                {
                    foreach (string Juego in JuegosNoProcesar)
                    {
                        JuegosDia.Remove(JuegosDia.Where(p => "gid_" + p.GID.game_id.Replace("/", "_").Replace("-", "_") == Juego).FirstOrDefault());
                    }
                    foreach (_Default.GameTuple Juego in JuegosDia)
                    {
                        //GameData gamedata = new GameData(Juego, myURL);
                        lblStatus2.Text = "Game: " + Juego;
                        GameData gamedata = new GameData(Juego, myURL);
                        if (gamedata.Valid)
                        {
                            if (gamedata.ProcessGame())
                            {
                                txtLog1.Text += Juego + " OK" + Environment.NewLine;
                            }
                            else
                            {
                                txtLog1.Text += Juego + " BAD" + Environment.NewLine;
                                txtLog1.Text += "Error Description: " + ErrorInfo.ErrorDescription + Environment.NewLine;
                                txtLog1.Text += "Where: " + ErrorInfo.WhereWereYou + Environment.NewLine;
                            }
                        }
                    }
                }
                Fecha = Fecha.AddDays(1);
            } //END WHILE
            Sumarizacion();
            crono.Stop();
            TimeSpan ts = crono.Elapsed;
            lblStatus2.Text = "Total time: " + ts.TotalSeconds.ToString() + " segundos";
            AddLog(lblStatus2.Text);
            AddLog("----");
            AddLog(Environment.NewLine + Environment.NewLine);
            txtLog1.Text += lblStatus2.Text;
        }

        protected void btOK_Click(object sender, EventArgs e)
        {
            ExecuteUpdate();
        }

        private void Sumarizacion()
        {
            // UPDATE SUMARIZACION GAMES, STANDING AND SEASONSTATS
            int RecordsAffected;
            txtLog1.Text += "Finalizing..." + Environment.NewLine;
            lblStatus.Text = "Finalizing...";
            AddLog(lblStatus.Text);

            lblStatus2.Text = "Updating Game Stats";
            AddLog(lblStatus2.Text);

            RecordsAffected = GameData.UpdateGameStats(SeasonInfo.AñoInicio, SeasonInfo.Phase);
            lblStatus2.Text += ": " + RecordsAffected.ToString() + " records";
            txtLog1.Text += "Updating Game Stats: " + RecordsAffected.ToString() + " records" + Environment.NewLine;
            AddLog("Updating Game Stats: " + RecordsAffected.ToString() + " records");

            lblStatus2.Text = "Updating Season Stats";
            AddLog(lblStatus2.Text);

            RecordsAffected = GameData.UpdateSeasonStats(SeasonInfo.AñoInicio, SeasonInfo.Phase);
            lblStatus2.Text += ": " + RecordsAffected.ToString() + " records";
            txtLog1.Text += "Updating Season Stats (Should Return ZERO): " + RecordsAffected.ToString() + " records" + Environment.NewLine;
            AddLog("Updating Season Stats (Should Return ZERO): " + RecordsAffected.ToString() + " records");

            lblStatus2.Text = "Updating Standing";
            AddLog(lblStatus2.Text);

            RecordsAffected = GameData.UpdateStandingStats(SeasonInfo.AñoInicio, SeasonInfo.Phase);
            lblStatus2.Text += ": " + RecordsAffected.ToString() + " records";
            txtLog1.Text += "Updating Standing: " + RecordsAffected.ToString() + " records" + Environment.NewLine;
            AddLog("Updating Standing: " + RecordsAffected.ToString() + " records");

            lblStatus2.Text = "Updating Streak...";
            AddLog(lblStatus2.Text);

            RecordsAffected = GameData.UpdateStreak(SeasonInfo.AñoInicio, SeasonInfo.Phase);
            lblStatus2.Text += ": " + RecordsAffected.ToString() + " records";
            txtLog1.Text += "Updating Streak: " + RecordsAffected.ToString() + " records" + Environment.NewLine;
            AddLog("Updating Streak: " + RecordsAffected.ToString() + " records");

            lblStatus2.Text = "Updating Last 10 Games Record...";
            AddLog(lblStatus2.Text);

            RecordsAffected = GameData.UpdateLast10GamesRecord(SeasonInfo.AñoInicio, SeasonInfo.Phase);
            lblStatus2.Text += ": " + RecordsAffected.ToString() + " records";
            txtLog1.Text += "Updating Last 10 Games Record: " + RecordsAffected.ToString() + " records" + Environment.NewLine;
            AddLog("Updating Last 10 Games Record: " + RecordsAffected.ToString() + " records");

            lblStatus.Text = "Done!";
            AddLog(lblStatus.Text);
            lblStatus2.Text = "Updates completed!";
            AddLog(lblStatus2.Text);
            txtLog1.Text += "Process Completed." + Environment.NewLine;
            AddLog("Process Completed.");
        }

        protected void btSumarizar_Click(object sender, EventArgs e)
        {
            SetupSeasonInfo();
            Sumarizacion();
        }

        protected void btUpdateTeamID_Click(object sender, EventArgs e)
        {
            BeisbolDataEntities db = new BeisbolDataEntities();
            var datos = db.Pitcher_Game_Stats.Where(m => m.SeasonID == 2011 && m.PhaseID == 1)
                .OrderBy(m => m.GameID).ToList();
            int i = 0;
            int j = 0;
            foreach (var item in datos)
            {
                i++;
                BeisbolData.Data.PlayerRepository.UpdateCurrentTeam(item.PlayerID, item.TeamID);
            }

            var datos2 = db.Player_Game_Stats.Where(m => m.SeasonID == 2011 && m.PhaseID == 1)
                .OrderBy(m => m.GameID).ToList();
            foreach (var item in datos2)
            {
                j++;
                BeisbolData.Data.PlayerRepository.UpdateCurrentTeam(item.PlayerID, item.TeamID);
            }
            txtLog1.Text += "Updated " + i.ToString() + " Pitcher records" + Environment.NewLine;
            txtLog1.Text += "Updated " + j.ToString() + " Player records" + Environment.NewLine;
            lblStatus.Text = "Completed!";
            lblStatus2.Text = "Done!";
        }

    }
}
