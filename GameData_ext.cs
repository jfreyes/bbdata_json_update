﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeisbolData_Web_XML_Database_Update
{
    public partial class GameData
    {

        public class FeedLiveRO
        {
            public int gamePk { get; set; }
            public string link { get; set; }
            public Metadata metaData { get; set; }
            public LDGamedata gameData { get; set; }
            public LiveData liveData { get; set; }
        }

        public class Metadata
        {
            public int wait { get; set; }
            public string timeStamp { get; set; }
            public string[] gameEvents { get; set; }
            public object[] logicalEvents { get; set; }
        }

        public class LDGamedata
        {
            public LDGame game { get; set; }
            public LDDatetime datetime { get; set; }
            public LDStatus status { get; set; }
            public GDTeams teams { get; set; }
            public GDPlayer[] players { get; set; }
            public Weather weather { get; set; }
            public Review review { get; set; }
            public GDFlags flags { get; set; }
            public object[] alerts { get; set; }
            public Probablepitchers probablePitchers { get; set; }
        }

        public class LDGame
        {
            public int pk { get; set; }
            public string type { get; set; }
            public string doubleHeader { get; set; }
            public string id { get; set; }
            public string gamedayType { get; set; }
            public string tiebreaker { get; set; }
            public int gameNumber { get; set; }
            public string calendarEventID { get; set; }
            public string season { get; set; }
            public string seasonDisplay { get; set; }
        }

        public class LDDatetime
        {
            public DateTime dateTime { get; set; }
            public string originalDate { get; set; }
            public string dayNight { get; set; }
            public string time { get; set; }
            public string ampm { get; set; }
        }

        public class LDStatus
        {
            public string abstractGameState { get; set; }
            public string codedGameState { get; set; }
            public string detailedState { get; set; }
            public string statusCode { get; set; }
            public string abstractGameCode { get; set; }
        }

        public class GDTeams
        {
            public GDHomeAway away { get; set; }
            public GDHomeAway home { get; set; }
        }

        public class GDRecord
        {
            public int gamesPlayed { get; set; }
            public string wildCardGamesBack { get; set; }
            public string leagueGamesBack { get; set; }
            public string springLeagueGamesBack { get; set; }
            public string sportGamesBack { get; set; }
            public string divisionGamesBack { get; set; }
            public string conferenceGamesBack { get; set; }
            public GDLeaguerecord leagueRecord { get; set; }
            public GDRecords records { get; set; }
            public bool divisionLeader { get; set; }
            public int wins { get; set; }
            public int losses { get; set; }
        }

        public class GDLeaguerecord
        {
            public int wins { get; set; }
            public int losses { get; set; }
            public string pct { get; set; }
        }

        public class GDRecords
        {
        }

        public class GDHomeAway
        {
            public int id { get; set; }
            public string name { get; set; }
            public string link { get; set; }
            public LDEntity venue { get; set; }
            public string teamCode { get; set; }
            public string fileCode { get; set; }
            public string abbreviation { get; set; }
            public string teamName { get; set; }
            public string locationName { get; set; }
            public string firstYearOfPlay { get; set; }
            public LDEntity league { get; set; }
            public LDEntity sport { get; set; }
            public string shortName { get; set; }
            public GDRecord record { get; set; }
            public string parentOrgName { get; set; }
            public int parentOrgId { get; set; }
            public bool active { get; set; }
        }

        public class GDPlayer
        {
            public int id { get; set; }
            public string fullName { get; set; }
            public string link { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string birthDate { get; set; }
            public int currentAge { get; set; }
            public string birthCity { get; set; }
            public string birthStateProvince { get; set; }
            public string birthCountry { get; set; }
            public string height { get; set; }
            public int weight { get; set; }
            public bool active { get; set; }
            public LDPosition primaryPosition { get; set; }
            public string useName { get; set; }
            public string middleName { get; set; }
            public string boxscoreName { get; set; }
            public LDCodeDesc batSide { get; set; }
            public LDCodeDesc pitchHand { get; set; }
            public string nameFirstLast { get; set; }
            public string nameSlug { get; set; }
            public string firstLastName { get; set; }
            public string lastFirstName { get; set; }
            public string lastInitName { get; set; }
            public string initLastName { get; set; }
            public string fullFMLName { get; set; }
            public string fullLFMName { get; set; }
            public float strikeZoneTop { get; set; }
            public float strikeZoneBottom { get; set; }
        }

        public class Weather
        {
            public string condition { get; set; }
            public string temp { get; set; }
            public string wind { get; set; }
        }

        public class Review
        {
            public bool hasChallenges { get; set; }
            public ReviewHomeAway away { get; set; }
            public ReviewHomeAway home { get; set; }
        }

        public class ReviewHomeAway
        {
            public int used { get; set; }
            public int remaining { get; set; }
        }

        public class GDFlags
        {
            public bool noHitter { get; set; }
            public bool perfectGame { get; set; }
        }

        public class Probablepitchers
        {
        }

        public class LiveData
        {
            public LDPlays plays { get; set; }
            public LDLinescore linescore { get; set; }
            public LDBoxscore boxscore { get; set; }
            public Decisions decisions { get; set; }
            public Leaders leaders { get; set; }
        }

        public class LDPlays
        {
            public LDplay[] allPlays { get; set; }
            public LDplay currentPlay { get; set; }
            public int[] scoringPlays { get; set; }
            public Playsbyinning[] playsByInning { get; set; }
        }


        public class LDplay
        {
            public Result result { get; set; }
            public About about { get; set; }
            public LDCount count { get; set; }
            public LDMatchup matchup { get; set; }
            public int[] pitchIndex { get; set; }
            public object[] actionIndex { get; set; }
            public object[] runnerIndex { get; set; }
            public Runner[] runners { get; set; }
            public Playevent[] playEvents { get; set; }
            public Credit[] credits { get; set; }
            public int atBatIndex { get; set; }
            public DateTime playEndTime { get; set; }
            public int sBCatcherID { get; set; }
            public int sBPitcherID { get; set; }
            public int stolenBases
            {
                get
                {
                    int SBs = 0;
                    foreach (Runner runner in runners)
                    {
                        if (runner.details.@event.ToLower().Contains("stolen base"))
                        {
                            sBCatcherID = playEvents.Where(p => p.defense != null).FirstOrDefault().defense.catcher.id;
                            sBPitcherID = playEvents.Where(p => p.defense != null).FirstOrDefault().defense.pitcher.id;
                            SBs++;
                        }
                    }
                    return SBs;
                }
            }
            public int caughtStealing
            {
                get
                {
                    int SBs = 0;
                    foreach (Runner runner in runners)
                    {
                        if (runner.details.@event.ToLower().Contains("caught steal") && !runner.details.@event.ToLower().Contains("pickoff"))
                        {
                            sBCatcherID = playEvents.Where(p => p.defense != null).FirstOrDefault().defense.catcher.id;
                            sBPitcherID = playEvents.Where(p => p.defense != null).FirstOrDefault().defense.pitcher.id;
                            SBs++;
                        }
                    }
                    return SBs;
                }
            }
            public string battedBallType
            {
                get
                {
                    string ballTypeResult = "";
                    if (result.description.ToLower().Contains(" pop"))
                    {
                        ballTypeResult = "P";
                    }
                    else if (result.description.ToLower().Contains(" flies") || result.description.ToLower().Contains(" fly"))
                    {
                        ballTypeResult = "P";
                    }
                    else
                    if (result.description.ToLower().Contains(" line"))
                    {
                        ballTypeResult = "L";
                    }
                    else
                    if (result.description.ToLower().Contains(" ground") || result.description.ToLower().Contains("bunt"))
                    {
                        ballTypeResult = "G";
                    }
                    return ballTypeResult;
                }
            }

        }

        public class Result
        {
            public string type { get; set; }
            public string @event { get; set; }
            public string description { get; set; }
            public int rbi { get; set; }
            public int awayScore { get; set; }
            public int homeScore { get; set; }
        }

        public class About
        {
            public int atBatIndex { get; set; }
            public string halfInning { get; set; }
            public int inning { get; set; }
            public DateTime startTime { get; set; }
            public DateTime endTime { get; set; }
            public bool isComplete { get; set; }
            public bool isScoringPlay { get; set; }
            public bool hasReview { get; set; }
            public bool hasOut { get; set; }
            public int captivatingIndex { get; set; }
        }

        public class LDCount
        {
            public int balls { get; set; }
            public int strikes { get; set; }
            public int outs { get; set; }
        }

        public class LDMatchup
        {
            public LDPerson batter { get; set; }
            public LDCodeDesc batSide { get; set; }
            public LDPerson pitcher { get; set; }
            public LDCodeDesc pitchHand { get; set; }
            public List<object> batterHotColdZones { get; set; }
            public List<object> pitcherHotColdZones { get; set; }
            public LDSplits splits { get; set; }
        }

        public class LDSplits
        {
            public string batter { get; set; }
            public string pitcher { get; set; }
            public string menOnBase { get; set; }
        }

        public class Playevent
        {
            public PEDetails details { get; set; }
            public PECount count { get; set; }
            public LDPitchdata pitchData { get; set; }
            public int index { get; set; }
            public int pitchNumber { get; set; }
            public DateTime startTime { get; set; }
            public DateTime endTime { get; set; }
            public bool isPitch { get; set; }
            public string type { get; set; }
            public Defense defense { get; set; }
            public Offense offense { get; set; }
        }

        public class LDCodeDesc
        {
            public string code { get; set; }
            public string description { get; set; }
        }

        public class PECount
        {
            public int balls { get; set; }
            public int strikes { get; set; }
        }

        public class LDPitchdata
        {
            public float strikeZoneTop { get; set; }
            public float strikeZoneBottom { get; set; }
            public Coordinates coordinates { get; set; }
            public Breaks breaks { get; set; }
        }

        public class Coordinates
        {
            public float x { get; set; }
            public float y { get; set; }
        }

        public class Breaks
        {
        }

        public class Defense
        {
            public LDFieldPerson pitcher { get; set; }
            public LDFieldPerson catcher { get; set; }
            public LDFieldPerson first { get; set; }
            public LDFieldPerson second { get; set; }
            public LDFieldPerson third { get; set; }
            public LDFieldPerson shortstop { get; set; }
            public LDFieldPerson left { get; set; }
            public LDFieldPerson center { get; set; }
            public LDFieldPerson right { get; set; }
            public LDEntity team { get; set; }
        }

        public class LDFieldPerson
        {
            public int id { get; set; }
            public string link { get; set; }
        }

        public class Credit
        {
            public LDFieldPerson player { get; set; }
            public string credit { get; set; }
        }

        public class Runner
        {
            public Movement movement { get; set; }
            public LDRunnerDetails details { get; set; }
        }

        public class Movement
        {
            public string start { get; set; }
            public string end { get; set; }
        }

        public class LDRunnerDetails
        {
            public string @event { get; set; }
            public LDPerson runner { get; set; }
            public bool isScoringEvent { get; set; }
            public bool rbi { get; set; }
            public bool earned { get; set; }
            public int playIndex { get; set; }
        }

        public class PEDetails
        {
            public LDCodeDesc call { get; set; }
            public string description { get; set; }
            public string code { get; set; }
            public string ballColor { get; set; }
            public bool isInPlay { get; set; }
            public bool isStrike { get; set; }
            public bool isBall { get; set; }
            public bool hasReview { get; set; }
            public string @event { get; set; }
            public int awayScore { get; set; }
            public int homeScore { get; set; }
            public bool isScoringPlay { get; set; }
        }

        public class Offense
        {
            public LDFieldPerson batter { get; set; }
            public LDFieldPerson first { get; set; }
            public LDFieldPerson second { get; set; }
            public LDFieldPerson third { get; set; }
        }

        public class Playsbyinning
        {
            public int startIndex { get; set; }
            public int endIndex { get; set; }
            public int[] top { get; set; }
            public int[] bottom { get; set; }
            public Hits hits { get; set; }
        }

        public class Hits
        {
            public HitsHomeAway[] away { get; set; }
            public HitsHomeAway[] home { get; set; }
        }

        public class LDEntity
        {
            public int id { get; set; }
            public string name { get; set; }
            public string link { get; set; }
        }

        public class HitsHomeAway
        {
            public LDEntity team { get; set; }
            public int inning { get; set; }
            public LDPerson pitcher { get; set; }
            public LDPerson batter { get; set; }
            public Coordinates coordinates { get; set; }
            public string type { get; set; }
            public string description { get; set; }
        }

        public class LDLinescore
        {
            public int currentInning { get; set; }
            public string currentInningOrdinal { get; set; }
            public string inningState { get; set; }
            public string inningHalf { get; set; }
            public bool isTopInning { get; set; }
            public int scheduledInnings { get; set; }
            public LSInnings[] innings { get; set; }
            public LSTeams teams { get; set; }
            public Defense defense { get; set; }
            public LSOffense offense { get; set; }
            public int balls { get; set; }
            public int strikes { get; set; }
            public int outs { get; set; }
        }

        public class LSTeams
        {
            public LSHomeAway home { get; set; }
            public LSHomeAway away { get; set; }
        }

        public class LSHomeAway
        {
            public int runs { get; set; }
            public int hits { get; set; }
            public int errors { get; set; }
        }


        public class LSOffense
        {
            public LDPerson batter { get; set; }
            public LDPerson onDeck { get; set; }
            public LDPerson inHole { get; set; }
            public LDPerson pitcher { get; set; }
            public LDEntity team { get; set; }
        }

        public class LSInnings
        {
            public int num { get; set; }
            public string ordinalNum { get; set; }
            public InningHomeAway home { get; set; }
            public InningHomeAway away { get; set; }
        }

        public class InningHomeAway
        {
            public int runs { get; set; }
        }

        public class LDBoxscore
        {
            public BSTeams teams { get; set; }
            public Official[] officials { get; set; }
            public BSInfo[] info { get; set; }
            public string[] pitchingNotes { get; set; }
        }

        public class BSTeams
        {
            public BSHomeAway away { get; set; }
            public BSHomeAway home { get; set; }
        }

        public class BSTeamInfo
        {
            public string title { get; set; }
            public Fieldlist[] fieldList { get; set; }
        }

        public class Fieldlist
        {
            public string label { get; set; }
            public string value { get; set; }
        }

        public class Note
        {
            public string label { get; set; }
            public string value { get; set; }
        }

        public class BSHomeAway
        {
            public LDEntity team { get; set; }
            public BSTeamstats teamStats { get; set; }
            public BSPlayer[] players { get; set; }
            public int[] batters { get; set; }
            public int[] pitchers { get; set; }
            public int[] bench { get; set; }
            public int[] bullpen { get; set; }
            public int[] battingOrder { get; set; }
            public BSTeamInfo[] info { get; set; }
            public object[] note { get; set; }
        }

        public class BSTeamstats
        {
            public BSTeamBatting batting { get; set; }
            public BSTeamPitching pitching { get; set; }
            public BSTeamFielding fielding { get; set; }
        }

        public class BSTeamBatting
        {
            public int gamesPlayed { get; set; }
            public int flyOuts { get; set; }
            public int groundOuts { get; set; }
            public int runs { get; set; }
            public int doubles { get; set; }
            public int triples { get; set; }
            public int homeRuns { get; set; }
            public int strikeOuts { get; set; }
            public int baseOnBalls { get; set; }
            public int hits { get; set; }
            public int hitByPitch { get; set; }
            public string avg { get; set; }
            public int atBats { get; set; }
            public string obp { get; set; }
            public string slg { get; set; }
            public string ops { get; set; }
            public int caughtStealing { get; set; }
            public int stolenBases { get; set; }
            public string stolenBasePercentage { get; set; }
            public int totalBases { get; set; }
            public int rbi { get; set; }
            public int leftOnBase { get; set; }
            public int sacBunts { get; set; }
            public int sacFlies { get; set; }
        }

        public class BSTeamPitching
        {
            public int runs { get; set; }
            public int homeRuns { get; set; }
            public int strikeOuts { get; set; }
            public int baseOnBalls { get; set; }
            public int hits { get; set; }
            public int atBats { get; set; }
            public string era { get; set; }
            public string inningsPitched { get; set; }
            public int earnedRuns { get; set; }
            public string whip { get; set; }
            public int battersFaced { get; set; }
            public int outs { get; set; }
            public int hitBatsmen { get; set; }
            public int rbi { get; set; }
            public string winPercentage { get; set; }
            public string strikeoutWalkRatio { get; set; }
            public string strikeoutsPer9Inn { get; set; }
            public string walksPer9Inn { get; set; }
            public string hitsPer9Inn { get; set; }
        }

        public class BSTeamFielding
        {
            public int assists { get; set; }
            public int putOuts { get; set; }
            public int errors { get; set; }
            public int chances { get; set; }
            public string fielding { get; set; }
            public int passedBall { get; set; }
        }

        public class BSPlayer
        {
            public LDPerson person { get; set; }
            public string jerseyNumber { get; set; }
            public LDPosition position { get; set; }
            public LDPlayerStats stats { get; set; }
            public LDCodeDesc status { get; set; }
            public int parentTeamId { get; set; }
            public string battingOrder { get; set; }
            public LDSeasonstats seasonStats { get; set; }
            public LDPlayerGamestatus gameStatus { get; set; }
            public LDPosition[] allPositions { get; set; }
        }

        public class LDPerson
        {
            public int id { get; set; }
            public string fullName { get; set; }
            public string link { get; set; }
        }

        public class LDPosition
        {
            public string code { get; set; }
            public string name { get; set; }
            public string type { get; set; }
            public string abbreviation { get; set; }
        }

        public class LDPlayerStats
        {
            public LDPlayerBatting batting { get; set; }
            public LDPlayerPitching pitching { get; set; }
            public LDPlayerFielding fielding { get; set; }
        }

        public class LDPlayerBatting
        {
            public int gamesPlayed { get; set; }
            public int flyOuts { get; set; }
            public int groundOuts { get; set; }
            public int runs { get; set; }
            public int doubles { get; set; }
            public int triples { get; set; }
            public int homeRuns { get; set; }
            public int strikeOuts { get; set; }
            public int baseOnBalls { get; set; }
            public int hits { get; set; }
            public int hitByPitch { get; set; }
            public int atBats { get; set; }
            public int caughtStealing { get; set; }
            public int stolenBases { get; set; }
            public string stolenBasePercentage { get; set; }
            public int totalBases { get; set; }
            public int rbi { get; set; }
            public int leftOnBase { get; set; }
            public int sacBunts { get; set; }
            public int sacFlies { get; set; }
        }

        public class LDPlayerFielding
        {
            public int assists { get; set; }
            public int putOuts { get; set; }
            public int errors { get; set; }
            public int chances { get; set; }
            public string fielding { get; set; }
            public int passedBall { get; set; }
        }

        public class LDPlayerPitching
        {
            public int runs { get; set; }
            public int homeRuns { get; set; }
            public int strikeOuts { get; set; }
            public int baseOnBalls { get; set; }
            public int hits { get; set; }
            public int atBats { get; set; }
            public int numberOfPitches { get; set; }
            public string inningsPitched { get; set; }
            public int wins { get; set; }
            public int losses { get; set; }
            public int saves { get; set; }
            public int holds { get; set; }
            public int blownSaves { get; set; }
            public int earnedRuns { get; set; }
            public int battersFaced { get; set; }
            public int outs { get; set; }
            public int pitchesThrown { get; set; }
            public int balls { get; set; }
            public int strikes { get; set; }
            public int hitBatsmen { get; set; }
            public int rbi { get; set; }
            public string note { get; set; }
            public int? flyOuts { get; set; }
            public int? groundOuts { get; set; }
            public int? wildPitches { get; set; }
        }

        public class LDSeasonstats
        {
            public LDSeasonBatting batting { get; set; }
            public LDSeasonPitching pitching { get; set; }
            public LDSeasonFielding fielding { get; set; }
        }

        public class LDSeasonBatting
        {
            public int gamesPlayed { get; set; }
            public int flyOuts { get; set; }
            public int groundOuts { get; set; }
            public int runs { get; set; }
            public int doubles { get; set; }
            public int triples { get; set; }
            public int homeRuns { get; set; }
            public int strikeOuts { get; set; }
            public int baseOnBalls { get; set; }
            public int hits { get; set; }
            public int hitByPitch { get; set; }
            public string avg { get; set; }
            public int atBats { get; set; }
            public string obp { get; set; }
            public string slg { get; set; }
            public string ops { get; set; }
            public int caughtStealing { get; set; }
            public int stolenBases { get; set; }
            public string stolenBasePercentage { get; set; }
            public int totalBases { get; set; }
            public int rbi { get; set; }
            public int leftOnBase { get; set; }
            public int sacBunts { get; set; }
            public int sacFlies { get; set; }
        }

        public class LDSeasonPitching
        {
            public int runs { get; set; }
            public int homeRuns { get; set; }
            public int strikeOuts { get; set; }
            public int baseOnBalls { get; set; }
            public int hits { get; set; }
            public int atBats { get; set; }
            public string era { get; set; }
            public string inningsPitched { get; set; }
            public int wins { get; set; }
            public int losses { get; set; }
            public int saves { get; set; }
            public int holds { get; set; }
            public int blownSaves { get; set; }
            public int earnedRuns { get; set; }
            public string whip { get; set; }
            public int outs { get; set; }
            public int hitBatsmen { get; set; }
            public int rbi { get; set; }
            public string winPercentage { get; set; }
            public string strikeoutWalkRatio { get; set; }
            public string strikeoutsPer9Inn { get; set; }
            public string walksPer9Inn { get; set; }
            public string hitsPer9Inn { get; set; }
        }

        public class LDSeasonFielding
        {
            public int assists { get; set; }
            public int putOuts { get; set; }
            public int errors { get; set; }
            public int chances { get; set; }
            public string fielding { get; set; }
            public int passedBall { get; set; }
        }

        public class LDPlayerGamestatus
        {
            public bool isCurrentBatter { get; set; }
            public bool isCurrentPitcher { get; set; }
            public bool isOnBench { get; set; }
            public bool isSubstitute { get; set; }
        }

        public class Official
        {
            public LDPerson official { get; set; }
            public string officialType { get; set; }
        }


        public class BSInfo
        {
            public string label { get; set; }
            public string value { get; set; }
        }

        public class Decisions
        {
            public LDPerson winner { get; set; }
            public LDPerson loser { get; set; }
        }

        public class Leaders
        {
            public Hitdistance hitDistance { get; set; }
            public Hitspeed hitSpeed { get; set; }
            public Pitchspeed pitchSpeed { get; set; }
        }

        public class Hitdistance
        {
        }

        public class Hitspeed
        {
        }

        public class Pitchspeed
        {
        }




    }
}


